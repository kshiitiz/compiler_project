# ----------------------------------------------------------------------
# lexer.py
#
# A parser for C generating parsetree.
# ----------------------------------------------------------------------

import ply.lex as lex
from ply.lex import TOKEN

# taboutputdir='../bin/'

# Reserved words
class CLexer(object):
    def __init__(self,lbrace_func,rbrace_func):
        # self.error_func=error_func
        self.last_token = None
        self.lbrace_func=lbrace_func
        self.rbrace_func=rbrace_func
        self.last_token=None

    def build(self,outputdir, debug=0):
        self.lexer = lex.lex(object=self, outputdir = outputdir, debug=debug)

    def input(self,text):
        self.lexer.input(text)

    def token(self):
        self.last_token = self.lexer.token()
        return self.last_token

    def find_tok_column(self,token):
        """ Find the column of the token in its line.
        """
        last_cr = self.lexer.lexdata.rfind('\n', 0, token.lexpos)
        return token.lexpos - last_cr

    def _error(self, msg, token):
        location = self._make_tok_location(token)
        print "Error while lexing: ",msg, location[0], location[1]
        self.lexer.skip(1)

    def _make_tok_location(self, token):
        return (token.lineno, self.find_tok_column(token))



    reserved = (
        # CPP Keywords
        # 'AND_EQ', 'ASM', 'BITAND', 'BITOR', 'BOOL', 'CATCH', 'CLASS', 'COMPL',
        # 'CONST_CAST', 'DELETE', 'DYNAMIC_CAST', 'EXPLICIT', 'EXPORT', 'FALSE', 'FRIEND',
        # 'INLINE', 'MUTABLE', 'NAMESPACE', 'NEW',  'NOT_EQ', 'OPERATOR', 
        # 'OR_EQ', 'PRIVATE', 'PROTECTED', 'PUBLIC', 'REINTERPRET_CAST', 'STATIC_CAST', 
        # 'TEMPLATE', 'THIS', 'THROW', 'TRUE', 'TRY', 'TYPENAME', 'USING', 
        # 'WCHAR_T', 'XOR_EQ',
        #Old C Keywords
        'AUTO', 'BREAK', 'CASE', 'CHAR', 'CONST', 'CONTINUE', 'DEFAULT', 'DO', 'DOUBLE',
        'ELSE',  'EXTERN', 'FLOAT', 'FOR', 'GOTO', 'IF', 'INT', 'LONG', 'REGISTER',
        'SHORT', 'SIGNED', 'SIZEOF', 'STATIC', 'STRUCT', 'SWITCH', 'TYPEDEF',
        'UNION', 'UNSIGNED', 'VOID', 'VOLATILE', 'WHILE',
        'RETURN', 'ENUM', 'CIN', 'COUT', 'ENDL'
        #'ENUM', 'RETURN', , 'EXTERN "C"', '#DEFINE', 'EXIT()', 'TYPEID',
    )

    tokens = reserved + (
        # Literals (identifier)
        'ID', 'TYPEID', 'INT_CONST_DEC', 'INT_CONST_OCT', 'INT_CONST_HEX', 
        'INT_CONST_BIN', 'FLOAT_CONST', 'HEX_FLOAT_CONST', 'CHAR_CONST',
        'STRING_LITERAL',

        # Operators (+,-,*,/,%,|,&,~,^,<<,>>, ||, &&, !, <, <=, >, >=, ==, !=)
        'PLUS', 'MINUS', 'TIMES', 'DIVIDE', 'MOD',
        'OR', 'AND', 'NOT', 'XOR', 'OUTSHIFT', 'INPSHIFT',
        'LSHIFT', 'RSHIFT', 'LOR', 'LAND', 'LNOT',
        'LT', 'LE', 'GT', 'GE', 'EQ', 'NE',

        # Assignment (=, *=, /=, %=, +=, -=, <<=, >>=, &=, ^=, |=)
        'EQUALS', 'TIMESEQUAL', 'DIVEQUAL', 'MODEQUAL', 'PLUSEQUAL', 'MINUSEQUAL',
        'LSHIFTEQUAL', 'RSHIFTEQUAL', 'ANDEQUAL', 'XOREQUAL', 'OREQUAL',

        # Increment/decrement (++,--)
        'PLUSPLUS', 'MINUSMINUS',

        # Structure dereference (->)
        'ARROW',

        # Conditional operator (?)
        'CONDOP',

        # Delimeters ( ) [ ] { } , . ; :
        'LPAREN', 'RPAREN',
        'LBRACKET', 'RBRACKET',
        'LBRACE', 'RBRACE',
        'COMMA', 'PERIOD', 'SEMI', 'COLON',

        # Ellipsis (...)
        'ELLIPSIS',

    )



    ##
    ## Regexes for use in tokens
    ##
    ##

    # valid C identifiers (K&R2: A.2.3), plus '$' (supported by some compilers)
    identifier = r'[a-zA-Z_$][0-9a-zA-Z_$]*'

    hex_prefix = '0[xX]'
    hex_digits = '[0-9a-fA-F]+'
    bin_prefix = '0[bB]'
    bin_digits = '[01]+'

    # integer constants (K&R2: A.2.5.1)
    integer_suffix_opt = r'(([uU]ll)|([uU]LL)|(ll[uU]?)|(LL[uU]?)|([uU][lL])|([lL][uU]?)|[uU])?'
    decimal_constant = '(0'+integer_suffix_opt+')|([1-9][0-9]*'+integer_suffix_opt+')'
    octal_constant = '0[0-7]*'+integer_suffix_opt
    hex_constant = hex_prefix+hex_digits+integer_suffix_opt
    bin_constant = bin_prefix+bin_digits+integer_suffix_opt

    #bad_octal_constant = '0[0-7]*[89]'

    # character constants (K&R2: A.2.5.2)
    # Note: a-zA-Z and '.-~^_!=&;,' are allowed as escape chars to support #line
    # directives with Windows paths as filenames (..\..\dir\file)
    # For the same reason, decimal_escape allows all digit sequences. We want to
    # parse all correct code, even if it means to sometimes parse incorrect
    # code.
    #
    simple_escape = r"""([a-zA-Z._~!=&\^\-\\?'"])"""
    decimal_escape = r"""(\d+)"""
    hex_escape = r"""(x[0-9a-fA-F]+)"""
    bad_escape = r"""([\\][^a-zA-Z._~^!=&\^\-\\?'"x0-7])"""

    escape_sequence = r"""(\\("""+simple_escape+'|'+decimal_escape+'|'+hex_escape+'))'
    cconst_char = r"""([^'\\\n]|"""+escape_sequence+')'
    char_const = "'"+cconst_char+"'"
    unmatched_quote = "('"+cconst_char+"*\\n)|('"+cconst_char+"*$)"
    bad_char_const = r"""('"""+cconst_char+"""[^'\n]+')|('')|('"""+bad_escape+r"""[^'\n]*')"""

    # string literals (K&R2: A.2.6)
    string_char = r"""([^"\\\n]|"""+escape_sequence+')'
    string_literal = '"'+string_char+'*"'
    bad_string_literal = '"'+string_char+'*?'+bad_escape+string_char+'*"'

    # floating constants (K&R2: A.2.5.3)
    exponent_part = r"""([eE][-+]?[0-9]+)"""
    fractional_constant = r"""([0-9]*\.[0-9]+)|([0-9]+\.)"""
    floating_constant = '(((('+fractional_constant+')'+exponent_part+'?)|([0-9]+'+exponent_part+'))[FfLl]?)'
    binary_exponent_part = r'''([pP][+-]?[0-9]+)'''
    hex_fractional_constant = '((('+hex_digits+r""")?\."""+hex_digits+')|('+hex_digits+r"""\.))"""
    hex_floating_constant = '('+hex_prefix+'('+hex_digits+'|'+hex_fractional_constant+')'+binary_exponent_part+'[FfLl]?)'

    # Completely ignored characters
    t_ignore = ' \t\x0c'

    # Newlines


    def t_NEWLINE(self,t):
        r'\n+'
        t.lexer.lineno += t.value.count("\n")

    # Operators
    t_PLUS = r'\+'
    t_MINUS = r'-'
    t_TIMES = r'\*'
    t_DIVIDE = r'/'
    t_MOD = r'%'
    t_OR = r'\|'
    t_AND = r'&'
    t_NOT = r'~'
    t_XOR = r'\^'
    t_OUTSHIFT = r'<<<'
    t_INPSHIFT = r'>>>'
    t_LSHIFT = r'<<'
    t_RSHIFT = r'>>'
    t_LOR = r'\|\|'
    t_LAND = r'&&'
    t_LNOT = r'!'
    t_LT = r'<'
    t_GT = r'>'
    t_LE = r'<='
    t_GE = r'>='
    t_EQ = r'=='
    t_NE = r'!='

    # Assignment operators

    t_EQUALS = r'='
    t_TIMESEQUAL = r'\*='
    t_DIVEQUAL = r'/='
    t_MODEQUAL = r'%='
    t_PLUSEQUAL = r'\+='
    t_MINUSEQUAL = r'-='
    t_LSHIFTEQUAL = r'<<='
    t_RSHIFTEQUAL = r'>>='
    t_ANDEQUAL = r'&='
    t_OREQUAL = r'\|='
    t_XOREQUAL = r'\^='

    # Increment/decrement
    t_PLUSPLUS = r'\+\+'
    t_MINUSMINUS = r'--'

    # ->
    t_ARROW = r'->'

    # ?
    t_CONDOP = r'\?'

    # Delimeters
    t_LPAREN = r'\('
    t_RPAREN = r'\)'
    t_LBRACKET = r'\['
    t_RBRACKET = r'\]'
    t_COMMA = r','
    t_PERIOD = r'\.'
    t_SEMI = r';'
    t_COLON = r':'
    t_ELLIPSIS = r'\.\.\.'

    # Identifiers and reserved words

    reserved_map = {}
    for r in reserved:
        reserved_map[r.lower()] = r

    def t_LBRACE(self,t):
        r'\{'
        self.lbrace_func()
        return t

    def t_RBRACE(self,t):
        r'\}'
        # self.rbrace_func()
        return t

    t_STRING_LITERAL    = string_literal
    @TOKEN(floating_constant)
    def t_FLOAT_CONST(self,t):
        return t

    @TOKEN(hex_floating_constant)
    def t_HEX_FLOAT_CONST(self,t):
        return t

    @TOKEN(hex_constant)
    def t_INT_CONST_HEX(self,t):
        return t

    @TOKEN(bin_constant)
    def t_INT_CONST_BIN(self,t):
        return t

    def t_BAD_CONST_OCT(self,t):
        r'0[0-7]*[89]'
        msg = "Invalid octal constant"
        self._error(msg,t)

    @TOKEN(octal_constant)
    def t_INT_CONST_OCT(self,t):
        return t

    @TOKEN(decimal_constant)
    def t_INT_CONST_DEC(self,t):
        return t

    @TOKEN(char_const)
    def t_CHAR_CONST(self,t):
        return t


    @TOKEN(unmatched_quote)
    def t_UNMATCHED_QUOTE(self,t):
        msg = "Unmatched '"
        self._error(msg, t)

    @TOKEN(bad_char_const)
    def t_BAD_CHAR_CONST(self,t):
        msg = "Invalid char constant %s" % t.value
        self._error(msg, t)

    # unmatched string literals are caught by the preprocessor

    @TOKEN(bad_string_literal)
    def t_BAD_STRING_LITERAL(self,t):
        msg = "String contains invalid escape code"
        self._error(msg, t)

    def t_ID(self,t):
        r'[A-Za-z_][\w_]*'
        t.type = self.reserved_map.get(t.value, "ID")
        return t

    # Comments

    def t_comment(self,t):
        r'(/\*(.|\n)*?\*/)|(//.*)'
        t.lexer.lineno += t.value.count('\n')

    # Preprocessor directive (ignored)

    def t_preprocessor(self,t):
        r'\#(.)*?\n'
        t.lexer.lineno += 1


    def t_error(self,t):
        msg ="Illegal character %s" % repr(t.value[0])
        self._error(msg,t)


# def error_func(msg, token):
#     print "Error in Lexer:",msg,token.lexer.lineno,find_tok_column(token)
#     lexer.skip(1)


# lexer = lex.lex(outputdir=taboutputdir,debug=0)
