# ----------------------------------------------------------------------
# tree_parser.py
#
# A parser for C generating parsetree.
# ----------------------------------------------------------------------


import ply.yacc as yacc
import sys
import ast
import symtab
import tac
import pprint
from lexer import CLexer
from collections import OrderedDict

if len(sys.argv) == 3:
    outFile= sys.argv[2]
    f= file(outFile,'w')
    sys.stdout = f
elif len(sys.argv) != 2:
    print "Usage: python final__ply.py <input_file_name> [output_file_name]"
    exit()


inFile = sys.argv[1]


# Output Files
taboutputdir='../bin/'
AST_FILE='ast.dot'
CSV_FILE='symbol.csv'
AST_TXT='ast.txt'

#global symbol table
scope_stack = OrderedDict()
scope_stack_type = OrderedDict()
offset_stack = []

#List of Implicit Function Declarations
implicit_decls=[]
global_string=dict()

#TAC
TAC = tac.ThreeAC()

# Initializing the global symbol table
current_symbol_table = symtab.SymTable(name='GLOBAL',parent=None,depth=0,scope_count=0)
current_symbol_table.parent = current_symbol_table
current_tac_table = 'GLOBAL'
scope_stack[0]=[]
scope_stack_type[0]=[]
offset_stack.append(0)

############
# Constants
############
# Data Types
FLOAT_TYPES = ['float','double']
INT_TYPES = ['int','long','short','char']
ARR_PTR_TYPES= (ast.ArrayDecl, ast.PtrDecl) 


######################################
def error_func(msg, lineno=None):
    if lineno==None:
        lineno = clex.lexer.lineno
    print "Error while Parsing in line number %s : " %(lineno),msg
    sys.exit()

def warning_func(msg):
    print "Warning : Line number %s : " %(clex.lexer.lineno),msg

def implicit_decl_check(cst,imp_decls):
    for imp_decl in imp_decls:
        look_decl = find_in_scope(imp_decl[0]) 
        if look_decl== False or not isinstance(look_decl['type']['type'], ast.FuncDecl):
            error_func("Function %s not declared"%imp_decl[0],lineno=imp_decl[2])
        else:
            func = look_decl['type']['type']
            if len(func.args.params) != imp_decl[1]:
                error_func("Incorrect Function Call %s"%imp_decl[0],lineno=imp_decl[2])

#----------------------scope push-pop functions---------------

def push_scope():
    global current_symbol_table

    if len(scope_stack)==1:
        new_table = symtab.SymTable(name=None,
                                parent=current_symbol_table,
                                depth=1,
                                offset=offset_stack[-1])
        current_symbol_table.add_child(new_table)
        current_symbol_table =  new_table
        scope_stack[1]=[]
        scope_stack_type[1]=[]
        offset_stack.append(0)
    else:
        current_symbol_table.scope_count +=1
        scope_stack[current_symbol_table.scope_count]=[]
        scope_stack_type[current_symbol_table.scope_count]=[]

def pop_scope():
    global current_symbol_table

    if len(scope_stack)<=1:
        error_func("Unmatched parenthesis")
    elif len(scope_stack)==2:
        current_symbol_table.size = offset_stack.pop()
    
    #Handling Structs
    if current_symbol_table.name == None:
        current_symbol_table.parent.children.pop()
    
    current_scope_number = next(reversed(scope_stack))
    del scope_stack[current_scope_number]
    del scope_stack_type[current_scope_number]
    if len(scope_stack)==1:
        current_symbol_table = current_symbol_table.parent



def temp_call(size = 4,make_ptr=False):
    if make_ptr:
        return current_symbol_table.makeTemp(size,offset_stack,'PtrTemp')    
    else:
        return current_symbol_table.makeTemp(size,offset_stack,'Temp')

def makelist(num):
    return  [TAC.getNextQuad(current_tac_table)+num]

def emit(dest,src1,src2,op):
    TAC.emit(current_symbol_table.name,current_tac_table,dest,src1,src2,op)

#----------------------------------------------------------------------

def find_tok_column(token):
    """ Find the column of the token in its line.
    """
    last_cr = lexer.lexdata.rfind('\n', 0, token.lexpos)
    return token.lexpos - last_cr

def make_tok_location(token):
    return (token.lineno(0), find_tok_column(token))


def print_tables():
    csv_file=open(CSV_FILE,'w')
    global current_symbol_table
    current_symbol_table.size = offset_stack[-1]
    implicit_decl_check(current_symbol_table.table,implicit_decls)
    current_symbol_table.print_symbol_table(csv_file,error_func)
    csv_file.close()

def print_ast(asttree):
    f=open(AST_FILE,'w')
    g=open(AST_TXT,"w")
    f.write("graph{\n")
    f.write("ordering=out\n")
    asttree.show(ast_dot=f,ast_txt=g)
    f.write("}\n")
    g.close()
    f.close()

################################### Scope ######################################

def find_in_scope(name):
    for scope_count in reversed(scope_stack):
        if name in scope_stack[scope_count]:
            if scope_count!=0:
                new_name = name+'_'+str(scope_count)
                sym_tab = current_symbol_table.table
                return sym_tab.get(new_name)
            else:
                new_name = name #+ '_0'
                sym_tab = current_symbol_table.parent.table
                return sym_tab.get(new_name)
    return False

def find_in_usertype_scope(name):
    for scope_count in reversed(scope_stack):
        if name in scope_stack_type[scope_count]:
            if scope_count!=0:
                new_name = name+'_'+str(scope_count)
                sym_tab = current_symbol_table.userTypeTable
                return sym_tab.get(new_name)
            else:
                new_name = name #+ '_0'
                sym_tab = current_symbol_table.parent.userTypeTable
                return sym_tab.get(new_name)
    return False


def add_id(node,init,type,flag=False,funcdecl=False):
    index = -2 if flag else -1
    if hasattr(node,'name'):
        scope_tab = current_symbol_table.table
        offset = offset_stack[-1]
        current_scope_number = next(reversed(scope_stack))
        new_name = node.name
        if current_scope_number!=0:
            new_name = node.name+'_'+str(current_scope_number)
        if node.name in scope_stack[current_scope_number]:     
            # Handling function definitions and declarations combination
            if not isinstance(type['type'], ast.FuncDecl):
                error_func("Variable %s already defined in current scope"%node.name)
            elif scope_tab[new_name]['defined']:
                error_func("Function %s already defined in current scope"%node.name)
            elif funcdecl:
                error_func("Function %s already declared in current scope"%node.name)
            else:
                temp = scope_tab[new_name]
                decl_params_len = len(temp['type']['type'].args.params)
                new_param_len = len(node.type.args.params)
                if decl_params_len != new_param_len:
                    error_func("Function %s incorrect number of parameters"%node.name)
                scope_tab[new_name]['offset']=offset
                scope_tab[new_name]['defined']=True
                offset_stack[-1] = offset + temp['size']
        else:
            if isinstance(type['type'], ast.FuncDecl) and funcdecl:
                scope_tab[new_name]=dict(init=init,
                                        type=type,
                                        size=node.size,
                                        offset=offset,
                                        defined=False,
                                        name=new_name)
            else:
                scope_tab[new_name]=dict(init=init,
                                            type=type,
                                            size=node.size,
                                            offset=offset,
                                            defined=True,
                                            name=new_name)
            current_symbol_table.addAddressDescriptor(new_name,offset)
            scope_stack[current_scope_number].append(node.name)
            offset_stack[index] = offset + node.size
            node.place = new_name


def add_user_type(s_or_u, user_type,decls,flag=False):
    index = -2 if flag else -1
    scope_tab = current_symbol_table.userTypeTable
    current_scope_number = next(reversed(scope_stack_type))
    new_name = user_type
    if current_scope_number!=0:
        new_name = user_type+'_'+str(current_scope_number)
    if user_type in scope_stack_type[current_scope_number]:     
        error_func("Usertype %s already defined in current scope"%user_type)
    else:
        size=0
        for key,value in decls.iteritems():
            if s_or_u == 'struct':
                value['declaration'].offset = size
                size += value['declaration'].size
            else:
                value['declaration'].offset = 0
                size = max(size,value['declaration'].size)
        scope_tab[new_name]=dict(decls=decls,size=size)
        scope_stack_type[current_scope_number].append(user_type)
    return scope_tab[user_type]

def add_decl_to_scope(spec,decls,flag=False,funcdecl=False):
    decl_list =[]
    assert isinstance(decls[0],dict)
    for item in decls:
        #Type Checking
        try:
            if isinstance(item['decl'],ast.ArrayDecl) and item['init']:
                if int(item['decl'].dim.value) < int(len(item['init'].exprs)):
                    warning_func("Excess elements in Array initializer") 
                    item['init'].exprs = item['init'].exprs[:int(item['decl'].dim.value)]
        except AttributeError:
            pass

        #To check whether an simple type is initialized with a list
        try:
            if isinstance(item['decl'],ast.TypeDeclaration) and item['init']:
                if isinstance(item['init'],ast.InitList) and len(item['init'].exprs)>1:
                    warning_func("Excess elements in Scalar Initializer")
                if item['init'].type == 'string':
                    error_func("Trying to initialize simple type with String")
        except AttributeError:
            pass

        #Size Calculation
        decl_size = find_size(spec['type'],item['decl'])
        offset = 0
        declaration = ast.Decl(
            name = None,
            quals = spec['qual'],
            storage = spec['storage'],
            funcspec =spec['function'],
            init=item['init'],
            type = item['decl'],
            size = decl_size,
            offset = offset)
        if isinstance(declaration.type,(ast.StructOrUnion,ast.IdentifierType)):
            fixed_declaration=declaration
        else:
            fixed_declaration=fix_decl_name_type(declaration,spec['type'])
        new_spec=dict(qual=fixed_declaration.quals,
                    storage=fixed_declaration.storage,
                    type = fixed_declaration.type,
                    function=fixed_declaration.funcspec)

        if isinstance(fixed_declaration.type, ast.FuncDecl):
            add_id(fixed_declaration,fixed_declaration.init,new_spec,flag,funcdecl)
        else:
            add_id(fixed_declaration,fixed_declaration.init,new_spec,flag)
        decl_list.append(fixed_declaration)

    return decl_list

def fix_user_type(spec,decls):
    decl_dict =dict()

    assert isinstance(decls[0],dict)
    for item in decls:
        
        #Size Calculation
        decl_size = find_size(spec['type'],item['decl'])
        declaration = ast.Decl(
            name = None,
            quals = spec['qual'],
            storage = spec['storage'],
            funcspec =spec['function'],
            type = item['decl'],
            size = decl_size,
            offset = 0)
        if isinstance(declaration.type,(ast.StructOrUnion,ast.IdentifierType)):
            fixed_declaration=declaration
        else:
            fixed_declaration=fix_decl_name_type(declaration,spec['type'])
        new_spec=dict(qual=fixed_declaration.quals,
                    storage=fixed_declaration.storage,
                    type = fixed_declaration.type,
                    function=fixed_declaration.funcspec)
        new_name=fixed_declaration.name
        if decl_dict.get(new_name):
            error_func("Variable %s redeclared in current record"%new_name)
        decl_dict[new_name]=dict(declaration=fixed_declaration,
                                type=new_spec)

    return decl_dict


def const_decl_spec(old_spec,new_spec,kind):
    if old_spec:
        spec=old_spec
    else:
        spec= dict(qual=[],storage=[],type=None,function=[])
    if kind!='type':
        spec[kind].insert(0,new_spec)
    elif spec['type']!= None:
        error_func("Error: cannot define multiple type specifiers")
    else:
        spec['type']=new_spec 
    return spec 

def const_function_definition(spec,decl,body):
    declaration = add_decl_to_scope(
        spec= spec, 
        decls = [dict(decl=decl,init=None)])[0]

    return ast.FuncDef(
        spec = spec,
        decl = declaration,
        body = body)


def modify_decl(decl, modifier):
        """ Tacks a type modifier on a declarator, and returns
            the modified declarator.

            Note: the declarator and modifier may be modified
        """
        #~ print '****'
        #~ decl.show(offset=3)
        #~ modifier.show(offset=3)
        #~ print '****'

        modifier_head = modifier
        modifier_tail = modifier

        # The modifier may be a nested list. Reach its tail.
        #
        while modifier_tail.type:
            modifier_tail = modifier_tail.type

        # If the decl is a basic type, just tack the modifier onto
        # it
        #
        if isinstance(decl, ast.TypeDeclaration):
            modifier_tail.type = decl
            return modifier
        else:
            # Otherwise, the decl is a list of modifiers. Reach
            # its tail and splice the modifier onto the tail,
            # pointing to the underlying basic type.
            #
            decl_tail = decl

            while not isinstance(decl_tail.type, ast.TypeDeclaration):
                decl_tail = decl_tail.type

            modifier_tail.type = decl_tail.type
            decl_tail.type = modifier_head
            return decl

def fix_decl_name_type(decl, typename):
        """ Fixes a declaration. Modifies decl.
        """
        # Reach the underlying basic type
        #
        type = decl
        while not isinstance(type, ast.TypeDeclaration):
            type = type.type
        decl.name = type.name
        type.quals = decl.quals

        # # The typename is a list of types. If any type in this
        # # list isn't an IdentifierType, it must be the only
        # # type in the list (it's illegal to declare "int enum ..")
        # # If all the types are basic, they're collected in the
        # # IdentifierType holder.
        # #

        if not isinstance(typename, ast.IdentifierType):
            type.type = typename
            return decl

        # In our case typename is a single type.

        if not typename:
            # Functions default to returning int
            #
            if not isinstance(decl.type, ast.FuncDecl):
                print "Warning: Missing type in declaration"
            type.type = ast.IdentifierType('int')
        else:
            # At this point, we know that typename is a list of IdentifierType
            # nodes. Concatenate all the names into a single list.
            #
            type.type = ast.IdentifierType(typename.name)
        return decl


def find_size(spec_type, decl):
    if isinstance(decl,ast.PtrDecl):
        return 8
    elif isinstance(decl,ast.ArrayDecl):
        return int(decl.dim.value) * find_size(spec_type, decl.type)
    elif isinstance(spec_type, ast.IdentifierType):
        if spec_type.name == 'char':
            return 4
        if spec_type.name == 'short':
            return 2
        elif spec_type.name in ('int','float'):
            return 4
        elif spec_type.name in ('double','long'):
            return 8
    elif isinstance(spec_type, ast.StructOrUnion):
        return spec_type.size
    else:
        return 0

##
##-------------------TYPE CHECKING-----------------------##
##

def check_castability(typename, exptype):
    if isinstance(typename, ast.TypeDeclaration):
        typename = typename.type.name
    if exptype == 'string' or typename == 'string':
        error_func("Type Casting with Strings Not Allowed")
    if isinstance(exptype,(ast.ArrayDecl,ast.PtrDecl)):
        #ARRAYDECL to pointer DECL
        if isinstance(typename, ast.PtrDecl):
            return True
        else:
            error_func('Cannot Type Cast Array/Pointer Type to %s'%typename)
    else:
        if isinstance(typename, ast.PtrDecl):
            error_func('Cannot Type Cast %s to Pointer Type'%exptype)
        else:
            return True

def check_exp(opr, exp1_type, exp2_type):
    if exp1_type == 'string' or exp2_type == 'string':
        error_func('Binary Operation %s Not Allowed on Strings'%opr)
    
    # we can compare pointers and simle types.
    elif opr in ['>','<','>=','<=','==','!=']:
        return 'int'
    elif isinstance(exp1_type, ARR_PTR_TYPES) or isinstance(exp2_type, ARR_PTR_TYPES):
            if isinstance(exp1_type, ARR_PTR_TYPES) and isinstance(exp2_type, ARR_PTR_TYPES):
                error_func('Binary Operation %s Not Allowed on 2 Arrays'%opr)     
            elif opr in ['+','-']:
                return exp1_type if isinstance(exp1_type, ARR_PTR_TYPES) else exp2_type
            else:
                error_func('Binary Operation %s Not Allowed on Arrays'%opr)
    elif opr in ['%','>>','<<','&','|','^']:
        if exp1_type in FLOAT_TYPES or exp2_type in FLOAT_TYPES:
            error_func('Binary Operation %s Not Allowed on Floating Values'%opr)
        else:
            return 'int'
    else:
        if exp1_type in FLOAT_TYPES or exp2_type in FLOAT_TYPES:
            return 'float'
        else:
            return 'int'

        

def exp_type(name):
    in_scope =find_in_scope(name)
    if in_scope:
        return in_scope[1]['type']
    else:
        return False

def check_type_equiv(type1,type2):
    if isinstance(type1, ARR_PTR_TYPES) and isinstance(type2,ast.StructOrUnion):
        return True 
    if not isinstance(type1, ARR_PTR_TYPES) and not isinstance(type2, ARR_PTR_TYPES):       
        return True
    if isinstance(type1, ARR_PTR_TYPES) and isinstance(type2, ARR_PTR_TYPES):
        type1=type1.type
        type2=type2.type
        return check_type_equiv(type1,type2)
    else:
        return False

def is_ptr(temp):
    if temp == None:
        return False
    if current_symbol_table.table[temp]['type']== 'PtrTemp':
        return True
    else:
        return False


#---------------------------building lexer----------------------
clex = CLexer(lbrace_func=push_scope,
            rbrace_func=None)
clex.build(outputdir = taboutputdir)
tokens = clex.tokens


##
## Precedence and associativity of operators
##
precedence = (
    ('left', 'LOR'),
    ('left', 'LAND'),
    ('left', 'OR'),
    ('left', 'XOR'),
    ('left', 'AND'),
    ('left', 'EQ', 'NE'),
    ('left', 'GT', 'GE', 'LT', 'LE'),
    ('left', 'RSHIFT', 'LSHIFT'),
    ('left', 'PLUS', 'MINUS'),
    ('left', 'TIMES', 'DIVIDE', 'MOD')
)

##
## Grammar productions
## Implementation of the BNF defined in K&R2 A.13
##

# Wrapper around a translation unit, to allow for empty input.
# Not strictly part of the C99 Grammar, but useful in practice.
#
def p_translation_unit_or_empty(p):
    """ translation_unit_or_empty   : translation_unit
                                    | empty
    """
    if p[1] is None:
        p[0]=ast.FileAST([])
    else:
        p[0]=ast.FileAST(p[1])
    # p[0].graph_dfs()

def p_translation_unit_1(p):
    """ translation_unit    : external_declaration
    """
    # Note: external_declaration is already a list
    #
    p[0] = p[1]

def p_translation_unit_2(p):
    """ translation_unit    : translation_unit external_declaration
    """

    p[0]= p[1]+p[2]

# Declarations always come as lists (because they can be
# several in one line), so we wrap the function definition
# into a list as well, to make the return value of
# external_declaration homogenous.


def p_external_declaration_1(p):
    """ external_declaration    : function_definition
    """
    p[0] = [p[1]]

def p_external_declaration_2( p):
    """ external_declaration    : declaration
    """
    p[0] = p[1]

def p_external_declaration_3(p):
    """ external_declaration    : function_declaration
    """
    p[0] = [p[1]]

def p_external_declaration_4(p):
    """ external_declaration    : SEMI
    """
    p[0]=[]

# In function definitions, the declarator can be followed by
# a declaration list, for old "K&R style" function definitios.
#
def p_function_declaration(p):
    """ function_declaration    :   declaration_specifiers func_declarator SEMI
    """
    if p[1]['type'] is None:
        p[1]['type']=ast.IdentifierType('int')

    decls = [dict(decl=p[2], init= None)]

    p[0] = add_decl_to_scope(spec=p[1],decls=decls,funcdecl=True)

    

def p_function_definition_1(p):
    """ function_definition : func_declarator compound_statement
    """
    # no declaration specifiers - 'int' becomes the default type
    spec = dict(qual=[],
                storage=[],
                type=ast.IdentifierType('int'),
                function=[])
    p[0] = const_function_definition(spec=spec,decl=p[1],body=p[2])
    emit('','','','JUMPBACK')

def p_function_definition_2(p):
    """ function_definition : declaration_specifiers func_declarator compound_statement
    """
    # declaration_specifiers is a dictionary of lists

    spec = p[1]
    p[0] = const_function_definition(spec=spec,decl=p[2],body=p[3])
    emit('','','','JUMPBACK')

def p_statement(p):
    """ statement   : labeled_statement
                            | expression_statement
                            | compound_statement
                            | selection_statement
                            | iteration_statement
                            | jump_statement
                            | input_statement
                            | output_statement
    """
    p[0]= p[1]


def p_declaration_1(p):
    """ declaration : declaration_specifiers SEMI
    """

    spec =p[1]
    s_u_or_e = (ast.StructOrUnion, ast.Enum)
    if spec['type'] and isinstance(spec['type'],s_u_or_e):
        decls = [ ast.Decl(
        name = None,
        quals=spec['qual'],
        storage = spec['storage'],
        funcspec = spec['function'],
        type= spec['type'],
        init = None)]

        p[0]=decls
    else:
        error_func("useless type name in empty declaration")
     

def p_declaration_2(p):
    """ declaration : declaration_specifiers init_declarator_list SEMI
    """
    # decl_list is a list of child nodes.
    if p[1]['type'] is None:
        p[1]['type']=ast.IdentifierType('int')

    if clex.last_token and clex.last_token.type == "LBRACE":
        decl_list = add_decl_to_scope(spec=p[1],decls=p[2],flag=True)
    else:
        decl_list = add_decl_to_scope(spec=p[1],decls=p[2])
    p[0] = decl_list
    for decl in decl_list:
        if decl.init:
            new_name = find_in_scope(decl.name)['name']
            emit(new_name,decl.init.place,'','=')

# Since each declaration is a list of declarations, this
# rule will combine all the declarations and return a single
# list
#
def p_abstract_declarator_opt(p):
    """ abstract_declarator_opt     :  empty 
                                    | abstract_declarator """
    p[0]= p[1]


def p_declaration_specifiers_opt(p):
    """ declaration_specifiers_opt  :  empty 
                                    | declaration_specifiers """
    p[0]= p[1]

def p_expression_opt(p):
    """ expression_opt  :  empty 
                        | expression """
    p[0]= p[1]                            

def p_direct_declarator_list_opt(p):
    ''' direct_declarator_list_opt :  direct_declarator_list 
                                    | empty '''
    p[0]= p[1]                              

def p_parameter_type_list_opt(p):
    """ parameter_type_list_opt     :  empty 
                                    | parameter_type_list """
    p[0]= p[1]

def p_specifier_qualifier_list_opt(p):
    """ specifier_qualifier_list_opt    :  empty 
                                        | specifier_qualifier_list """
    p[0]= p[1]
    
def p_block_item_list_opt(p):
    """ block_item_list_opt  :  empty 
                             | block_item_list """
    p[0]= p[1]
    
def p_type_qualifier_list_opt(p):
    """ type_qualifier_list_opt     :  empty 
                                    | type_qualifier_list """
    p[0]= p[1]
    
def p_struct_declarator_list_opt(p):
    """ struct_declarator_list_opt  :  empty 
                                    | struct_declarator_list """
    p[0]= p[1]
    

def p_declaration_specifiers_1(p):
    """ declaration_specifiers  : type_qualifier declaration_specifiers_opt
    """
    p[0]=const_decl_spec(p[2],p[1],'qual')

def p_declaration_specifiers_2(p):
    """ declaration_specifiers  : type_specifier declaration_specifiers_opt
    """
    p[0]=const_decl_spec(p[2],p[1],'type')

def p_declaration_specifiers_3(p):
    """ declaration_specifiers  : storage_class_specifier declaration_specifiers_opt
    """
    p[0]=const_decl_spec(p[2],p[1],'storage')

def p_storage_class_specifier(p):
    """ storage_class_specifier : AUTO
                                | REGISTER
                                | STATIC
                                | EXTERN
                                | TYPEDEF
    """
    p[0]=p[1]

def p_type_specifier_1(p):
    """ type_specifier  : VOID
                        | CHAR
                        | SHORT
                        | INT
                        | LONG
                        | FLOAT
                        | DOUBLE
                        | SIGNED
                        | UNSIGNED

    """
    p[0]= ast.IdentifierType(p[1])

def p_type_specifier_2(p):
    """ type_specifier  : typedef_name
                        | enum_specifier
                        | struct_or_union_specifier
    """
    p[0]= p[1]

def p_type_qualifier(p):
    """ type_qualifier  : CONST
                        | VOLATILE
    """
    p[0]=p[1]

def p_init_declarator_list(p):
    """ init_declarator_list    : init_declarator
                                | init_declarator_list COMMA init_declarator
    """
    p[0]=p[1]+[p[3]] if len(p) == 4 else [p[1]]


def p_init_declarator(p):
    """ init_declarator : declarator
                        | declarator EQUALS initializer
    """
    p[0] = dict(decl=p[1], init=(p[3] if len(p) > 2 else None))

def p_specifier_qualifier_list_1(p):
    """ specifier_qualifier_list    : type_qualifier specifier_qualifier_list_opt
    """
    p[0]= const_decl_spec(p[2],p[1],'qual')

def p_specifier_qualifier_list_2(p):
    """ specifier_qualifier_list    : type_specifier specifier_qualifier_list_opt
    """
    p[0]= const_decl_spec(p[2],p[1],'type')


# TYPEID is allowed here (and in other struct/enum related tag names), because
# struct/enum tags reside in their own namespace and can be named the same as types
#
def p_struct_or_union_specifier_1(p):
    """ struct_or_union_specifier   : struct_or_union ID
                                    | struct_or_union TYPEID
    """
    s_or_u = find_in_usertype_scope(p[2])
    if s_or_u == False:
        p[0]=ast.StructOrUnion(s_or_u = p[1],name=p[2],decls=None,size=None) 
        # error_func("Usertype %s not found"%p[2])
    # p[0]=ast.StructOrUnion(name=None,decls=p[2],size=s_or_u['size'])
    else:
        p[0]=ast.StructOrUnion(s_or_u = p[1],name=p[2],decls=s_or_u['decls'],size=s_or_u['size'])


def p_struct_or_union_specifier_3(p):
    """ struct_or_union_specifier   : struct_or_union ID brace_open struct_declaration_list brace_close
                                    | struct_or_union TYPEID brace_open struct_declaration_list brace_close
    """
    s_or_u = add_user_type(p[1], p[2],p[4])
    p[0]=ast.StructOrUnion(s_or_u = p[1],name=p[2],decls=p[4],size=s_or_u['size'])

def p_struct_or_union(p):
    """ struct_or_union : STRUCT
                        | UNION
    """
    p[0]=p[1]

# Combine all declarations into a single list
#
def p_struct_declaration_list(p):
    """ struct_declaration_list     : struct_declaration
                                    | struct_declaration_list struct_declaration
    """
    p[0] = p[1] 
    if len(p) != 2:
        if(p[0].viewkeys()-p[2].viewkeys() != set(p[0].viewkeys())):
            error_func("Redeclared variables in record")
        else:
            p[0] = OrderedDict(p[0], **p[2])

def p_struct_declaration_1(p):

    """ struct_declaration : specifier_qualifier_list struct_declarator_list_opt SEMI
    """

    spec = p[1]
    if p[2]:
        # print p[2]
        decls=fix_user_type(spec=spec,decls=p[2])
        # print decls
        p[0]=decls
    else:
        p[0]=None

def p_struct_declarator_list(p):
    """ struct_declarator_list  : struct_declarator
                                | struct_declarator_list COMMA struct_declarator
    """
    if len(p)==4:
        p[0]=p[1]+[p[3]]
    else:
        p[0]=[p[1]]

# struct_declarator passes up a dict with the keys: decl (for
# the underlying declarator) and bitsize (for the bitsize)
#
def p_struct_declarator_1(p):
    """ struct_declarator : declarator
    """
    p[0]=dict(decl=p[1])

def p_enum_specifier_1(p):
    """ enum_specifier  : ENUM ID
                        | ENUM TYPEID
    """
    p[0]=ast.Enum(p[2],None)

def p_enum_specifier_2(p):
    """ enum_specifier  : ENUM brace_open enumerator_list brace_close
    """
    p[0]=ast.Enum(None,p[3])

def p_enum_specifier_3(p):
    """ enum_specifier  : ENUM ID brace_open enumerator_list brace_close
                        | ENUM TYPEID brace_open enumerator_list brace_close
    """

    p[0]=ast.Enum(p[2],p[4])

def p_enumerator_list(p):
    """ enumerator_list : enumerator
                        | enumerator_list COMMA enumerator
    """
    if len(p) == 2:
        p[0] = ast.EnumeratorList([p[1]])
    else:
        p[1].enumerators.append(p[3])
        p[0]=p[1]


def p_enumerator(p):
    """ enumerator  : ID
                    | ID EQUALS constant_expression
    """
    if len(p) == 2:
        p[0]=ast.Enumerator(p[1],None)


    else:
        p[0]=ast.Enumerator(p[1],p[3])


def p_func_declarator_1(p):
    """ func_declarator     : func_direct_declarator
                            | pointer func_direct_declarator
    """
    if len(p) == 2:
        p[0] = p[1]
    else:
        p[0] = modify_decl(p[2],p[1])   


def p_declarator_1(p):
    """ declarator  : direct_declarator
    """
    p[0] = p[1]

def p_declarator_2(p):
    """ declarator  : pointer direct_declarator
    """
    p[0] = modify_decl(p[2],p[1])

def p_direct_declarator_1(p):
    """ direct_declarator   : ID
    """
    p[0]=ast.TypeDeclaration(p[1],None,None)

def p_direct_declarator_2(p):
    """ direct_declarator   : LPAREN declarator RPAREN
    """
    p[0]=p[2]

def p_direct_declarator_3(p):
    """ direct_declarator   : direct_declarator LBRACKET constant_expression RBRACKET
    """
    array = ast.ArrayDecl(type = None, dim = p[3])
    p[0] = modify_decl(p[1],array)

def p_direct_declarator_4(p):
    """ direct_declarator   : direct_declarator LBRACKET RBRACKET
    """
    array = ast.ArrayDecl(type = None , dim = None)
    p[0] = modify_decl(p[1],array)


def p_func_direct_declarator_1(p):
    """ func_direct_declarator   : direct_declarator LPAREN parameter_type_list RPAREN
    """
    global current_symbol_table,current_tac_table
    params_list= p[3] or ast.ParamList(params=[])     
    if clex.last_token and clex.last_token.type =="LBRACE":
        current_symbol_table.name = p[1].name
        current_tac_table = p[1].name

        TAC.newFuncCode(current_tac_table)
        for param in params_list.params:
            if not isinstance(param,ast.Ellipsis):
                decl = add_decl_to_scope(spec=param.spec,decls=[dict(decl=param.decl,init=None)])
                addr_name = find_in_scope(decl[0].name)['name']
                current_symbol_table.addressDescriptor[addr_name]['arg']=True
    func = ast.FuncDecl(args=params_list,type = None)
    p[0] = modify_decl(decl=p[1],modifier=func)

def p_func_direct_declarator_2(p):
    """ func_direct_declarator   : direct_declarator LPAREN direct_declarator_list_opt RPAREN
    """
    global current_symbol_table,current_tac_table

    if clex.last_token and clex.last_token.type =="LBRACE":
        current_symbol_table.name = p[1].name
        current_tac_table = p[1].name
        TAC.newFuncCode(current_tac_table)
        if p[3] is not None:
            for param in p[3].params:
                if not isinstance(param,ast.Ellipsis):
                    add_decl_to_scope(spec=param.spec,decls=[dict(decl=param.decl,init=None)])
    params_list= p[3] or ast.ParamList(params=[])     
    func = ast.FuncDecl(args=params_list,type = None)
    p[0] = modify_decl(decl=p[1],modifier=func)


def p_pointer(p):
    """ pointer : TIMES type_qualifier_list_opt
                | TIMES type_qualifier_list_opt pointer
    """
    nested_type = ast.PtrDecl(quals=p[2] or [], type=None)
    if len(p) > 3:
        tail_type = p[3]
        while tail_type.type is not None:
            tail_type = tail_type.type
        tail_type.type = nested_type
        p[0] = p[3]
    else:
        p[0] = nested_type


def p_type_qualifier_list(p):
    """ type_qualifier_list : type_qualifier
                            | type_qualifier_list type_qualifier
    """
    if len(p)==2:
        p[0]= [p[1]]
    else:
        p[0]= p[1]+[p[2]]

def p_parameter_type_list(p):
    """ parameter_type_list : parameter_list
                            | parameter_list COMMA ELLIPSIS
    """
    if len(p) > 2:
        p[1].params.append(ast.Ellipsis())
        p[0]=p[1]
    else:
        p[0]= p[1]

def p_parameter_list(p):
    """ parameter_list  : parameter_declaration
                        | parameter_list COMMA parameter_declaration
    """
    if len(p) == 2: # single parameter
        p[0] = ast.ParamList([p[1]])
    else:
        p[1].params.append(p[3])
        p[0]=p[1]


def p_parameter_declaration_1(p):
    """ parameter_declaration   : declaration_specifiers declarator
    """
    spec = p[1]
    p[0]= ast.ParamDecl(spec=p[1],decl=p[2])

def p_parameter_declaration_2(p):
    """ parameter_declaration   : declaration_specifiers abstract_declarator_opt
    """
    spec=p[1]
    if spec['type'] == None:
        spec['type']= ast.IdentifierType('int')
    p[0]= ast.ParamDecl(spec=spec,decl=p[2])

def p_direct_declarator_list(p):
    """ direct_declarator_list : ID
                        | direct_declarator_list COMMA ID
    """

    # no declaration specifiers - 'int' becomes the default type
    spec = dict(qual=[],
            storage=[],
            type=ast.IdentifierType('int'),
            function=[])

    if len(p) == 2: # single parameter
        decl = ast.TypeDeclaration(p[1],None,None)
        param_decl = ast.ParamDecl(spec=spec,decl=decl)
        p[0] = ast.ParamList([param_decl])
    else:
        decl = ast.TypeDeclaration(p[3],None,None)
        param_decl = ast.ParamDecl(spec=spec,decl=decl)
        p[1].params.append(param_decl)
        p[0]= p[1]

def p_initializer_1(p):
    """ initializer : assignment_expression
    """
    p[0] = p[1]

def p_initializer_2(p):
    """ initializer : brace_open initializer_list brace_close
                    | brace_open initializer_list COMMA brace_close
    """
    p[0]=p[2]

def p_initializer_list(p):
    """ initializer_list    : initializer
                            | initializer_list COMMA initializer
    """
    if len(p) == 2: # single initializer
        p[0]= ast.InitList([p[1]])
    else:
        p[1].exprs.append(p[3])
        p[0]=p[1]

def p_type_name(p):
    """ type_name   : specifier_qualifier_list abstract_declarator_opt
    """
    typename = ast.TypeName(
            name='',
            quals=p[1]['qual'],
            type=p[2] or ast.TypeDeclaration(None,None,None))
    p[0] = fix_decl_name_type(typename, p[1]['type'])

def p_abstract_declarator_1(p):
    """ abstract_declarator     : pointer
    """
    p[0]=modify_decl(ast.TypeDeclaration(None,None,None),p[1])

def p_abstract_declarator_2(p):
    """ abstract_declarator     : pointer direct_abstract_declarator
    """
    p[0]=modify_decl(p[2],p[1])

def p_abstract_declarator_3(p):
    """ abstract_declarator     : direct_abstract_declarator
    """
    p[0]=p[1]

# Creating and using direct_abstract_declarator_opt here
# instead of listing both direct_abstract_declarator and the
# lack of it in the beginning of _1 and _2 caused two
# shift/reduce errors.
#
def p_direct_abstract_declarator_1(p):
    """ direct_abstract_declarator  : LPAREN abstract_declarator RPAREN """
    p[0]=p[2]

def p_direct_abstract_declarator_2(p):
    """ direct_abstract_declarator  : direct_abstract_declarator LBRACKET constant_expression RBRACKET
    """
    arr = ast.ArrayDecl(type= ast.TypeDeclaration(None,None,None),dim=p[3])
    p[0]= modify_decl(decl=p[1],modifier=arr)

def p_direct_abstract_declarator_3(p):
    """ direct_abstract_declarator  : direct_abstract_declarator LBRACKET RBRACKET
    """
    arr = ast.ArrayDecl(type=ast.TypeDeclaration(None,None,None),dim= None)
    p[0]= modify_decl(decl=p[1],modifier=arr)

def p_direct_abstract_declarator_4(p):
    """ direct_abstract_declarator  : LBRACKET constant_expression RBRACKET
    """

    p[0] = ast.ArrayDecl(type= ast.TypeDeclaration(None,None,None),dim=p[2])

def p_direct_abstract_declarator_5(p):
    """ direct_abstract_declarator  : LBRACKET RBRACKET
    """

    p[0] = ast.ArrayDecl(type= ast.TypeDeclaration(None,None,None),dim=None)

def p_direct_abstract_declarator_6(p):
    """ direct_abstract_declarator  : direct_abstract_declarator LPAREN parameter_type_list_opt RPAREN
    """
    func = ast.FuncDecl(args=p[3],type=ast.TypeDeclaration(None,None,None))
    p[0]=modify_decl(decl=p[1],modifier=func)

def p_direct_abstract_declarator_7(p):
    """ direct_abstract_declarator  : LPAREN parameter_type_list_opt RPAREN
    """
    p[0]=ast.FuncDecl(args=p[2],type=ast.TypeDeclaration(None,None,None))

# declaration is a list, statement isn't. To make it consistent, block_item
# will always be a list
#
def p_block_item(p):
    """ block_item  : declaration
                    | statement
    """
    p[0]= p[1] if isinstance(p[1],list) else [p[1]]

# Since we made block_item a list, this just combines lists
#
def p_block_item_list(p):

    """ block_item_list : block_item
                        | block_item_list M_empty block_item
    """
    # Empty block items (plain ';') produce [None], so ignore them
    if len(p) == 2 or p[2] == None:
        p[0]= ast.BlockItemList(p[1])
        p[0].nextlist=p[1][-1].nextlist
        p[0].breaklist=p[1][-1].breaklist
        p[0].contlist=p[1][-1].contlist

    else: 
        TAC.backPatch(current_tac_table,p[1].nextlist,p[2]['quad'])
        p[0]= p[1]
        p[0].blockitems += p[3]
        p[0].breaklist=TAC.merge(p[0].breaklist,p[3][-1].breaklist)
        p[0].contlist=TAC.merge(p[0].contlist,p[3][-1].contlist)
        p[0].nextlist = p[3][-1].nextlist

def p_compound_statement(p):
    """ compound_statement : brace_open block_item_list_opt brace_close """
    p[0] = ast.CompStmt(block_items_list = p[2].blockitems if p[2] else p[2])
    if p[2]:
        p[0].nextlist = p[2].nextlist
        p[0].breaklist=p[2].breaklist
        p[0].contlist=p[2].contlist
        TAC.backPatch(current_tac_table,p[2].blockitems[-1].nextlist,
                        TAC.getNextQuad(current_tac_table))

def p_labeled_statement_1(p):
    """ labeled_statement : ID COLON M_empty statement """
    p[0]=ast.Labelled(p[1],p[4])
    if TAC.findLabel(current_tac_table,p[1]):
        error_func("label %s redeclared in function"%p[1])
    else:
        TAC.insertLabel(current_tac_table,p[1],p[3]['quad'])


def p_labeled_statement_2(p):
    """ labeled_statement : CASE constant_expression COLON statement """
    p[0]=ast.Case(p[2],p[4])

def p_labeled_statement_3(p):
    """ labeled_statement : DEFAULT COLON statement """
    p[0]=ast.Default(p[3])

def p_selection_statement_1(p):
    """ selection_statement : IF LPAREN expression RPAREN M_empty statement N1_empty"""
    p[0]=ast.If(p[3],p[6],None)
    TAC.backPatch(current_tac_table,p[3].exprs[-1].truelist,p[5]['quad'])

    p[0].nextlist= TAC.merge(p[3].exprs[-1].falselist,p[6].nextlist)
    p[0].breaklist=p[6].breaklist
    p[0].contlist=p[6].contlist

def p_selection_statement_2(p):
    """ selection_statement : IF LPAREN expression RPAREN M_empty statement N1_empty ELSE M_empty statement """
    p[0]=ast.If(p[3],p[6],p[10])
    TAC.backPatch(current_tac_table,p[3].truelist,p[5]['quad'])
    TAC.backPatch(current_tac_table,p[3].falselist,p[9]['quad'])
    p[0].nextlist= TAC.merge(p[6].nextlist,
                            TAC.merge(p[7]['nextlist'],p[10].nextlist))
    p[0].breaklist=TAC.merge(p[6].breaklist,p[10].breaklist)
    p[0].contlist=TAC.merge(p[6].contlist,p[10].contlist)


def p_selection_statement_3(p):
    """ selection_statement : SWITCH LPAREN expression RPAREN statement """
    p[0]=ast.Switch(p[3],p[5])

def p_iteration_statement_1(p):
    """ iteration_statement : WHILE M_empty LPAREN expression RPAREN M_empty statement"""
    p[0]=ast.While(p[4],p[7])
    TAC.backPatch(current_tac_table,p[7].nextlist,p[2]['quad'])
    TAC.backPatch(current_tac_table,p[7].contlist,p[2]['quad'])
    TAC.backPatch(current_tac_table,p[4].truelist,p[6]['quad'])
    p[0].nextlist=TAC.merge(p[4].falselist, p[7].breaklist)
    emit('','',p[2]['quad'],'GOTO')


def p_iteration_statement_2(p):
    """ iteration_statement : DO M_empty statement WHILE LPAREN M_empty expression RPAREN SEMI """
    p[0]=ast.DoWhile(p[5],p[2])
    TAC.backPatch(current_tac_table,p[7].truelist,p[2]['quad'])
    TAC.backPatch(current_tac_table,p[3].nextlist,p[6]['quad'])
    TAC.backPatch(current_tac_table,p[3].contlist,p[6]['quad'])
    p[0].nextlist = TAC.merge(p[7].falselist,p[3].breaklist)



#Assuming no declarations in for in C.
def p_iteration_statement_3(p):
    """ iteration_statement : FOR LPAREN expression_opt SEMI M_empty expression_opt SEMI M_empty expression_opt N_empty RPAREN M_empty statement """
    p[0]=ast.For([p[3]],p[5],p[7],p[9])
    if p[3]:
        TAC.backPatch(current_tac_table,p[3].nextlist,p[5]['quad'])
    if p[13]:
        TAC.backPatch(current_tac_table,p[13].nextlist,p[8]['quad'])
        TAC.backPatch(current_tac_table,p[13].contlist,p[8]['quad'])
    if p[6]:
        TAC.backPatch(current_tac_table,p[6].truelist,p[12]['quad'])
        p[0].nextlist=TAC.merge(p[6].falselist,p[13].breaklist)

    TAC.backPatch(current_tac_table,p[10]['nextlist'],p[5]['quad'])
    emit('','',p[8]['quad'],'GOTO')

def p_jump_statement_1(p):
    """ jump_statement  : GOTO ID SEMI """
    p[0]=ast.Goto(p[2])
    index = TAC.findLabel(current_tac_table,p[2])
    if index:
        emit('','',index,'GOTO')
    else:
        error_func("label %s not declared in function"%p[2])

def p_jump_statement_2(p):
    """ jump_statement  : BREAK SEMI """

    p[0]=ast.Break()
    p[0].breaklist=makelist(0)
    emit('','',-1,'GOTO')

def p_jump_statement_3(p):
    """ jump_statement  : CONTINUE SEMI """
    p[0]=ast.Continue();
    p[0].contlist=makelist(0)
    emit('','',-1,'GOTO')

def p_jump_statement_4(p):
    """ jump_statement  : RETURN expression SEMI
                        | RETURN SEMI
    """


    #Assume return accesses the last expression
    if len(p)==4:
        p[0]= ast.Return(p[2])
        emit(p[2].place,'','','RETURN')
    else:
        p[0]=ast.Return(None)
        emit('','','','RETURN')


####Handling I/O
def p_input_statement(p):
    """ input_statement : CIN input_list SEMI"""
    p[0]= ast.Input(p[2])
    for exp in p[2]:
        emit(exp.place,'','','SCAN')

def p_input_list(p):
    """input_list : INPSHIFT unary_expression
                    | input_list INPSHIFT unary_expression
    """
    if len(p)==3:
        p[0]=[p[2]]
    else:
        p[0]= p[1]
        p[0].append(p[3])

def p_output_statement(p):
    """ output_statement : COUT output_list SEMI"""
    p[0]= ast.Output(p[2])
    for exp in p[2]:
        if exp != 'endl':
            emit(exp.place,exp.exptype,'','PRINT')
        else:
            emit('','','','PRINT')

def p_output_list(p):
    """output_list : OUTSHIFT assignment_expression 
                    | output_list OUTSHIFT assignment_expression 
                    | OUTSHIFT ENDL
                    | output_list OUTSHIFT ENDL
    """
    if len(p)==3:
        p[0]=[p[2]]
    else:
        p[0]= p[1]
        p[0].append(p[3])

def p_expression_statement(p):
    """ expression_statement : expression_opt SEMI """
    p[0]=p[1]


def p_expression(p):
    """ expression  : assignment_expression
                    | expression COMMA M_empty assignment_expression
    """
    # expression is a node with a list of child expressions
    # Assume expression to attain the value of the last child
    if len(p) == 2:
        p[0] = ast.ExprList([p[1]],p[1].exptype)
        p[0].place = p[1].place
        p[0].nextlist = p[1].nextlist
        p[0].truelist = p[1].truelist
        p[0].falselist = p[1].falselist

    else:
        p[1].exprs.append(p[4])
        p[1].exptype = p[4].exptype
        p[1].place = p[4].place
        p[0]=p[1]
        p[0].truelist = p[4].truelist
        p[0].falselist = p[4].falselist
        TAC.backPatch(current_tac_table,p[1].nextlist,p[3]['quad'])

def p_M_empty(p):
    """ M_empty : empty """
    p[0] = {'quad' : TAC.getNextQuad(current_tac_table)}

def p_N_empty(p):
    """ N_empty : empty """
    p[0] = {'nextlist' : [TAC.getNextQuad(current_tac_table)]}
    emit('','',-1,'GOTO')

def p_N1_empty(p):
    """ N1_empty : empty """
    next_q = TAC.getNextQuad(current_tac_table)
    p[0] = {'quad':next_q,'nextlist' : [next_q]}
    if clex.last_token.type == 'ELSE':
        emit('','',-1,'GOTO')

def p_typedef_name(p):
    """ typedef_name : TYPEID """
    p[0]=ast.IdentifierType(p[1])

def p_assignment_expression(p):
    """ assignment_expression   : conditional_expression
                                | unary_expression assignment_operator assignment_expression
    """
    if len(p) == 2:
        p[0] = p[1]
    else:
        v1 = p[1].exptype
        v2 = p[3].exptype
        if v1 == 'string' or v2 =="string":
            error_func("Cannot do string assignments")
        elif check_type_equiv(v1,v2):
            p[0] = ast.AssExpr(p[2],p[1],p[3],v1)
            quad_next= TAC.getNextQuad(current_tac_table)
            TAC.backPatch(current_tac_table,p[3].truelist,quad_next)
            TAC.backPatch(current_tac_table,p[3].falselist,quad_next)
            if p[2]=='=':
                emit(p[1].place,
                    p[3].place,
                    '',
                    '=')
            else:
                temp = temp_call()
                if p[2] in ('^=','*=','+=','-=','/=','&=','|='):
                    emit(temp,
                        p[1].place,
                        p[3].place,
                        p[2][0])
                elif p[2] in ('<<=','>>='):
                    emit(temp,
                        p[1].place,
                        p[3].place,
                        p[2][:2])
                emit(p[1].place,
                    temp,
                    '',
                    '=')

            p[0].place = p[1].place
            p[0].nextlist = []
        else:
            error_func('Array/Basic Type mismatch in Assignment Expression') 

        # elif isinstance(v1, ast.ArrayDecl) or isinstance(v2, ast.ArrayDecl):
        #     if isinstance(v1, ast.ArrayDecl) and isinstance(v2, ast.ArrayDecl):
        #         p[0] = ast.AssExpr(p[2],p[1],p[3],v1)
        #     else:
        #         error_func('Array/Basic Type mismatch in Assignment Expression') 
        # else:
        #     p[0] = ast.AssExpr(p[2],p[1],p[3],v1)

def p_assignment_operator(p):
    """ assignment_operator : EQUALS
                            | XOREQUAL
                            | TIMESEQUAL
                            | DIVEQUAL
                            | MODEQUAL
                            | PLUSEQUAL
                            | MINUSEQUAL
                            | LSHIFTEQUAL
                            | RSHIFTEQUAL
                            | ANDEQUAL
                            | OREQUAL
    """
    #
    p[0]=p[1]

def p_constant_expression(p):
    """ constant_expression : conditional_expression """
    p[0] = p[1]

def p_conditional_expression(p):
    """ conditional_expression  : binary_expression
                                | binary_expression CONDOP M_empty expression N_empty COLON M_empty conditional_expression M_empty
    """
    if len(p) == 2:
        p[0] = p[1]
    else:
        v1 = p[4].exprs[-1].exptype
        v2 = p[8].exptype
        if isinstance(v1, ARR_PTR_TYPES) or isinstance(v2, ARR_PTR_TYPES):
            if check_type_equiv(v1,v2):
                p[0] = ast.TernaryOp(p[1], p[4], p[8], v1 )            
            else:
                error_func('Pointer/Basic Type Mismatch in expression')
        else:
            if v1 in FLOAT_TYPES or v2 in FLOAT_TYPES: 
                p[0] = ast.TernaryOp(p[1], p[4], p[8],'float')
            else:
                p[0] = ast.TernaryOp(p[1], p[4], p[8],'int')

        TAC.backPatch(current_tac_table,p[1].truelist,p[3]['quad'])
        TAC.backPatch(current_tac_table,p[1].falselist,p[7]['quad'])
        p[0].place = temp_call()
        emit(p[0].place,p[8].place,'','=')
        emit('','',p[9]['quad']+3,'GOTO')
        emit(p[0].place,p[4].place,'','=')
        TAC.backPatch(current_tac_table,p[5]['nextlist'],p[9]['quad']+2)



def p_binary_expression_1(p):
    """ binary_expression   : cast_expression """
    p[0] = p[1]
    if is_ptr(p[1].place):
        if  not isinstance(p[1],ast.ArrayRef) or (p[1].depth + 1) == len(p[1].dims):
            emit(p[0].place,p[1].place,'','=REF')

def p_binary_expression_2(p):
    """ binary_expression   : binary_expression TIMES binary_expression
                            | binary_expression DIVIDE binary_expression
                            | binary_expression MOD binary_expression
                            | binary_expression PLUS binary_expression
                            | binary_expression MINUS binary_expression
                            | binary_expression RSHIFT binary_expression
                            | binary_expression LSHIFT binary_expression
    """
    optype = check_exp(p[2], p[1].exptype, p[3].exptype)

    if optype:
        p[0] = ast.BinaryOp(p[2], p[1], p[3], optype)
        p[0].place =temp_call()
        emit(p[0].place,
            p[1].place,
            p[3].place,
            p[2])
    else:
        error_func("Invalid Types: ")

def p_binary_expression_3(p):
    """ binary_expression   : binary_expression LT binary_expression
                            | binary_expression LE binary_expression
                            | binary_expression GE binary_expression
                            | binary_expression GT binary_expression
                            | binary_expression EQ binary_expression
                            | binary_expression NE binary_expression
                            | binary_expression AND binary_expression
                            | binary_expression OR binary_expression
    """
    optype = check_exp(p[2], p[1].exptype, p[3].exptype)

    if optype: 
        p[0] = ast.BinaryOp(p[2], p[1], p[3], optype)
        exp = [p[1].place,p[2],p[3].place]
        p[0].place = temp_call()
        quad_next = TAC.getNextQuad(current_tac_table)
        TAC.backPatch(current_tac_table,p[1].truelist,quad_next)
        TAC.backPatch(current_tac_table,p[1].falselist,quad_next)
        emit(p[0].place,p[1].place,p[3].place,p[2])

        p[0].falselist =  makelist(0)
        p[0].truelist = makelist(1)


        emit(p[0].place,'',-1,'COND_GOTO_Z')
        emit('','',-1,'GOTO')
    else:
        error_func("Invalid Types: ")

def p_binary_expression_4(p):
    """ binary_expression   : binary_expression XOR M_empty binary_expression
                            | binary_expression LAND M_empty binary_expression
                            | binary_expression LOR M_empty binary_expression
    """

    optype = check_exp(p[2], p[1].exptype, p[4].exptype)
    if optype: 
        p[0] = ast.BinaryOp(p[2], p[1], p[4], optype)
        # p[0].place =temp_call()
        if p[2] == '||':
            TAC.backPatch(current_tac_table,p[1].falselist,p[3]['quad'])
            p[0].truelist = TAC.merge(p[1].truelist,p[4].truelist)
            p[0].falselist = p[4].falselist
        elif p[2] == '&&':
            TAC.backPatch(current_tac_table,p[1].truelist,p[3]['quad'])
            p[0].truelist = p[4].truelist
            p[0].falselist = TAC.merge(p[1].falselist,p[4].falselist)
    else:
        error_func("Invalid Types: ")

def p_cast_expression_1(p):
    """ cast_expression : unary_expression """
    p[0] = p[1]

def p_cast_expression_2(p):
    """ cast_expression : LPAREN type_name RPAREN cast_expression """
    
    if check_castability(p[2].type,p[4].exptype):
        p[0] = ast.Cast(p[2],p[4],p[2])
    else:
        error_func('Incompatible cast expression')

def p_unary_expression_1(p):
    """ unary_expression    : postfix_expression """
    if isinstance(p[1], ast.ID):
        if isinstance(p[1].exptype,ast.ArrayDecl):
            exp_scope= find_in_scope(p[1].name)
            arr_dims = find_arr_dims(exp_scope['type']['type'])
            new_temp = temp_call(make_ptr=True)
            zero_temp = temp_call()
            emit(zero_temp,0,'','=i')
            new_name = exp_scope['name']
            emit(new_temp,new_name,zero_temp,'=Arr')
            p[0]=ast.ArrayRef(prev=p[1],
                            index=0, 
                            exptype = p[1].exptype,
                            name = p[1].name,
                            dims = arr_dims,
                            depth = 0,
                            loffset = zero_temp)
            p[0].place = new_temp
        elif isinstance(p[1].exptype,ast.StructOrUnion):
            new_name = find_in_scope(p[1].name)['name']
            p[0]=ast.StructRef(new_name,'','',p[1].exptype)
            p[0].place = temp_call()
            emit(p[0].place,new_name,'','=Struct')
        else:
            p[0]=p[1]
    else:
        p[0]=p[1]

def p_unary_expression_2(p):
    """ unary_expression    : PLUSPLUS unary_expression
                            | MINUSMINUS unary_expression
                            | unary_operator cast_expression
    """
    if p[1] == "&" :
        ptr_type = ast.PtrDecl(quals=None,type=p[2].exptype)
        p[0] = ast.UnaryOp(p[1],p[2],ptr_type)
        p[0].place = temp_call()
        emit(p[0].place,p[2].place,'','&Ptr')

    elif p[1] == "*" :
        if isinstance(p[2].exptype,(ast.ArrayDecl,ast.PtrDecl)):
            exptype = p[2].exptype.type if not isinstance(p[2].exptype.type,ast.TypeDeclaration) else p[2].exptype.type.type.name
            p[0] = ast.UnaryOp(p[1],p[2],exptype)
            p[0].place= temp_call(make_ptr=True)
            emit(p[0].place,p[2].place,'','*Deref')

        else:
            error_func("Cannot dereference non-pointer type")

    elif p[2].exptype in INT_TYPES:
        p[0]= ast.UnaryOp(p[1],p[2],'int')
        p[0].place = temp_call()
        emit(p[0].place,p[2].place,'','u'+p[1])
    elif isinstance(p[2].exptype, ast.PtrDecl):
        p[0] == ast.UnaryOp(p[1],p[2],p[2].exptype)
    else:
        error_func("Type Error")

def p_unary_expression_3(p):
    """ unary_expression    : SIZEOF unary_expression
                            | SIZEOF LPAREN type_name RPAREN
    """
    if len(p)==3:
        p[0]=UnaryOp(p[1],p[2],'int')
    else:
        p[0]=UnaryOp(p[1],p[3],'int')

def p_unary_operator(p):
    """ unary_operator  : AND
                        | TIMES
                        | PLUS
                        | MINUS
                        | NOT
                        | LNOT
    """
    p[0]=p[1]

def p_postfix_expression_1(p):
    """ postfix_expression  : primary_expression """
    if p[1].exptype is None:
        exp_scope= find_in_scope(p[1].name)['type']['type']
        if isinstance(exp_scope, (ast.TypeDeclaration)):
            if isinstance(exp_scope.type, ast.IdentifierType):
                p[1].exptype = exp_scope.type.name
            else:
                p[1].exptype = exp_scope.type
        else:
            p[1].exptype = exp_scope 
    p[0] = p[1]





def find_arr_dims(arr_type):
    size=[]
    while(1):
        if isinstance(arr_type,ast.ArrayDecl):
            size.append(int(arr_type.dim.value))
        else:
            if isinstance(arr_type,ast.PtrDecl):
                size.append(8)
            elif isinstance(arr_type.type,ast.IdentifierType):
                arr_type_name = arr_type.type.name
                if arr_type_name in ('int','float'):
                    size.append(4)
                elif arr_type_name in ('double'):
                    size.append(8)
                elif arr_type_name in ('char'):
                    size.append(4)
            elif isinstance(arr_type.type,ast.StructOrUnion):
                arr_type_name = arr_type.type.name
                struct_size = find_in_usertype_scope(arr_type_name)['size']
                size.append(struct_size)
            return size
        arr_type = arr_type.type


def p_postfix_expression_2(p):
    """ postfix_expression  : postfix_expression LBRACKET expression RBRACKET """

    # expression is a list of assignment expressions
    # Handles array references
    # Checks whether value is int and within the dimensions
    if p[3].exprs[-1].exptype in INT_TYPES:
        if isinstance(p[3].exprs[-1], ast.Constant) and int(p[3].exprs[-1].value) >= int(p[1].exptype.dim.value):
            error_func('Array index out of bound')   
        if isinstance(p[1].exptype.type, (ast.TypeDeclaration)):
            type_exp = p[1].exptype.type.type.name
        else:    
            type_exp = p[1].exptype.type
    else:
        error_func('Array Index should be of int type')

    if isinstance(p[1],ast.ID):
        arr_type_info = find_in_scope(p[1].name)['type']['type']
        arr_dims = find_arr_dims(arr_type_info)
        new_temp = temp_call()
        new_temp_1 = temp_call()
        new_temp_2 = temp_call(make_ptr=True)
        dim_temp = temp_call()
        emit(dim_temp,arr_dims[1],'','=i')
        emit(new_temp,dim_temp,p[3].place,'*')
        if clex.last_token.type != 'LBRACKET':
            remaining_dims = arr_dims[2:]
            if remaining_dims:
                remaining_size = reduce(lambda x, y: x*y, remaining_dims)
                dim_temp_2 = temp_call()
                emit(dim_temp_2,remaining_size,'','=i')
                emit(new_temp,new_temp,dim_temp_2,'*')
            new_name = find_in_scope(p[1].name)['name']
            emit(new_temp_2,new_name,new_temp,'=Arr')
        p[0]=ast.ArrayRef(prev=p[1],
                        index=p[3], 
                        exptype = type_exp,
                        name = p[1].name,
                        dims = arr_dims,
                        depth = 1,
                        loffset = new_temp)
        p[0].place = new_temp_2

    
    elif isinstance(p[1],ast.ArrayRef):
        new_temp = temp_call()
        emit(new_temp,p[1].loffset,p[3].place,'+')
        arr_dims = p[1].dims
        new_temp_1 = temp_call()
        new_temp_2 = temp_call(make_ptr=True)
        if clex.last_token.type != 'LBRACKET':
            remaining_dims = arr_dims[p[1].depth+1:]
            remaining_size = reduce(lambda x, y: x*y, remaining_dims)
            dim_temp_2 = temp_call()
            emit(dim_temp_2,remaining_size,'','=i')
            emit(new_temp_1,new_temp,dim_temp_2,'*')

            new_name = find_in_scope(p[1].name)['name']

            emit(new_temp_2,new_name,new_temp_1,'=Arr')
        else:
            next_inner_dim = arr_dims[p[1].depth+1]
            dim_temp_2 = temp_call()
            emit(dim_temp_2,next_inner_dim,'','=i')
            emit(new_temp_1,new_temp,dim_temp_2,'*')

        p[0]=ast.ArrayRef(prev=p[1],
                        index=p[3], 
                        exptype = type_exp,
                        name = p[1].name,
                        dims = p[1].dims,
                        depth = p[1].depth+1,
                        loffset = new_temp_1)
        p[0].place = new_temp_2

def p_postfix_expression_3(p):
    """ postfix_expression  : ID LPAREN argument_expression_list RPAREN
                            | ID LPAREN RPAREN
    """
    global implicit_decls
    p[1] = ast.ID(p[1])
    func_declared = find_in_scope(p[1].name)
    if func_declared==False:
        warning_func("Implicit declaration of function-> %s"%p[1].name)
        ret_type = 'int'
        if len(p) == 5:
            implicit_decls.append((p[1].name,len(p[3].exprs),clex.lexer.lineno))
            p[0] = ast.FuncCall(p[1],p[3],ret_type)
            for expr in p[3].exprs:
                emit(expr.place,'','','PARAM')
        else:
            p[0] = ast.FuncCall(p[1],None,ret_type)
            implicit_decls.append((p[1].name,0,clex.lexer.lineno))
    else:
        func = func_declared['type']['type']
        if isinstance(func.type, ast.TypeDeclaration):
            ret_type = func.type.type.name
        else:
            ret_type = func.type
        params_len = len(func.args.params)  
        if len(p)==5:

            if len(p[3].exprs) == params_len:
                p[0]=ast.FuncCall(p[1],p[3],ret_type)
            #handling ellipsis
            elif isinstance(func.args.params[-1],ast.Ellipsis) and len(p[3].exprs)>=params_len-1:
                p[0]=ast.FuncCall(p[1],p[3],ret_type)
            else:
                error_func("Incorrect arguments in Function Call")

            for expr in p[3].exprs:
                emit(expr.place,'','','PARAM')
        else:
            if params_len == 0:
                # ret_type = func.type.type.name
                p[0]=ast.FuncCall(p[1],None, ret_type)

            else:
                error_func("No arguments specified in Function call")
    emit ('','',p[1].place,'CALL')
    p[0].place = temp_call()
    emit(p[0].place,'','','RET_VAL')


def p_postfix_expression_4(p):
    """ postfix_expression  : postfix_expression PERIOD ID
                            | postfix_expression PERIOD TYPEID
                            | postfix_expression ARROW ID
                            | postfix_expression ARROW TYPEID
    """
    struct_name = find_in_scope(p[1].name)['type']['type'].type.name
    struct_decls = find_in_usertype_scope(struct_name)['decls']
    if struct_decls.has_key(p[3]):
        exp_type_name = struct_decls.get(p[3])
        if isinstance(exp_type_name['type']['type'], (ast.TypeDeclaration)):
            p[1].exptype = exp_type_name['type']['type'].type.name
        else:
            p[1].exptype = exp_type_name['type']['type']
        new_var = ast.ID(p[3])
        p[0]=ast.StructRef(p[1],p[2],new_var,p[1].exptype)
        p[0].place = temp_call(make_ptr =True )

        emit(p[0].place,p[1].place,p[3],'dot')
    else:
        error_func("Access to unknown field ->%s"%p[3])


def p_postfix_expression_5(p):
    """ postfix_expression  : postfix_expression PLUSPLUS
                            | postfix_expression MINUSMINUS
    """
    if p[1].exptype in INT_TYPES:
        p[0]= ast.UnaryOp('p'+p[2],p[1],'int')
        p[0].place = temp_call()
        emit(p[0].place,p[1].place,'','p'+p[2])
    else:
        error_func("Type Error")


def p_primary_expression_1(p):
    """ primary_expression  : identifier """
    p[0] = p[1]

def p_primary_expression_2(p):
    """ primary_expression  : constant """
    p[0] = p[1]

def p_primary_expression_3(p):
    """ primary_expression  : unified_string_literal
    """
    p[0] = p[1]

def p_primary_expression_4(p):
    """ primary_expression  : LPAREN expression RPAREN """
    p[0] = p[2]


def p_argument_expression_list(p):
    """ argument_expression_list    : assignment_expression
                                    | argument_expression_list COMMA assignment_expression
    """
    if len(p) == 2: # single expr
        #ExprList type is taken same as that of the last expression
        p[0] = ast.ExprList([p[1]],p[1].exptype)
    else:
        p[1].exprs.append(p[3])
        p[1].exptype = p[3].exptype
        p[0] = p[1]

def p_identifier(p):
    """ identifier  : ID """
    # check if id has been declared previously.

    #assuming no typedefs for now.
    id_found = find_in_scope(p[1])
    if id_found:
        p[0]=ast.ID(p[1])
        p[0].place = id_found['name']
    else:
        error_func("identifier %s not declared in current scope" %p[1])


def p_constant_1(p):
    """ constant    : INT_CONST_DEC
                    | INT_CONST_OCT
                    | INT_CONST_HEX
                    | INT_CONST_BIN
    """
    p[0]=ast.Constant('int',p[1],'int')
    p[0].place = temp_call()
    emit(p[0].place,p[1],'int','=i')

def p_constant_2(p):
    """ constant    : FLOAT_CONST
                    | HEX_FLOAT_CONST
    """
    p[0]=ast.Constant('float',p[1],'float')
    p[0].place = temp_call()
    emit(p[0].place,p[1],'float','=i')

def p_constant_3(p):
    """ constant    : CHAR_CONST """
    #
    p[1]=p[1][1]
    p[0]=ast.Constant('char',p[1],'char')
    p[0].place = temp_call()
    emit(p[0].place,p[1],'char','=i')


def p_unified_string_literal(p):
    """ unified_string_literal  : STRING_LITERAL
                                | unified_string_literal STRING_LITERAL
    """
    if len(p) == 2: # single literal
        p[0]=ast.Constant('string',p[1],'string')
        p[0].place = temp_call()
        global_string[p[0].place + '_'+current_symbol_table.name] = p[1]
    else:
        p[1].value = p[1].value[:-1]+p[2][1:]
        p[0]=p[1]
        global_string[p[0].place + '_'+current_symbol_table.name] = p[1].value


# def p_unified_wstring_literal(p):
#     """ unified_wstring_literal : WSTRING_LITERAL
#                                 | unified_wstring_literal WSTRING_LITERAL
#     """
#     if len(p) == 2: # single literal
#         p[0]=ast.Constant('string',p[1],'string')
#     else:
#         p[1].value = p[1].value[:-1]+p[2][2:]
#         p[0]=p[1]

def p_brace_open(p):
    """ brace_open  :   LBRACE
    """
    # push_scope()
    p[0]=p[1]


def p_brace_close(p):
    """ brace_close :   RBRACE
    """
    pop_scope()
    p[0]=p[1]


def p_empty(p):
    'empty : '
    p[0] = None 

def p_error(p):
    # If error recovery is added here in the future, make sure
    # _get_yacc_lookahead_token still works!
    #
    if p:
        error_func('at: %s' % p.value)
    else:
        print 'Error: At end of input', inFile


def make_TAC_ST(inFile):
    cparser=yacc.yacc(outputdir=taboutputdir,debug=0)

    with open(inFile,'r') as i:
        text = i.read()
        ast_tree = cparser.parse(input=text,lexer=clex)
        print_tables()
        # print_ast(ast_tree)
        TAC.addLabels()
        TAC.stringDict = global_string
        TAC.printCode()

    return TAC, current_symbol_table

if __name__ == "__main__":

    cparser=yacc.yacc(outputdir=taboutputdir,debug=0)

    with open(inFile,'r') as i:
        text = i.read()
        ast_tree = cparser.parse(input=text,lexer=clex)
        print_tables()
        # print_ast(ast_tree)
        TAC.printCode()
    if len(sys.argv)==3:
        f.close()