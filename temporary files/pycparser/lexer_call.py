from c_lexer import CLexer
import sys

inFile=sys.argv[1]

def error_func(err_msg, line, col):
    print("Got an error: {} \n line: {} col: {} ".format(err_msg, line, col))

def look_up_func(name):
    return False;

lexer = CLexer(error_func,None,None,look_up_func)

lexer.build()

with open(inFile,'r') as i:
    text = i.read()
lexer.input(text)
while True:
	tok= lexer.token()
	if not tok:
		break
	print tok

