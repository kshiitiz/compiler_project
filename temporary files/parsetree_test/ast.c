# include <stdio.h>
# include <stdlib.h>
# include <stdarg.h>
# include "ast.h"
int id=0;
struct node *
newast(int nodetype, struct node *one, struct node *two,struct node *three,struct node* four)
{
 struct node *a = malloc(sizeof(struct ast));

 if(!a) {
 yyerror("out of space");
 exit(0);
 }
 a->nodetype = nodetype;
 a->l = l;
 a->r = r;
 a->nodeid=getid();
 // printf("%d  %d  %d\n",a->nodeid,a->l->nodeid,a->r->nodeid);
 return a;
}
struct ast *
newnum(double d)
{
 struct numval *a = malloc(sizeof(struct numval));
  if(!a) {
 yyerror("out of space");
 exit(0);
 }
 a->nodetype = 'K';
 a->number = d;
 a->nodeid=getid();
 printf("num: %d\n",a->nodeid);
 return (struct ast *)a;
}
int getid(){
	return id++;
}
void
eval(struct ast *a)
{
/* double v; 
 switch(a->nodetype) {
 case 'K': v = ((struct numval *)a)->number; break;
 case '+': v = eval(a->l) + eval(a->r); break;
 case '-': v = eval(a->l) - eval(a->r); break;
 case '*': v = eval(a->l) * eval(a->r); break;
 case '/': v = eval(a->l) / eval(a->r); break;
 case '|': v = eval(a->l); if(v < 0) v = -v; break;
 case 'M': v = -eval(a->l); break;
 default: printf("internal error: bad node %c\n", a->nodetype);
 }
 return v;*/

	//printf("nodeid: %d, left: %d,right : %d\n",a->nodeid,a->l->nodeid,a->r->nodeid);
	switch(a->nodetype){
		case '+' : printf("%d [label=\"+\"]\n%d --  %d\n" ,a->nodeid, a->nodeid, a->l->nodeid ); 
				   printf(" %d --  %d\n" , a->nodeid, a->r->nodeid ); 
				   eval(a->l);
				   eval (a->r);
		break;
		case 'K': printf("%d [label=\"%f\"]\n",a->nodeid,((struct numval *)a)->number);
			break;
	}
}
void
treefree(struct ast *a)
{
 switch(a->nodetype) {
 /* two subtrees */
 case '+':
 case '-':
 case '*':
 case '/':
 treefree(a->r);
 /* one subtree */
 case '|':
 case 'M':
 treefree(a->l);
 /* no subtree */
 case 'K':
 free(a);
 break;
 default: printf("internal error: free bad node %c\n", a->nodetype);
 }
}

void
yyerror(char *s, ...)
{
 va_list ap;
 va_start(ap, s);
 fprintf(stderr, "%d: error: ", yylineno);
 vfprintf(stderr, s, ap);
 fprintf(stderr, "\n");
}
int
main()
{
 printf("> ");
 return yyparse();
}