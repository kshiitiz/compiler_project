
	
	// BISON file example1 .y
%{
# include <stdio.h>
# include <stdlib.h>
# include <string>
# include <map>
# include <stdio.h>
# include <stdlib.h>
# include "tree.h"
using namespace std ;
map < string , double > vars ; // map from variable name to value
extern int yylex ();
void yyerror (const char *);
void Div0Error ( void );
void UnknownVarError ( string s );
%}


%union {
struct node* n;
int i;
double d ;
std::string* str_val ;
}

%token < i > PLUS MINUS ASTERISK FSLASH EQUALS PRINT LPAREN RPAREN SEMICOLON
%token < str_val > VARIABLE
%token < d > NUMBER

%type < n > expression parsetree lines line ;
%type < n > inner1 ;
%type < n > inner2 ;

%start parsetree


%%

parsetree : lines { $$=newnode("parsetree",$1,NULL,NULL); eval($$);} ;


lines : lines line { $$=newnode("lines",$1,$2,NULL); } 
| line{ $$=newnode("lines",$1,NULL,NULL);};

line : PRINT expression SEMICOLON {
struct node * p =newnode ("PRINT",NULL,NULL,NULL);
struct node * s =newnode ("SEMICOLON",NULL,NULL,NULL);
$$=newnode("line",p,$2,s);
};/*
| VARIABLE EQUALS expression SEMICOLON {
struct node * v =newnode ($1,NULL,NULL,NULL);
struct node * e =newnode ("EQUALS",NULL,NULL,NULL);
struct node * s =newnode ("SEMICOLON",NULL,NULL,NULL);
$$=newnode("line",v,e,$3,s);
};*/

expression : expression PLUS inner1 { struct node * p =newnode ("PLUS",NULL,NULL,NULL);
$$=newnode("expression",$1,p,$3);
}

| expression MINUS inner1 { struct node * m =newnode ("MINUS",NULL,NULL,NULL);
$$=newnode("expression",$1,m,$3);}
| inner1 { 
	$$=newnode("expression",$1,NULL,NULL);
};


inner1 : inner1 ASTERISK inner2 { struct node * a=newnode("ASTERISK",NULL,NULL,NULL);
$$=newnode("inner1",$1,a,$3);}
| inner1 FSLASH inner2 
{if( $3 == 0) Div0Error (); 
	else {struct node * f=newnode("FSLASH",NULL,NULL,NULL);
$$=newnode("inner1",$1,f,$3);}}
| inner2 { $$=newnode("inner1",$1,NULL,NULL);};

inner2 : VARIABLE
{ struct node * v=newnode(*($1),NULL,NULL,NULL);
 $$=newnode("inner2",v,NULL,NULL);};
| NUMBER { 
	char str[15];
	sprintf(str,"%lf",$1);
struct node * n=newnode(str,NULL,NULL,NULL);
 $$=newnode("inner2",n,NULL,NULL);}
| LPAREN expression RPAREN { $$ = $2 ;};

%%

void Div0Error ( void ) { printf (" Error : division by zero \n"); exit (0);}
void UnknownVarError ( string s ) { printf (" Error : %s does not exist !\n", s . c_str ()); exit (0);}