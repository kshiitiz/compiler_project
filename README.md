# C Compiler In Python
## Milestone 1:
### Implementation Details

We are using PLY (Python-Lex-Yacc) for lexing and parsing code. The actions specified in the parser generate a .dot file which is compiled using the Dot tool to generate a parse-tree. 

### Usage

To generate .dot file:
python lexer_parser.py <input_file_name> [output_file_name]

To get corresponding graph:
dot -Tps <file_name.dot> -o graph.ps

## Milestone 2:
### Implementation Details

We now modify the parser to generate the required AST and symbol tables. The output files are:
1)ast.txt - Contains AST in txt format
2)symbol.csv - CSV Dump of Symbol Tables
3)ast.dot - DOT output for AST

### Usage

To generate .dot file:
python tree_parser.py <input_file_name>

To get corresponding graph:
dot -Tps ast.dot -o <outputimage.ps>

##References:

Grammar and Lexer taken from -
[https://github.com/eliben/pycparser/tree/master/pycparser](https://github.com/eliben/pycparser/tree/master/pycparser)


