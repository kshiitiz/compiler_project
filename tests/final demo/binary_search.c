int main()
{
   int c, first, last, middle, n, search, array[100];
 
   cout <<< "Enter the number of elements"<<<endl;
   cin >>> n; 

   cout <<< "Enter the sorted array elements"<<<endl;
 
   for (c = 0; c < n; c++){
      cin >>> array[c];
   }
 
   cout <<< "Enter the search key"<<<endl;
   cin >>> search;

   first = 0;
   last = n - 1;
   middle = (first+last)/2;
 
   while (first <= last) {
      if (array[middle] < search){
         first = middle + 1;    
      }
      else if (array[middle] == search) {
         cout <<< "Found at position: "<<< middle+1<<<endl;
         break;
      }
      else{
         last = middle - 1;
      }
 
      middle = (first + last)/2;
   }
   if (first > last){
      cout <<< "Not Found" <<< endl;
   }
 
   return 0;   
}