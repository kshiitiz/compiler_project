from collections import OrderedDict
class ThreeAC:
    def __init__ (self,ST=None):
        self.ST = ST
        self.code = OrderedDict()
        self.quad= OrderedDict()
        self.nextquad= OrderedDict()
        self.code['GLOBAL']=[]
        self.quad['GLOBAL']= -1
        self.nextquad['GLOBAL'] = 0
        self.labels = OrderedDict()
        self.stringDict = None

    def getNextQuad(self,currentFunc):
        return self.nextquad[currentFunc]

    def addQuad(self,currentFunc):
        self.quad[currentFunc]=self.nextquad[currentFunc]
        self.nextquad[currentFunc]=self.nextquad[currentFunc]+1
        return self.quad[currentFunc]

    def getCodeLen(self,currentFunc):
        return self.quad[currentFunc]

    def emit(self,currentScope,currentFunc,Dest,src1,src2,op):
        self.addQuad(currentFunc)
        self.code[currentFunc].append([Dest,src1,src2,op,currentScope])

    def newFuncCode(self,currentFunc):
        self.code[currentFunc]=[]
        self.quad[currentFunc]=-1
        self.nextquad[currentFunc]=0
        self.labels[currentFunc]=dict()

    def insertLabel(self,currentFunc,label,index):
        self.labels[currentFunc][label]=index

    def findLabel(self,currentFunc,label):
        index = self.labels[currentFunc].get(label,None)
        return index


        # Function to merge two lists
    def merge(self, l1, l2):
        l3 = list(l1)
        l3.extend(l2)
        return l3

    # Function to backpatch
    def backPatch(self, currentFunc, locationList, location):
        # print "here",currentFunc, locationList, location
        for position in locationList:
            self.code[currentFunc][position][2] = location

    # Function to print code
    def printCode(self, fileName=''):
        if fileName != '':
            f = open('log/' + fileName + '.log', 'w')
            for functionName in self.code.keys():
                f.write("\n%s:\n" %functionName)
                for i in range(len(self.code[functionName])):
                    codePoint = self.code[functionName][i][:-1]
                    f.write("%5d: \t%s\n" %(i, codePoint))
            f.close()
        else:
            for functionName in self.code.keys():
                print "\n%s:" %functionName
                for i in range(len(self.code[functionName])):
                    codePoint = self.code[functionName][i][:-1]
                    print "%5d: \t%s" %(i, codePoint)

 # Patch all the labels in the functions
    def addLabels(self):
        for functionName in self.code.keys():
            labelcount=0
            unresolvedLabels = {}
            for line in self.code[functionName]:
                if line[3] in ['COND_GOTO_Z', 'GOTO']:
                    # Generate label
                    # print line
                    if unresolvedLabels.has_key(line[2]):
                        label = unresolvedLabels[line[2]]
                    else:

                        label = functionName+'_'+str(labelcount)
                        unresolvedLabels[line[2]] = label
                        labelcount += 1

                    line[2] = label

            lineNumber = -1
            count = 0
            for line in range(len(self.code[functionName])):
                lineNumber += 1

                if lineNumber in unresolvedLabels.keys():
                    effectiveLineNumber = lineNumber + count
                    self.code[functionName].insert(effectiveLineNumber, ['LABEL', unresolvedLabels[lineNumber], '', ''])
                    count += 1
                    del unresolvedLabels[lineNumber]
