from collections import OrderedDict
class RunCode:
    def __init__(self,ThreeAC,SymTab):
        self.ThreeAC = ThreeAC
        self.SymTabs = dict()
        self.SymTabs['GLOBAL']=SymTab
        for Sym_Tab in SymTab.children:
            self.SymTabs[Sym_Tab.name]=Sym_Tab
        self.code = {}
        self.currentFunc = None
        self.registerDescriptor = OrderedDict()
        self.freeAll()

    def freeAll(self):
        for x in range(10):
            self.registerDescriptor['$t'+str(x)]= None
        for x in range(5):
            self.registerDescriptor['$s'+str(x)]= None
        self.freeReg = [reg for reg in self.registerDescriptor.keys()]
        self.regInUse = []

    def flushAll(self):
        for temp in self.registerDescriptor.keys():
            self.flushRegister(temp)

    def makeFunc(self,func):
        self.code[func]=[]
        self.currentFunc=func

    def addCode(self,code):
        self.code[self.currentFunc].append(code)

    def dumpCode(self,file=''):
        if file != '':
            # Open the file
            f_out = open(file + '.asm', 'w')

            # Write out the data
            const_data = open('utilities/const.asm').read()
            f_out.write(const_data)

            # Print the strings in the file
            for key, value in self.ThreeAC.stringDict.iteritems():
                f_out.write('\t%s:\t\t\t.asciiz\t\t%s\n' %(key, value))

            # Start of the code
            f_out.write('\n.text\n')

            # For each function, we have to print the data
            for functionName in self.code.keys():
                f_out.write("\n%s:\n" %functionName)
                for i in range(len(self.code[functionName])):
                    line = self.code[functionName][i]
                    if line[0] == 'LABEL':
                        f_out.write("%s:\n" %line[1])
                    elif line[1] == '':
                        f_out.write("\t%s\n" %line[0])
                    elif line[2] == '':
                        f_out.write("\t%s\t\t%s\n" %(line[0], line[1]))
                    elif line[3] == '':
                        f_out.write("\t%s\t\t%s,\t%s\n" %(line[0], line[1], line[2]))
                    elif line[0] == '#':
                        f_out.write(line+'\n')
                    else:
                        f_out.write("\t%s\t\t%s,\t%s,\t%s\n" %(line[0], line[1], line[2], line[3]))

            # Write out the libraray routines
            data = open('utilities/code.asm').read()
            f_out.write(data)

            # CLose the file
            f_out.close()

        else:
            for funcName in self.code.keys():
                print "\n%s:" %funcName
                for i in range(len(self.code[funcName])):
                    line = self.code[funcName][i]
                    print "\t%s\t%s\t%s\t%s" %(line[0], line[1], line[2], line[3])

    def getReg(self,temp):
        # Check if already in register
        new_reg = None
        symtab = self.SymTabs[self.currentFunc]

        if temp in self.registerDescriptor.values():
            new_reg = symtab.addressDescriptor[temp]['register']

        else:
            
            if len(self.freeReg) > 0:
                # load value of temporary from memory into register if temporary has been stored previously
                new_reg = self.freeReg.pop()

                # print symtab.addressDescriptor[temp]
                if symtab.addressDescriptor[temp]['memory']!=None and symtab.addressDescriptor[temp]["store"]:
                    # load value of activation record where temporary must be stored
                    offset=symtab.addressDescriptor[temp]['memory']

                    #Loading value of temporary into register

                    #find location of temporary in activation record
                    self.addCode(['li','$s6',offset,''])
                    self.addCode(['jal','flushOrLoad_temp','',''])

                    #store value into register
                    self.addCode(['lw',new_reg,'0($s7)',''])



                symtab.addressDescriptor[temp]['register']= new_reg
                self.regInUse.append(new_reg)

                self.registerDescriptor[new_reg]= temp

            else:
                new_reg=self.regInUse.pop(0)

                #remove old temporary from this register
                oldTemp=self.registerDescriptor[new_reg]
                symtab.addressDescriptor[oldTemp]['register']=None

                #update register descriptor to new temp
                self.registerDescriptor[new_reg]=temp


                #Store oldTemp back in memory
                if symtab.addressDescriptor[oldTemp]['memory']!=None:
                    # load value of activation record where temporary must be stored
                    offset=symtab.addressDescriptor[oldTemp]['memory']

                    # Flushing
                    #find location of temporary in activation record
                    self.addCode(['li','$s6',offset,''])
                    self.addCode(['jal','flushOrLoad_temp','',''])
                    #store value into address
                    self.addCode(['sw',new_reg,'0($s7)',''])


                    #set the store flag
                    symtab.addressDescriptor[oldTemp]['store']=True

                #Load value of temp into new_reg from memory
                if symtab.addressDescriptor[temp]['memory']!=None and symtab.addressDescriptor[temp]["store"]:
                    # load value of activation record where temporary must be stored
                    offset=symtab.addressDescriptor[temp]['memory']

                    #find location of temporary in activation record
                    #Loading value of temporary into register

                    self.addCode(['li','$s6',offset,''])
                    self.addCode(['jal','flushOrLoad_temp','',''])

                    #store value into register
                    self.addCode(['lw',new_reg,'0($s7)',''])

                self.regInUse.append(new_reg)
                symtab.addressDescriptor[temp]['register']=new_reg
        return new_reg

    def flushRegister(self,temp):

        symtab = self.SymTabs[self.currentFunc]

        desc = symtab.addressDescriptor[temp]
        reg=desc['register']
        if desc['memory']!=None and desc['register']!=None:
            offset=desc['memory']
            #find location of temporary in activation record

            #flushing Register
            self.addCode(['li','$s6',offset,''])
            self.addCode(['jal','flushOrLoad_temp','',''])

            self.addCode(['sw',reg,'0($s7)',''])

            desc['store']=True
            desc['register']=None

            self.registerDescriptor[reg]=None
            self.regInUse.pop(self.regInUse.index(reg))
            self.freeReg.append(reg)

    def clearRegister(self,temp):

        symtab = self.SymTabs[self.currentFunc]

        desc = symtab.addressDescriptor[temp]
        reg=desc['register']
        if desc['memory']!=None and desc['register']!=None:
            desc['store']=True
            desc['register']=None

            self.registerDescriptor[reg]=None
            self.regInUse.pop(self.regInUse.index(reg))
            self.freeReg.append(reg)