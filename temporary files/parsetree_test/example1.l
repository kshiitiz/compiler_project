        // FLEX file example1 .l
%{
# include <stdlib.h>
# include <string>
using namespace std ;
# include "example1.tab.h" // output of bison on example1 .y
void yyerror (const char *);
int yyparse ( void );
%}

%%
[ \t\n]+ ; // do nothing on whitespace
"print" return PRINT ;
[a-zA-Z][a-zA-Z0-9]* {yylval.str_val = new string(yytext ); return VARIABLE ;}
[0-9][0-9]*(.[0-9]+)? { yylval.d = atof (yytext ); return NUMBER ;}
"=" return EQUALS ;
"+" return PLUS ;
"-" return MINUS ;
"*" return ASTERISK ;
"/" return FSLASH ;
"(" return LPAREN ;
")" return RPAREN ;
";" return SEMICOLON ;
%%

void yyerror ( const char * str ) { printf (" Parse Error : \n%s\n", str );}
int yywrap ( void ) { return 1; }

int main (int num_args , char ** args ) {
	if( num_args != 2) { printf (" usage : ./parser filename \n"); exit (0);}
	FILE * file = fopen ( args [1] ,"r");
	printf("graph{\n");
	if( file == NULL ) { printf (" couldn ’t open %s\n", args [1]); exit (0);}
	yyin = file ; // now flex reads from file
	yyparse ();
	printf("\n}\n");
	fclose(file);
}
