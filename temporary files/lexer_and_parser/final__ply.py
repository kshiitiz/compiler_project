# ----------------------------------------------------------------------
# final_ply.py
#
# A lexer and parser for C++.
# ----------------------------------------------------------------------

import ply.lex as lex
import ply.yacc as yacc

import sys
inFile = sys.argv[1]

id_count = 0
def getid():
    global id_count 
    id_count += 1
    return id_count


class Node:

    id_count = 0
    
    def __init__(self,name,children=None):
        self.name = name
        if children:
            self.children = children
        else:
            self.children = []
        Node.id_count += 1 
        self.id = Node.id_count
        print self.id," [label= \"",self.name,"\"]"
    
    def graph_dfs(self):
        if self.children:
            # print "Bacche: ",self.id
            # print self.children
            # print self.id,":",len(self.children)
            # for child in self.children:
            #     print child.id
            for child in self.children:            
                print self.id,"--",child.id
                child.graph_dfs()


# Reserved words
reserved = (
	# CPP Keywords
    # 'AND_EQ', 'ASM', 'BITAND', 'BITOR', 'BOOL', 'CATCH', 'CLASS', 'COMPL',
    # 'CONST_CAST', 'DELETE', 'DYNAMIC_CAST', 'EXPLICIT', 'EXPORT', 'FALSE', 'FRIEND',
    # 'INLINE', 'MUTABLE', 'NAMESPACE', 'NEW',  'NOT_EQ', 'OPERATOR', 
    # 'OR_EQ', 'PRIVATE', 'PROTECTED', 'PUBLIC', 'REINTERPRET_CAST', 'STATIC_CAST', 
    # 'TEMPLATE', 'THIS', 'THROW', 'TRUE', 'TRY', 'TYPENAME', 'USING', 
    # 'WCHAR_T', 'XOR_EQ',
    #Old C Keywords
    'AUTO', 'BREAK', 'CASE', 'CHAR', 'CONST', 'CONTINUE', 'DEFAULT', 'DO', 'DOUBLE',
    'ELSE',  'EXTERN', 'FLOAT', 'FOR', 'GOTO', 'IF', 'INT', 'LONG', 'REGISTER',
    'SHORT', 'SIGNED', 'SIZEOF', 'STATIC', 'STRUCT', 'SWITCH', 'TYPEDEF',
    'UNION', 'UNSIGNED', 'VOID', 'VOLATILE', 'WHILE',
    'RETURN', 'ENUM', '_BOOL', '_COMPLEX', '__INT128', 'INLINE', 'OFFSETOF',
    'RESTRICT'
    #'ENUM', 'RETURN', , 'EXTERN "C"', '#DEFINE', 'EXIT()', 'TYPEID',
)

tokens = reserved + (
    # Literals (identifier)
    'ID', 'TYPEID', 'INT_CONST_DEC', 'INT_CONST_OCT', 'INT_CONST_HEX', 
    'INT_CONST_BIN', 'FLOAT_CONST', 'HEX_FLOAT_CONST', 'CHAR_CONST',
    'WCHAR_CONST', 'STRING_LITERAL', 'WSTRING_LITERAL',

    # Operators (+,-,*,/,%,|,&,~,^,<<,>>, ||, &&, !, <, <=, >, >=, ==, !=)
    'PLUS', 'MINUS', 'TIMES', 'DIVIDE', 'MOD',
    'OR', 'AND', 'NOT', 'XOR', 'LSHIFT', 'RSHIFT',
    'LOR', 'LAND', 'LNOT',
    'LT', 'LE', 'GT', 'GE', 'EQ', 'NE',

    # Assignment (=, *=, /=, %=, +=, -=, <<=, >>=, &=, ^=, |=)
    'EQUALS', 'TIMESEQUAL', 'DIVEQUAL', 'MODEQUAL', 'PLUSEQUAL', 'MINUSEQUAL',
    'LSHIFTEQUAL', 'RSHIFTEQUAL', 'ANDEQUAL', 'XOREQUAL', 'OREQUAL',

    # Increment/decrement (++,--)
    'PLUSPLUS', 'MINUSMINUS',

    # Structure dereference (->)
    'ARROW',

    # Conditional operator (?)
    'CONDOP',

    # Delimeters ( ) [ ] { } , . ; :
    'LPAREN', 'RPAREN',
    'LBRACKET', 'RBRACKET',
    'LBRACE', 'RBRACE',
    'COMMA', 'PERIOD', 'SEMI', 'COLON',

    # Ellipsis (...)
    'ELLIPSIS',


    # # pre-processor
    # 'PPHASH',       # '#'
    # 'PPPRAGMA',     # 'pragma'
    # 'PPPRAGMASTR',
)



##
## Regexes for use in tokens
##
##

# valid C identifiers (K&R2: A.2.3), plus '$' (supported by some compilers)
identifier = r'[a-zA-Z_$][0-9a-zA-Z_$]*'

hex_prefix = '0[xX]'
hex_digits = '[0-9a-fA-F]+'
bin_prefix = '0[bB]'
bin_digits = '[01]+'

# integer constants (K&R2: A.2.5.1)
integer_suffix_opt = r'(([uU]ll)|([uU]LL)|(ll[uU]?)|(LL[uU]?)|([uU][lL])|([lL][uU]?)|[uU])?'
decimal_constant = '(0'+integer_suffix_opt+')|([1-9][0-9]*'+integer_suffix_opt+')'
octal_constant = '0[0-7]*'+integer_suffix_opt
hex_constant = hex_prefix+hex_digits+integer_suffix_opt
bin_constant = bin_prefix+bin_digits+integer_suffix_opt

#bad_octal_constant = '0[0-7]*[89]'

# character constants (K&R2: A.2.5.2)
# Note: a-zA-Z and '.-~^_!=&;,' are allowed as escape chars to support #line
# directives with Windows paths as filenames (..\..\dir\file)
# For the same reason, decimal_escape allows all digit sequences. We want to
# parse all correct code, even if it means to sometimes parse incorrect
# code.
#
simple_escape = r"""([a-zA-Z._~!=&\^\-\\?'"])"""
decimal_escape = r"""(\d+)"""
hex_escape = r"""(x[0-9a-fA-F]+)"""
bad_escape = r"""([\\][^a-zA-Z._~^!=&\^\-\\?'"x0-7])"""

escape_sequence = r"""(\\("""+simple_escape+'|'+decimal_escape+'|'+hex_escape+'))'
cconst_char = r"""([^'\\\n]|"""+escape_sequence+')'
char_const = "'"+cconst_char+"'"
wchar_const = 'L'+char_const
unmatched_quote = "('"+cconst_char+"*\\n)|('"+cconst_char+"*$)"
bad_char_const = r"""('"""+cconst_char+"""[^'\n]+')|('')|('"""+bad_escape+r"""[^'\n]*')"""

# string literals (K&R2: A.2.6)
string_char = r"""([^"\\\n]|"""+escape_sequence+')'
string_literal = '"'+string_char+'*"'
wstring_literal = 'L'+string_literal
bad_string_literal = '"'+string_char+'*?'+bad_escape+string_char+'*"'

# floating constants (K&R2: A.2.5.3)
exponent_part = r"""([eE][-+]?[0-9]+)"""
fractional_constant = r"""([0-9]*\.[0-9]+)|([0-9]+\.)"""
floating_constant = '(((('+fractional_constant+')'+exponent_part+'?)|([0-9]+'+exponent_part+'))[FfLl]?)'
binary_exponent_part = r'''([pP][+-]?[0-9]+)'''
hex_fractional_constant = '((('+hex_digits+r""")?\."""+hex_digits+')|('+hex_digits+r"""\.))"""
hex_floating_constant = '('+hex_prefix+'('+hex_digits+'|'+hex_fractional_constant+')'+binary_exponent_part+'[FfLl]?)'

# Completely ignored characters
t_ignore = ' \t\x0c'

# Newlines


def t_NEWLINE(t):
    r'\n+'
    t.lexer.lineno += t.value.count("\n")

# Operators
t_PLUS = r'\+'
t_MINUS = r'-'
t_TIMES = r'\*'
t_DIVIDE = r'/'
t_MOD = r'%'
t_OR = r'\|'
t_AND = r'&'
t_NOT = r'~'
t_XOR = r'\^'
t_LSHIFT = r'<<'
t_RSHIFT = r'>>'
t_LOR = r'\|\|'
t_LAND = r'&&'
t_LNOT = r'!'
t_LT = r'<'
t_GT = r'>'
t_LE = r'<='
t_GE = r'>='
t_EQ = r'=='
t_NE = r'!='

# Assignment operators

t_EQUALS = r'='
t_TIMESEQUAL = r'\*='
t_DIVEQUAL = r'/='
t_MODEQUAL = r'%='
t_PLUSEQUAL = r'\+='
t_MINUSEQUAL = r'-='
t_LSHIFTEQUAL = r'<<='
t_RSHIFTEQUAL = r'>>='
t_ANDEQUAL = r'&='
t_OREQUAL = r'\|='
t_XOREQUAL = r'\^='

# Increment/decrement
t_PLUSPLUS = r'\+\+'
t_MINUSMINUS = r'--'

# ->
t_ARROW = r'->'

# ?
t_CONDOP = r'\?'

# Delimeters
t_LPAREN = r'\('
t_RPAREN = r'\)'
t_LBRACKET = r'\['
t_RBRACKET = r'\]'
t_LBRACE = r'\{'
t_RBRACE = r'\}'
t_COMMA = r','
t_PERIOD = r'\.'
t_SEMI = r';'
t_COLON = r':'
t_ELLIPSIS = r'\.\.\.'

# Identifiers and reserved words

reserved_map = {}
for r in reserved:
    if r == '_BOOL':
        reserved_map['_Bool'] = r
    elif r == '_COMPLEX':
        reserved_map['_Complex'] = r
    else:    
        reserved_map[r.lower()] = r


t_STRING_LITERAL    = string_literal
t_FLOAT_CONST       = floating_constant
t_HEX_FLOAT_CONST   = hex_floating_constant
t_INT_CONST_HEX     = hex_constant
t_INT_CONST_BIN     = bin_constant

def t_BAD_CONST_OCT(t):
    r'0[0-7]*[89]'
    msg = "Invalid octal constant"
    print(msg)

t_INT_CONST_OCT     = octal_constant
t_INT_CONST_DEC     = decimal_constant
t_CHAR_CONST        = char_const
t_WCHAR_CONST       = wchar_const

# @TOKEN(unmatched_quote)
# def t_UNMATCHED_QUOTE(t):
#     msg = "Unmatched '"
#     self._error(msg, t)

# @TOKEN(bad_char_const)
# def t_BAD_CHAR_CONST(t):
#     msg = "Invalid char constant %s" % t.value
#     self._error(msg, t)

t_WSTRING_LITERAL   = wstring_literal

# unmatched string literals are caught by the preprocessor

# @TOKEN(bad_string_literal)
# def t_BAD_STRING_LITERAL(t):
#     msg = "String contains invalid escape code"
#     self._error(msg, t)

def t_ID(t):
    r'[A-Za-z_][\w_]*'
    t.type = reserved_map.get(t.value, "ID")
    return t

# Comments

def t_comment(t):
    r'(/\*(.|\n)*?\*/)|(//.*)'
    t.lexer.lineno += t.value.count('\n')

# Preprocessor directive (ignored)

def t_preprocessor(t):
    r'\#(.)*?\n'
    t.lexer.lineno += 1


def t_error(t):
    print("Illegal character %s" % repr(t.value[0]))
    t.lexer.skip(1)

lexer = lex.lex()





def lex_error_func(msg, line, column):
    print(msg,line,column)

##
## Precedence and associativity of operators
##
precedence = (
    ('left', 'LOR'),
    ('left', 'LAND'),
    ('left', 'OR'),
    ('left', 'XOR'),
    ('left', 'AND'),
    ('left', 'EQ', 'NE'),
    ('left', 'GT', 'GE', 'LT', 'LE'),
    ('left', 'RSHIFT', 'LSHIFT'),
    ('left', 'PLUS', 'MINUS'),
    ('left', 'TIMES', 'DIVIDE', 'MOD')
)

##
## Grammar productions
## Implementation of the BNF defined in K&R2 A.13
##

# Wrapper around a translation unit, to allow for empty input.
# Not strictly part of the C99 Grammar, but useful in practice.
#
def p_translation_unit_or_empty(p):
    """ translation_unit_or_empty   : translation_unit
                                    | empty
    """

    p[0] = Node("translation_unit_or_empty",[p[1]])
    p[0].graph_dfs()

def p_translation_unit_1(p):
    """ translation_unit    : external_declaration
    """
    # Note: external_declaration is already a list
    #
    p[0] = Node("translation_unit",[p[1]])

def p_translation_unit_2(p):
    """ translation_unit    : translation_unit external_declaration
    """

    p[0]=Node("translation_unit",[p[1],p[2]])

# Declarations always come as lists (because they can be
# several in one line), so we wrap the function definition
# into a list as well, to make the return value of
# external_declaration homogenous.
#
def p_external_declaration_1(p):
    """ external_declaration    : function_definition
    """
    p[0] = Node("external_declaration",[p[1]])

def p_external_declaration_2( p):
    """ external_declaration    : declaration
    """
    p[0] = Node("external_declaration",[p[1]])

# def p_external_declaration_3(p):
#     """ external_declaration    : pp_directive
#                                 | pppragma_directive
#     """
#     p[0] = Node("external_declaration",[p[1]])


def p_external_declaration_4(p):
    """ external_declaration    : SEMI
    """
    a=Node('SEMI')
    p[0] = Node("external_declaration",[a])


# def p_pp_directive(p):
#     """ pp_directive  : PPHASH
#     """
#     a=Node("PPHASH")
#     p[0]=Node("pp_directive",[a])

# def p_pppragma_directive(p):
#     """ pppragma_directive      : PPPRAGMA
#                                 | PPPRAGMA PPPRAGMASTR
#     """
#     if len(p) == 3:
#         a=Node("PPPRAGMA")
#         b=Node("PPPRAGMASTR")
#         p[0]=Node("pppragma_directive",[a,b])
#     else:
#         a=Node("PPPRAGMA")
#         p[0]=Node("pppragma_directive",[a])

# In function definitions, the declarator can be followed by
# a declaration list, for old "K&R style" function definitios.
#
def p_function_definition_1(p):
    """ function_definition : declarator declaration_list_opt compound_statement
    """
    p[0]=Node("function_definition",[p[1],p[2],p[3]])

def p_function_definition_2(p):
    """ function_definition : declaration_specifiers declarator declaration_list_opt compound_statement
    """

    p[0]=Node("function_definition",[p[1],p[2],p[3],p[4]])

def p_statement(p):
    """ statement   : labeled_statement
                    | expression_statement
                    | compound_statement
                    | selection_statement
                    | iteration_statement
                    | jump_statement
    """
    p[0]=Node("statement",[p[1]])

# In C, declarations can come several in a line:
#   int x, *px, romulo = 5;
#
# However, for the AST, we will split them to separate Decl
# nodes.
#
# This rule splits its declarations and always returns a list
# of Decl nodes, even if it's one element long.
#
def p_decl_body(p):
    """ decl_body : declaration_specifiers init_declarator_list_opt
    """
    p[0]=Node("decl_body",[p[1],p[2]])

# The declaration has been split to a decl_body sub-rule and
# SEMI, because having them in a single rule created a problem
# for defining typedefs.
#
# If a typedef line was directly followed by a line using the
# type defined with the typedef, the type would not be
# recognized. This is because to reduce the declaration rule,
# the parser's lookahead asked for the token after SEMI, which
# was the type from the next line, and the lexer had no chance
# to see the updated type symbol table.
#
# Splitting solves this problem, because after seeing SEMI,
# the parser reduces decl_body, which actually adds the new
# type into the table to be seen by the lexer before the next
# line is reached.
def p_declaration(p):
    """ declaration : decl_body SEMI
    """
    a=Node("SEMI")
    p[0]=Node("declaration",[p[1],a])

# Since each declaration is a list of declarations, this
# rule will combine all the declarations and return a single
# list
#
def p_abstract_declarator_opt(p):
    """ abstract_declarator_opt     :  empty 
                                    | abstract_declarator """
    p[0]=Node("abstract_declarator_opt",[p[1]])

def p_assignment_expression_opt(p):
    """ assignment_expression_opt   :  empty 
                                    | assignment_expression """
    p[0]=Node("assignment_expression_opt",[p[1]])

def p_declaration_list_opt(p):
    """ declaration_list_opt    :  empty 
                                | declaration_list """
    p[0]=Node("declaration_list_opt",[p[1]])

def p_declaration_specifiers_opt(p):
    """ declaration_specifiers_opt  :  empty 
                                    | declaration_specifiers """
    p[0]=Node("declaration_specifiers_opt",[p[1]])

def p_designation_opt(p):
    """ designation_opt     :  empty 
                            | designation """
    p[0]=Node("designation_opt",[p[1]])

def p_expression_opt(p):
    """ expression_opt  :  empty 
                        | expression """
    p[0]=Node("expression_opt",[p[1]])                               

def p_identifier_list_opt(p):
    ''' identifier_list_opt :  identifier_list 
                            | empty '''
    p[0]=Node("identifier_list_opt",[p[1]])                               

def p_init_declarator_list_opt(p):
    """ init_declarator_list_opt    :  empty 
                                    | init_declarator_list """
    p[0]=Node("init_declarator_list_opt",[p[1]])                               

def p_initializer_list_opt(p):
    """ initializer_list_opt    :  empty 
                                | initializer_list """
    p[0]=Node("initializer_list_opt",[p[1]]) 

def p_parameter_type_list_opt(p):
    """ parameter_type_list_opt     :  empty 
                                    | parameter_type_list """
    p[0]=Node("parameter_type_list_opt",[p[1]])                               

def p_specifier_qualifier_list_opt(p):
    """ specifier_qualifier_list_opt    :  empty 
                                        | specifier_qualifier_list """
    p[0]=Node("specifier_qualifier_list_opt",[p[1]])                               

def p_block_item_list_opt(p):
    """ block_item_list_opt  :  empty 
                             | block_item_list """
    p[0]=Node("block_item_list_opt",[p[1]])                               

def p_type_qualifier_list_opt(p):
    """ type_qualifier_list_opt     :  empty 
                                    | type_qualifier_list """
    p[0]=Node("type_qualifier_list_opt",[p[1]])                               

def p_struct_declarator_list_opt(p):
    """ struct_declarator_list_opt  :  empty 
                                    | struct_declarator_list """
    p[0]=Node("struct_declarator_list_opt",[p[1]])


def p_declaration_list(p):
    """ declaration_list    : declaration
                            | declaration_list declaration
    """
    if len(p) == 2:
        p[0] = Node("declaration_list",[p[1]]) 
    else:
        p[0] = Node("declaration_list",[p[1],p[2]])

def p_declaration_specifiers_1(p):
    """ declaration_specifiers  : type_qualifier declaration_specifiers_opt
    """
    p[0]=Node("declaration_specifiers",[p[1],p[2]])

def p_declaration_specifiers_2(p):
    """ declaration_specifiers  : type_specifier declaration_specifiers_opt
    """
    p[0]=Node("declaration_specifiers",[p[1],p[2]])

def p_declaration_specifiers_3(p):
    """ declaration_specifiers  : storage_class_specifier declaration_specifiers_opt
    """
    p[0]=Node("declaration_specifiers",[p[1],p[2]])

def p_declaration_specifiers_4(p):
    """ declaration_specifiers  : function_specifier declaration_specifiers_opt
    """
    p[0]=Node("declaration_specifiers",[p[1],p[2]])

def p_storage_class_specifier(p):
    """ storage_class_specifier : AUTO
                                | REGISTER
                                | STATIC
                                | EXTERN
                                | TYPEDEF
    """
    #To-Do
    a=Node(p[1])
    p[0]=Node("storage_class_specifier",[a])

def p_function_specifier(p):
    """ function_specifier  : INLINE
    """
    a=Node("INLINE")
    p[0] = Node("function_specifier",[a])


def p_type_specifier_1(p):
    """ type_specifier  : VOID
                        | _BOOL
                        | CHAR
                        | SHORT
                        | INT
                        | LONG
                        | FLOAT
                        | DOUBLE
                        | SIGNED
                        | UNSIGNED
                        | _COMPLEX
                        | __INT128
    """
    #To-Do
    a=Node(p[1])
    p[0]=Node("type_specifier",[a]);

def p_type_specifier_2(p):
    """ type_specifier  : typedef_name
                        | enum_specifier
                        | struct_or_union_specifier
    """
    p[0] = Node("type_specifier",[p[1]])

def p_type_qualifier(p):
    """ type_qualifier  : CONST
                        | RESTRICT
                        | VOLATILE
    """

    #To-Do
    a=Node(p[1])
    p[0]=Node("type_qualifier",[a])

def p_init_declarator_list_1(p):
    """ init_declarator_list    : init_declarator
                                | init_declarator_list COMMA init_declarator
    """
    if len(p)==4:
        a=Node("COMMA")
        p[0]=Node("init_declarator_list",[p[1],a,p[3]])
    else:
        p[0]=Node("init_declarator_list",[p[1]]) 

# If the code is declaring a variable that was declared a typedef in an
# outer scope, yacc will think the name is part of declaration_specifiers,
# not init_declarator, and will then get confused by EQUALS.  Pass None
# up in place of declarator, and handle this at a higher level.
#
def p_init_declarator_list_2(p):
    """ init_declarator_list    : EQUALS initializer
    """
    a=Node("EQUALS")
    p[0]=Node("init_declarator_list",[a,p[2]])

# Similarly, if the code contains duplicate typedefs of, for example,
# array types, the array portion will appear as an abstract declarator.
#
def p_init_declarator_list_3(p):
    """ init_declarator_list    : abstract_declarator
    """
    p[0]=Node("init_declarator_list",[p[1]])

# Returns a {decl=<declarator> : init=<initializer>} dictionary
# If there's no initializer, uses None
#
def p_init_declarator(p):
    """ init_declarator : declarator
                        | declarator EQUALS initializer
    """
    if len(p)==2:
        p[0]=Node("init_declarator",[p[1]])
    else:
        a=Node("EQUALS");
        p[0]=Node("init_declarator",[p[1],a,p[3]])

def p_specifier_qualifier_list_1(p):
    """ specifier_qualifier_list    : type_qualifier specifier_qualifier_list_opt
    """
    p[0]=Node("specifier_qualifier_list",[p[1],p[2]])

def p_specifier_qualifier_list_2(p):
    """ specifier_qualifier_list    : type_specifier specifier_qualifier_list_opt
    """
    p[0]=Node("specifier_qualifier_list",[p[1],p[2]])


# TYPEID is allowed here (and in other struct/enum related tag names), because
# struct/enum tags reside in their own namespace and can be named the same as types
#
def p_struct_or_union_specifier_1(p):
    """ struct_or_union_specifier   : struct_or_union ID
                                    | struct_or_union TYPEID
    """

    #To-Do
    a=Node(p[2])
    p[0]=Node("struct_or_union_specifier",[p[1],a])


def p_struct_or_union_specifier_2(p):
    """ struct_or_union_specifier : struct_or_union brace_open struct_declaration_list brace_close
    """
    # klass = self._select_struct_union_class(p[1])
    # p[0] = klass(
    #     name=None,
    #     decls=p[3],
    #     coord=self._coord(p.lineno(2)))
    p[0]=Node("struct_or_union_specifier",[p[1],p[2],p[3],p[4]])

def p_struct_or_union_specifier_3(p):
    """ struct_or_union_specifier   : struct_or_union ID brace_open struct_declaration_list brace_close
                                    | struct_or_union TYPEID brace_open struct_declaration_list brace_close
    """
    #To-Do
    a=Node(p[2])
    p[0]=Node("struct_or_union_specifier",[p[1],a,p[3],p[4],p[5]])

def p_struct_or_union(p):
    """ struct_or_union : STRUCT
                        | UNION
    """
    a=Node(p[1])
    p[0]=Node("struct_or_union",[a])

# Combine all declarations into a single list
#
def p_struct_declaration_list(p):
    """ struct_declaration_list     : struct_declaration
                                    | struct_declaration_list struct_declaration
    """
    if len(p) == 2:
        p[0] = Node("struct_declaration_list",[p[1]])
    else:
        p[0] = Node("struct_declaration_list",[p[1],p[2]])

def p_struct_declaration_1(p):

    """ struct_declaration : specifier_qualifier_list struct_declarator_list_opt SEMI
    """

    a=Node("SEMI")
    p[0]=Node("struct_declaration",[p[1],p[2],a])

def p_struct_declaration_2(p):
    """ struct_declaration : specifier_qualifier_list abstract_declarator SEMI
    """

    a=Node("SEMI")
    p[0]=Node("struct_declaration",[p[1],p[2],a])

def p_struct_declaration_3(p):
    """ struct_declaration : SEMI
    """
    a=Node("SEMI")
    p[0]=Node("struct_declaration",[a])

def p_struct_declarator_list(p):
    """ struct_declarator_list  : struct_declarator
                                | struct_declarator_list COMMA struct_declarator
    """
    if len(p)==4:
        a=Node("COMMA")
        p[0]=Node("struct_declarator_list",[p[1],a,p[3]])
    else:
        p[0]=Node("struct_declarator_list",[p[1]])

# struct_declarator passes up a dict with the keys: decl (for
# the underlying declarator) and bitsize (for the bitsize)
#
def p_struct_declarator_1(p):
    """ struct_declarator : declarator
    """
    p[0]=Node("struct_declarator",[p[1]])

def p_struct_declarator_2(p):
    """ struct_declarator   : declarator COLON constant_expression
                            | COLON constant_expression
    """
    if len(p) > 3:
        a=Node("COLON")
        p[0]=Node("struct_declarator",[p[1],a,p[3]])
    else:
        a=Node("COLON")
        p[0]=Node("struct_declarator",[a,p[2]])

def p_enum_specifier_1(p):
    """ enum_specifier  : ENUM ID
                        | ENUM TYPEID
    """
    a=Node("ENUM")
    b=Node(p[2])
    p[0]=Node("enum_specifier",[a,b])

def p_enum_specifier_2(p):
    """ enum_specifier  : ENUM brace_open enumerator_list brace_close
    """
    a=Node("ENUM")
    p[0]=Node("enum_specifier",[a,p[2],p[3],p[4]])

def p_enum_specifier_3(p):
    """ enum_specifier  : ENUM ID brace_open enumerator_list brace_close
                        | ENUM TYPEID brace_open enumerator_list brace_close
    """
    #To-do
    a=Node("ENUM")
    b=Node(p[2])
    p[0]=Node("enum_specifier",[a,b,p[3],p[4],p[4]])

def p_enumerator_list(p):
    """ enumerator_list : enumerator
                        | enumerator_list COMMA
                        | enumerator_list COMMA enumerator
    """
    if len(p) == 2:
        p[0] = Node("enumerator_list",[p[1]])
    elif len(p) == 3:
        a=Node("COMMA")
        p[0]=Node("enumerator_list",[p[1],a])
    else:
        a=Node("COMMA")
        p[0]=Node("enumerator_list",[p[1],a,p[3]])

def p_enumerator(p):
    """ enumerator  : ID
                    | ID EQUALS constant_expression
    """
    if len(p) == 2:
        a=Node(p[1])
        p[0]=Node("enumerator",[a])


    else:
        a=Node(p[1])
        b=Node("EQUALS")
        p[0]=Node("enumerator",[a,b,p[3]])

def p_declarator_1(p):
    """ declarator  : direct_declarator
    """
    p[0]=Node("declarator",[p[1]])

def p_declarator_2(p):
    """ declarator  : pointer direct_declarator
    """
    p[0] = Node("declarator",[p[1],p[2]])

# Since it's impossible for a type to be specified after a pointer, assume
# it's intended to be the name for this declaration.  _add_identifier will
# raise an error if this TYPEID can't be redeclared.
#
def p_declarator_3(p):
    """ declarator  : pointer TYPEID
    """
    a=Node("TYPEID")
    p[0]=Node("declarator",[p[1],a])

def p_direct_declarator_1(p):
    """ direct_declarator   : ID
    """
    a=Node(p[1])
    p[0] = Node("direct_declarator",[a])

def p_direct_declarator_2(p):
    """ direct_declarator   : LPAREN declarator RPAREN
    """
    a=Node("LPAREN")
    b=Node("RPAREN")
    p[0]=Node("direct_declarator",[a,p[2],b])

def p_direct_declarator_3(p):
    """ direct_declarator   : direct_declarator LBRACKET type_qualifier_list_opt assignment_expression_opt RBRACKET
    """
    a=Node("LBRACKET")
    b=Node("RBRACKET")
    p[0]=Node("direct_declarator",[p[1],a,p[3],p[4],b])


def p_direct_declarator_4(p):
    """ direct_declarator   : direct_declarator LBRACKET STATIC type_qualifier_list_opt assignment_expression RBRACKET
    """
    a=Node("LBRACKET")
    b=Node("STATIC")
    c=Node("RBRACKET")
    p[0]=Node("direct_declarator",[p[1],a,b,p[4],p[5],c])

def p_direct_declarator_5(p):
    """ direct_declarator   : direct_declarator LBRACKET type_qualifier_list STATIC assignment_expression RBRACKET
    """
    a=Node("LBRACKET")
    b=Node("STATIC")
    c=Node("RBRACKET")
    p[0]=Node("direct_declarator",[p[1],a,p[3],b,p[5],c])

# Special for VLAs
#
def p_direct_declarator_6(p):
    """ direct_declarator   : direct_declarator LBRACKET type_qualifier_list_opt TIMES RBRACKET
    """
    a=Node("LBRACKET")
    b=Node("TIMES")
    c=Node("RBRACKET")
    p[0]=Node("direct_declarator",[p[1],a,p[3],b,c])

def p_direct_declarator_7(p):
    """ direct_declarator   : direct_declarator LPAREN parameter_type_list RPAREN
                            | direct_declarator LPAREN identifier_list_opt RPAREN
    """
    a=Node("LPAREN")
    b=Node("RPAREN")
    p[0]=Node("direct_declarator",[p[1],a,p[3],b])



def p_pointer(p):
    """ pointer : TIMES type_qualifier_list_opt
                | TIMES type_qualifier_list_opt pointer
    """
    a=Node("TIMES")
    if len(p) == 3:
        p[0]=Node("pointer",[a,p[2]])
    else:
        p[0] = Node("pointer",[a,p[2],p[3]])

def p_type_qualifier_list(p):
    """ type_qualifier_list : type_qualifier
                            | type_qualifier_list type_qualifier
    """
    if len(p)==2:
        p[0]=Node("type_qualifier_list",[p[1]])
    else:
        p[0]=Node("type_qualifier_list",[p[1],p[2]])

def p_parameter_type_list(p):
    """ parameter_type_list : parameter_list
                            | parameter_list COMMA ELLIPSIS
    """
    if len(p) > 2:
        a=Node("COMMA")
        b=Node("ELLIPSIS")
        p[0]=Node("parameter_type_list",[p[1],a,b])
    else:
        p[0]=Node("parameter_type_list",[p[1]])

def p_parameter_list(p):
    """ parameter_list  : parameter_declaration
                        | parameter_list COMMA parameter_declaration
    """
    if len(p) == 2: # single parameter
        p[0] = Node("parameter_list",[p[1]])
    else:
        a=Node("COMMA")
        p[0]=Node("parameter_type_list",[p[1],a,p[3]])

def p_parameter_declaration_1(p):
    """ parameter_declaration   : declaration_specifiers declarator
    """
    p[0]=Node("parameter_declaration",[p[1],p[2]])

def p_parameter_declaration_2(p):
    """ parameter_declaration   : declaration_specifiers abstract_declarator_opt
    """
    p[0]=Node("parameter_declaration",[p[1],p[2]])

def p_identifier_list(p):
    """ identifier_list : identifier
                        | identifier_list COMMA identifier
    """
    if len(p) == 2: # single parameter
        p[0] = Node("identifier_list",[p[1]])
    else:
        a=Node("COMMA")
        p[0]= Node("identifier_list",[p[1],a,p[3]])

def p_initializer_1(p):
    """ initializer : assignment_expression
    """
    p[0] = Node("initializer",[p[1]])

def p_initializer_2(p):
    """ initializer : brace_open initializer_list_opt brace_close
                    | brace_open initializer_list COMMA brace_close
    """
    if len(p)==4:
        p[0] = Node("initializer",[p[1],p[2],p[3]])
    else:
        a=Node("COMMA")
        p[0] = Node("initializer",[p[1],p[2],a,p[4]])

def p_initializer_list(p):
    """ initializer_list    : designation_opt initializer
                            | initializer_list COMMA designation_opt initializer
    """
    if len(p) == 3: # single initializer
        p[0]=Node("initializer_list",[p[1],p[2]])
    else:
        a=Node("COMMA")
        p[0]=Node("initializer_list",[p[1],a,p[3],p[4]])

def p_designation(p):
    """ designation : designator_list EQUALS
    """
    a=Node("EQUALS")
    p=Node("designation",[p[1],a])



# Designators are represented as a list of nodes, in the order in which
# they're written in the code.
#
def p_designator_list(p):
    """ designator_list : designator
                        | designator_list designator
    """
    if len(p)==2:
        p=Node("designator_list",[p[1]])
    else:
        p=Node("designator_list",[p[1],p[2]])

def p_designator(p):
    """ designator  : LBRACKET constant_expression RBRACKET
                    | PERIOD identifier
    """
    if len(p)==4:
        a=Node("LBRACKET")
        b=Node("RBRACKET")
        p[0]=Node("designator",[a,p[2],b])
    else:
        a=Node("PERIOD")
        p[0]=Node("designator",[a,p[2]])

def p_type_name(p):
    """ type_name   : specifier_qualifier_list abstract_declarator_opt
    """
    p[0]=Node("type_name",[p[1],p[2]])

def p_abstract_declarator_1(p):
    """ abstract_declarator     : pointer
    """
    p[0]=Node("abstract_declarator",[p[1]])

def p_abstract_declarator_2(p):
    """ abstract_declarator     : pointer direct_abstract_declarator
    """
    p[0]=Node("abstract_declarator",[p[1],p[2]])

def p_abstract_declarator_3(p):
    """ abstract_declarator     : direct_abstract_declarator
    """
    p[0]=Node("abstract_declarator",[p[1]])

# Creating and using direct_abstract_declarator_opt here
# instead of listing both direct_abstract_declarator and the
# lack of it in the beginning of _1 and _2 caused two
# shift/reduce errors.
#
def p_direct_abstract_declarator_1(p):
    """ direct_abstract_declarator  : LPAREN abstract_declarator RPAREN """
    a=Node("LPAREN")
    b=Node("RPAREN")
    p[0]=Node("direct_abstract_declarator",[a,p[2],b])

def p_direct_abstract_declarator_2(p):
    """ direct_abstract_declarator  : direct_abstract_declarator LBRACKET assignment_expression_opt RBRACKET
    """
    a=Node("LBRACKET")
    b=Node("RBRACKET")
    p[0]=Node("direct_abstract_declarator",[p[1],a,p[3],b])

def p_direct_abstract_declarator_3(p):
    """ direct_abstract_declarator  : LBRACKET assignment_expression_opt RBRACKET
    """
    a=Node("LBRACKET")
    b=Node("RBRACKET")
    p[0]=Node("direct_abstract_declarator",[a,p[2],b])

def p_direct_abstract_declarator_4(p):
    """ direct_abstract_declarator  : direct_abstract_declarator LBRACKET TIMES RBRACKET
    """
    a=Node("LBRACKET")
    b=Node("RBRACKET")
    c=Node("TIMES")
    p[0]=Node("direct_abstract_declarator",[p[1],a,c,b])

def p_direct_abstract_declarator_5(p):
    """ direct_abstract_declarator  : LBRACKET TIMES RBRACKET
    """
    a=Node("LBRACKET")
    b=Node("RBRACKET")
    c=Node("TIMES")
    p[0]=Node("direct_abstract_declarator",[a,c,b])

def p_direct_abstract_declarator_6(p):
    """ direct_abstract_declarator  : direct_abstract_declarator LPAREN parameter_type_list_opt RPAREN
    """
    a=Node("LPAREN")
    b=Node("RPAREN")
    p[0]=Node("direct_abstract_declarator",[p[1],a,p[3],b])

def p_direct_abstract_declarator_7(p):
    """ direct_abstract_declarator  : LPAREN parameter_type_list_opt RPAREN
    """
    a=Node("LPAREN")
    b=Node("RPAREN")
    p[0]=Node("direct_abstract_declarator",[a,p[2],b])

# declaration is a list, statement isn't. To make it consistent, block_item
# will always be a list
#
def p_block_item(p):
    """ block_item  : declaration
                    | statement
    """
    p[0]=Node("block_item",[p[1]])
# Since we made block_item a list, this just combines lists
#
def p_block_item_list(p):
    """ block_item_list : block_item
                        | block_item_list block_item
    """
    # Empty block items (plain ';') produce [None], so ignore them
    if len(p) == 2:
        p[0]=Node("block_item_list",[p[1]])
    else:
        p[0]=Node("block_item_list",[p[1],p[2]])

def p_compound_statement_1(p):
    """ compound_statement : brace_open block_item_list_opt brace_close """
    p[0] = Node("compound_statement",[p[1],p[2],p[3]])

def p_labeled_statement_1(p):
    """ labeled_statement : ID COLON statement """
    a=Node(p[1])
    b=Node("COLON")
    p[0]=Node("labeled_statement",[a,b,p[3]])

def p_labeled_statement_2(p):
    """ labeled_statement : CASE constant_expression COLON statement """
    a=Node("CASE")
    b=Node("COLON")
    p[0]=Node("labeled_statement",[a,p[2],b,p[4]])

def p_labeled_statement_3(p):
    """ labeled_statement : DEFAULT COLON statement """
    a=Node("DEFAULT")
    b=Node("COLON")
    p[0]=Node("labeled_statement",[a,b,p[3]])

def p_selection_statement_1(p):
    """ selection_statement : IF LPAREN expression RPAREN statement """
    a=Node("IF")
    b=Node("LPAREN")
    c=Node("RPAREN")
    p[0]=Node("selection_statement",[a,b,p[3],c,p[5]])
def p_selection_statement_2(p):
    """ selection_statement : IF LPAREN expression RPAREN statement ELSE statement """
    a=Node("IF")
    b=Node("LPAREN")
    c=Node("RPAREN")
    d=Node("ELSE")
    p[0]=Node("selection_statement",[a,b,p[3],c,p[5],d,p[7]])

def p_selection_statement_3(p):
    """ selection_statement : SWITCH LPAREN expression RPAREN statement """
    a=Node("SWITCH")
    b=Node("LPAREN")
    c=Node("RPAREN")
    p[0]=Node("selection_statement",[a,b,p[3],c,p[5]])

def p_iteration_statement_1(p):
    """ iteration_statement : WHILE LPAREN expression RPAREN statement """
    a=Node("WHILE")
    b=Node("LPAREN")
    c=Node("RPAREN")
    p[0]=Node("iteration_statement",[a,b,p[3],c,p[5]])

def p_iteration_statement_2(p):
    """ iteration_statement : DO statement WHILE LPAREN expression RPAREN SEMI """
    a=Node("DO")
    b=Node("WHILE")
    c=Node("LPAREN")
    d=Node("RPAREN")
    e=Node("SEMI")
    p[0]=Node("iteration_statement",[a,p[2],b,c,p[5],d,e])

def p_iteration_statement_3(p):
    """ iteration_statement : FOR LPAREN expression_opt SEMI expression_opt SEMI expression_opt RPAREN statement """
    a=Node("FOR")
    b=Node("LPAREN")
    c=Node("SEMI")
    d=Node("SEMI")
    e=Node("RPAREN")
    p[0]=Node("iteration_statement",[a,b,p[3],c,p[5],d,p[7],e,p[9]])


def p_iteration_statement_4(p):
    """ iteration_statement : FOR LPAREN declaration expression_opt SEMI expression_opt RPAREN statement """
    a=Node("FOR")
    b=Node("LPAREN")
    c=Node("SEMI")
    d=Node("RPAREN")
    p[0]=Node("iteration_statement",[a,b,p[3],p[4],c,p[6],d,p[8]])

def p_jump_statement_1(p):
    """ jump_statement  : GOTO ID SEMI """
    a=Node("GOTO")
    b=Node(p[2])
    c=Node("SEMI")
    p[0]=Node("jump_statement",[a,b,c])

def p_jump_statement_2(p):
    """ jump_statement  : BREAK SEMI """
    a=Node("BREAK")
    b=Node("SEMI")
    p[0]=Node("jump_statement",[a,b])
    

def p_jump_statement_3(p):
    """ jump_statement  : CONTINUE SEMI """
    a=Node("CONTINUE")
    b=Node("SEMI")
    p[0]=Node("jump_statement",[a,b])

def p_jump_statement_4(p):
    """ jump_statement  : RETURN expression SEMI
                        | RETURN SEMI
    """
    a=Node("RETURN")
    b=Node("SEMI")
    if len(p)==4:
        p[0]=Node("jump_statement",[a,p[2],b])
    else:
        p[0]=Node("jump_statement",[a,b])

def p_expression_statement(p):
    """ expression_statement : expression_opt SEMI """
    a=Node("SEMI")
    p[0]=Node("expression_statement",[p[1],a])

def p_expression(p):
    """ expression  : assignment_expression
                    | expression COMMA assignment_expression
    """
    if len(p) == 2:
        p[0] = Node("expression",[p[1]])
    else:
        a=Node("COMMA")
        p[0]=Node("expression",[p[1],a,p[3]])


def p_typedef_name(p):
    """ typedef_name : TYPEID """
    a=Node("TYPEID")
    p[0]=Node("typedef_name",[a])

def p_assignment_expression(p):
    """ assignment_expression   : conditional_expression
                                | unary_expression assignment_operator assignment_expression
    """
    if len(p) == 2:
        p[0] = Node("assignment_expression",[p[1]])
    else:
        p[0] = Node("assignment_expression",[p[1],p[2],p[3]])

# K&R2 defines these as many separate rules, to encode
# precedence and associativity. Why work hard ? I'll just use
# the built in precedence/associativity specification feature
# of PLY. (see precedence declaration above)
#
def p_assignment_operator(p):
    """ assignment_operator : EQUALS
                            | XOREQUAL
                            | TIMESEQUAL
                            | DIVEQUAL
                            | MODEQUAL
                            | PLUSEQUAL
                            | MINUSEQUAL
                            | LSHIFTEQUAL
                            | RSHIFTEQUAL
                            | ANDEQUAL
                            | OREQUAL
    """
    ##TODO
    a=Node(p[1])
    p[0] = Node("assignment_operator",[a])

def p_constant_expression(p):
    """ constant_expression : conditional_expression """
    p[0] = Node("constant_expression",[p[1]])

def p_conditional_expression(p):
    """ conditional_expression  : binary_expression
                                | binary_expression CONDOP expression COLON conditional_expression
    """
    if len(p) == 2:
        p[0] = Node("conditional_expression",[p[1]])
    else:
        a=Node("CONDOP")
        b= Node("COLON")

        p[0] = Node("conditional_expression",[p[1],a,p[3],b,p[5]])

def p_binary_expression(p):
    """ binary_expression   : cast_expression
                            | binary_expression TIMES binary_expression
                            | binary_expression DIVIDE binary_expression
                            | binary_expression MOD binary_expression
                            | binary_expression PLUS binary_expression
                            | binary_expression MINUS binary_expression
                            | binary_expression RSHIFT binary_expression
                            | binary_expression LSHIFT binary_expression
                            | binary_expression LT binary_expression
                            | binary_expression LE binary_expression
                            | binary_expression GE binary_expression
                            | binary_expression GT binary_expression
                            | binary_expression EQ binary_expression
                            | binary_expression NE binary_expression
                            | binary_expression AND binary_expression
                            | binary_expression OR binary_expression
                            | binary_expression XOR binary_expression
                            | binary_expression LAND binary_expression
                            | binary_expression LOR binary_expression
    """
    if len(p) == 2:
        p[0] = Node("binary_expression",[p[1]])
    else:
        ##TODO
        a=Node(p[2])
        p[0] = Node("binary_expression",[p[1],a,p[3]])

def p_cast_expression_1(p):
    """ cast_expression : unary_expression """
    p[0] = Node("cast_expression",[p[1]])

def p_cast_expression_2(p):
    """ cast_expression : LPAREN type_name RPAREN cast_expression """
    a=Node("LPAREN")
    b=Node("RPAREN")
    p[0]=Node("cast_expression",[a,p[2],b,p[4]])
def p_unary_expression_1(p):
    """ unary_expression    : postfix_expression """
    p[0] = Node("unary_expression",[p[1]])

def p_unary_expression_2(p):
    """ unary_expression    : PLUSPLUS unary_expression
    """
    a=Node("PLUSPLUS")
    p[0]=Node("unary_expression",[a,p[2]])


def p_unary_expression_3(p):
    """ unary_expression   : MINUSMINUS unary_expression
    """        
    a=Node("MINUSMINUS")
    p[0]=Node("unary_expression",[a,p[2]])

def p_unary_expression_4(p):
    """ unary_expression    : unary_operator cast_expression
    """
    p[0]=Node("unary_expression",[p[1],p[2]])

def p_unary_expression_5(p):
    """ unary_expression    : SIZEOF unary_expression
                            | SIZEOF LPAREN type_name RPAREN
    """
    if len(p)==3:
        a=Node("SIZEOF")
        p[0]=Node("unary_expression",[a,p[2]])
    else:
        a=Node("SIZEOF")
        b=Node("LPAREN")
        c=Node("RPAREN")
        p[0]=Node("unary_expression",[a,b,p[3],c])

def p_unary_operator(p):
    """ unary_operator  : AND
                        | TIMES
                        | PLUS
                        | MINUS
                        | NOT
                        | LNOT
    """
    a=Node(p[1])
    p[0] = Node("unary_operator",[a])

def p_postfix_expression_1(p):
    """ postfix_expression  : primary_expression """
    p[0] = Node("postfix_expression",[p[1]])

def p_postfix_expression_2(p):
    """ postfix_expression  : postfix_expression LBRACKET expression RBRACKET """
    a=Node("LBRACKET")
    b=Node("RBRACKET")
    p[0]=Node("postfix_expression",[p[1],a,p[3],b])

def p_postfix_expression_3(p):
    """ postfix_expression  : postfix_expression LPAREN argument_expression_list RPAREN
                            | postfix_expression LPAREN RPAREN
    """
    a=Node("LPAREN")
    b=Node("RPAREN")

    if len(p)==5:
        p[0]=Node("postfix_expression",[p[1],a,p[3],b])
    else:
        p[0]=Node("postfix_expression",[p[1],a,b])

def p_postfix_expression_4(p):
    """ postfix_expression  : postfix_expression PERIOD ID
                            | postfix_expression PERIOD TYPEID
                            | postfix_expression ARROW ID
                            | postfix_expression ARROW TYPEID
    """
    a=Node(p[2])
    b=Node(p[3])
    p[0]=Node("postfix_expression",[p[1],a,b])

def p_postfix_expression_5(p):
    """ postfix_expression  : postfix_expression PLUSPLUS
                            | postfix_expression MINUSMINUS
    """
    a=Node(p[2])
    p[0] = Node("postfix_expression",[p[1],a])

def p_postfix_expression_6(p):
    """ postfix_expression  : LPAREN type_name RPAREN brace_open initializer_list brace_close
                            | LPAREN type_name RPAREN brace_open initializer_list COMMA brace_close
    """
    if len(p)==7:
        a=Node("LPAREN")
        b=Node("RPAREN")
        p[0]=Node("postfix_expression",[a,p[1],b,p[4],p[5],p[6]])
    else:
        a=Node("LPAREN")
        b=Node("RPAREN")
        c=Node("COMMA")
        p[0]=Node("postfix_expression",[a,p[1],b,p[4],p[5],c,p[7]])

def p_primary_expression_1(p):
    """ primary_expression  : identifier """
    p[0] = Node("primary_expression",[p[1]])

def p_primary_expression_2(p):
    """ primary_expression  : constant """
    p[0] = Node("primary_expression",[p[1]])

def p_primary_expression_3(p):
    """ primary_expression  : unified_string_literal
                            | unified_wstring_literal
    """
    p[0] = Node("primary_expression",[p[1]])

def p_primary_expression_4(p):
    """ primary_expression  : LPAREN expression RPAREN """
    a=Node("LPAREN")
    b=Node("RPAREN")
    p[0] = Node("primary_expression",[a,p[2],b])

def p_primary_expression_5(p):
    """ primary_expression  : OFFSETOF LPAREN type_name COMMA offsetof_member_designator RPAREN
    """
    a=Node("OFFSETOF")
    b=Node("LPAREN")
    c=Node("COMMA")
    d=Node("RPAREN")
    p[0]=Node("primary_expression",[a,b,p[3],c,p[5],d])

def p_offsetof_member_designator(p):
    """ offsetof_member_designator : identifier
                                     | offsetof_member_designator PERIOD identifier
                                     | offsetof_member_designator LBRACKET expression RBRACKET
    """
    if len(p) == 2:
        p[0] = Node("offsetof_member_designator",[p[1]])
    elif len(p) == 4:
        a=Node("PERIOD")
        p[0]=Node("offsetof_member_designator",[p[1],a,p[3]])
    elif len(p) == 5:
        a=Node("RBRACKET")
        b=Node("LBRACKET")
        p[0]=Node("offsetof_member_designator",[p[1],b,p[3],a])
    else:
        raise NotImplementedError("Unexpected parsing state. len(p): %u" % len(p))

def p_argument_expression_list(p):
    """ argument_expression_list    : assignment_expression
                                    | argument_expression_list COMMA assignment_expression
    """
    if len(p) == 2: # single expr
        p[0] = Node("argument_expression_list",[p[1]])
    else:
        a=Node("COMMA")
        p[0]=Node("argument_expression_list",[p[1],a,p[3]])

def p_identifier(p):
    """ identifier  : ID """
    a=Node(p[1])
    p[0]=Node("identifier",[a])

def p_constant_1(p):
    """ constant    : INT_CONST_DEC
                    | INT_CONST_OCT
                    | INT_CONST_HEX
                    | INT_CONST_BIN
    """
    a=Node(p[1])
    p[0]=Node("constant",[a])

def p_constant_2(p):
    """ constant    : FLOAT_CONST
                    | HEX_FLOAT_CONST
    """
    a=Node(p[1])
    p[0]=Node("constant",[a])     

def p_constant_3(p):
    """ constant    : CHAR_CONST
                    | WCHAR_CONST
    """
    ##TODO
    a=Node(p[1])
    p[0]=Node("constant",[a])

# The "unified" string and wstring literal rules are for supporting
# concatenation of adjacent string literals.
# I.e. "hello " "world" is seen by the C compiler as a single string literal
# with the value "hello world"
#
def p_unified_string_literal(p):
    """ unified_string_literal  : STRING_LITERAL
                                | unified_string_literal STRING_LITERAL
    """
    a=Node("STRING_LITERAL")
    if len(p) == 2: # single literal
        p[0] = Node("unified_string_literal",[a])
    else:
        p[0] = Node("unified_string_literal",[p[1],a])

def p_unified_wstring_literal(p):
    """ unified_wstring_literal : WSTRING_LITERAL
                                | unified_wstring_literal WSTRING_LITERAL
    """
    a=Node("WSTRING_LITERAL")
    if len(p) == 2: # single literal
        p[0] = Node("unified_wstring_literal",[a])
    else:
        p[0] = Node("unified_wstring_literal",[p[1],a])

def p_brace_open(p):
    """ brace_open  :   LBRACE
    """
    a=Node("LBRACE")
    p[0]=Node("brace_open",[a])

def p_brace_close(p):
    """ brace_close :   RBRACE
    """
    a=Node("RBRACE")
    p[0]=Node("brace_close",[a])

def p_empty(p):
    'empty : '
    p[0] = Node("empty")

def p_error(p):
    # If error recovery is added here in the future, make sure
    # _get_yacc_lookahead_token still works!
    #
    print "Error in Parsing"
    # if p:
    #     self._parse_error(
    #         'before: %s' % p.value,
    #         self._coord(lineno=p.lineno,
    #                     column=self.clex.find_tok_column(p)))
    # else:
    #     self._parse_error('At end of input', self.clex.filename)


if __name__ == "__main__":
    parser=yacc.yacc()
    print "graph{"
    print "ordering=out"
    with open(inFile,'r') as i:
        text = i.read()
        parser.parse(text)
    print"}"









