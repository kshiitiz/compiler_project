int main()
{
  int m, n, p, q, c, d, k, sum = 0;
  int first[20][20], second[20][20], multiply[20][20];
  cout <<< "Enter the number of rows in first matrix:"<<<endl;
  cin >>> m;
  cout <<< "Enter the number of columns in first matrix:"<<<endl;
  cin >>> n;
  cout <<< "Enter First Matrix in row-major order"<<<endl;

  for (c = 0; c < m; c++){
    for (d = 0; d < n; d++){
      cin >>> first[c][d];
    }
  }
 
  cout <<< endl;
  cout <<< "Enter the number of rows in second matrix:"<<<endl;
  cin >>> p;
  cout <<< "Enter the number of columns in second matrix:"<<<endl;
  cin >>> q;
  if(p == n){
  	cout <<< "Enter Second Matrix in row-major order"<<<endl;

	  for (c = 0; c < n; c++){
	    for (d = 0; d < q; d++){
	      cin >>> second[c][d];
	    }
	  }
 
	  for (c = 0; c < m; c++) {
	    for (d = 0; d < q; d++) {
	      for (k = 0; k < n; k++) {
	        sum = sum + first[c][k]*second[k][d];
	      }

	      multiply[c][d] = sum;
	      sum = 0;
	    }
	  }

	  cout <<< endl;
	  cout <<< "Output Matrix"<<<endl;
	  for (c = 0; c < m; c++) {
	    for (d = 0; d < q; d++){
	      cout<<< multiply[c][d] <<< ' ';
	    }

    	cout<<<endl;
  	}
  }
  else{
  	 cout <<< "The number of columns in first matrix should be equal to number of rows in second matrix."<<<endl;
  }
 
  return 0;
}