/*
 * Declarations for a calculator fb3-1
 */
# include <string>
using namespace std;
/* interface to the lexer */
extern int yylineno; /* from lexer */
/* nodes in the abstract syntax tree */
struct node {
 string nodetype;
struct node *first;
struct node *second;
struct node *third;
 int nodeid;
};

/* build an AST */
struct node * newnode(string , struct node *,struct node *,struct node *);

int getid();
/* evaluate an AST */
void eval(struct node *);
