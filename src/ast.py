import sys

class Node(object):

    id_count = 0
    
    def __init__(self,name,children=None):
        self.name = name
        if children:
            self.children = children
        else:
            self.children = []
        Node.id_count += 1 
        self.id = Node.id_count
        # print "Here"
        # print self.id," [label= \"",self.name,"\"]"

    def assign_id(self):
        Node.id_count +=1
        self.id = Node.id_count

    def show(self, buf=sys.stdout, offset=0, attrnames=True, nodenames=False, showcoord=False, _my_node_name=None, ast_dot=None,ast_txt=None):
        """ Pretty print the Node and all its attributes and
            children (recursively) to a buffer.

            buf:
                Open IO buffer into which the Node is printed.

            offset:
                Initial offset (amount of leading spaces)

            attrnames:
                True if you want to see the attribute names in
                name=value pairs. False to only see the values.

            nodenames:
                True if you want to see the actual node names
                within their parents.

            showcoord:
                Do you want the coordinates of each Node to be
                displayed.
        """
        lead = ' ' * offset
        if nodenames and _my_node_name is not None:
            print (lead + self.__class__.__name__+ ' <' + _my_node_name + '>: '),
            ast_txt.write(lead + self.__class__.__name__+ ' <' + _my_node_name + '>:')
        else:
            print (lead + self.__class__.__name__+ ': '),
            ast_txt.write(lead + self.__class__.__name__+ ': ')

        nvlist=[]
        if self.attr_names:
            if attrnames:
                nvlist = [(n, getattr(self,n)) for n in self.attr_names]
                attrstr = ', '.join('%s=%s' % nv for nv in nvlist)
            else:
                nvlist = [getattr(self, n) for n in self.attr_names]
                attrstr = ', '.join('%s' % v for v in nvlist)
            print('('+attrstr+')')
            ast_txt.write('('+attrstr+')\n')
        else :
            print
            ast_txt.write("\n")

        if ast_dot:
            line="{0} [label=\"{1}\"]\n".format(self.id,self.__class__.__name__)
            ast_dot.write(str(line))
            if attrnames:
                for nv in nvlist:
                    if nv[1]:
                        Node.id_count += 1
                        child_id=Node.id_count

                        line="{0} [label=\"{1}\"]\n".format(child_id,nv[1])
                        ast_dot.write(str(line))
                        line="{0} -- {1}\n".format(self.id,child_id)
                        ast_dot.write(str(line))




        # for creating children list via function call:
        try:
            self.make_children()
        except AttributeError:
            pass

        for (child_name,child) in self.children:
            if ast_dot:
                line="{0} -- {1}\n".format(self.id,child.id)
                ast_dot.write(str(line))


            child.show(
                buf,
                offset=offset + 2,
                attrnames=attrnames,
                nodenames=nodenames,
                showcoord=showcoord,
                _my_node_name=child_name,
                ast_dot=ast_dot,
                ast_txt=ast_txt)

class Statements(Node):
    def __init__(self):
        self.assign_id()
        self.children=[]
        self.nextlist=[]
        self.falselist=[]
        self.truelist=[]
        self.breaklist=[]
        self.contlist=[]

class ArrayDecl(Node):
    def __init__(self, type , dim, coord=None):
        self.type = type
        self.dim = dim
        self.assign_id()
        self.coord = coord
        self.children = []

    def make_children(self):
        if self.type is not None: self.children.append(("type", self.type))
        if self.dim is not None: self.children.append(("dim", self.dim))
    attr_names = ( )

class ArrayRef(Statements):
    def __init__(self, prev ,index, name, exptype=None,dims=None,depth=None,loffset=None,coord=None):
        self.prev=prev
        self.name = name
        self.index = index
        self.exptype = exptype
        self.coord = coord
        self.place = None
        self.depth = depth
        self.dims = dims
        self.loffset = loffset
        super(ArrayRef,self).__init__()

    def make_children(self):
        if self.prev is not None: self.children.append(("prev", self.prev))
        if self.index is not None: self.children.append(("index", self.index))
    
    attr_names = ( )

class AssExpr(Statements):
    def __init__(self,op,lexpr,rexpr,exptype=None,coord=None):
        self.op=op
        self.lexpr=lexpr
        self.rexpr=rexpr
        self.place = None
        self.exptype=exptype
        super(AssExpr,self).__init__()

        if type(exptype) is str:
            self.optype=op+'('+exptype+')'
        else:
            self.optype=op+'('+exptype.__class__.__name__+')'

    def make_children(self):
        if self.lexpr is not None: self.children.append(("lexpr", self.lexpr))
        if self.rexpr is not None: self.children.append(("rexpr", self.rexpr))
    
    attr_names = ('optype',)

class BlockItemList(object):
    def __init__(self,child1,coord=None):
        self.truelist=[]
        self.falselist=[]
        self.nextlist=[]
        self.blockitems = child1
        self.breaklist= []
        self.contlist=[]

class BinaryOp(Statements):
    def __init__(self,op,lexpr,rexpr,exptype=None,coord=None):
        self.op=op
        self.lexpr=lexpr
        self.rexpr=rexpr
        self.exptype = exptype
        self.place = None
        super(BinaryOp,self).__init__()

        if type(exptype) is str:
            self.optype=op+'('+exptype+')'
        else:
            self.optype=op+'('+exptype.__class__.__name__+')'
    
    def make_children(self):
        if self.lexpr is not None: self.children.append(("lexpr", self.lexpr))
        if self.rexpr is not None: self.children.append(("rexpr", self.rexpr))
    
    attr_names = ('optype',)

class Break(Statements):
    def __init__(self,  coord=None):
        self.coord=coord
        super(Break,self).__init__()
    
    def make_children(self):
        pass
    
    attr_names = ()

class Case(Node):
    def __init__(self, const_expr ,statement, coord=None):
        self.statement=statement
        self.const_expr=const_expr
        self.assign_id()
        self.coord = coord
        self.children=[]
    
    def make_children(self):
        if self.const_expr is not None: self.children.append(("const_expr", self.const_expr))
        if self.statement is not None: self.children.append(("statement", self.statement))

    attr_names = ( )

class Cast(Statements):
    def __init__(self,to_type,expr,exptype=None,coord=None):
        self.to_type=to_type
        self.expr=expr
        self.exptype=exptype
        self.coord=coord
        super(Cast,self).__init__()

    def make_children(self):
        if self.to_type is not None: self.children.append(("to_type", self.to_type))
        if self.expr is not None: self.children.append(("expr", self.expr))
    
    attr_names = ()


class CompStmt(Statements):
    def __init__(self,block_items_list,coord=None):
        self.block_items_list = block_items_list
        self.coord = coord
        super(CompStmt,self).__init__()
    
    def make_children(self):
        if self.block_items_list:
            for i,block_item in enumerate(self.block_items_list):
                self.children.append(("block_item[%d]"%i,block_item))

    attr_names = ()


class Constant(Statements):
    def __init__(self,type,value,exptype=None,coord=None):
        self.type=type
        self.value=value
        self.exptype = exptype
        self.place = value
        self.coord=coord
        super(Constant,self).__init__()
    
    def make_children(self):
        pass
    
    attr_names = ('type', 'value', )


class Continue(Statements):
    def __init__(self,  coord=None):
        self.coord=coord
        super(Continue,self).__init__()

    def make_children(self):
        pass

    attr_names = ()


class Decl(Statements):
    def __init__(self,name=None,quals=None,storage=None,funcspec=None,type=None,init=None,size=0,offset=0,coord=None):
        self.name=name
        self.quals=quals
        self.storage=storage
        self.funcspec=funcspec
        self.type=type
        self.init=init
        self.size=size
        self.offset=offset
        self.coord=coord
        super(Decl,self).__init__()

    def make_children(self):
        if self.type is not None: self.children.append(("type", self.type))
        if self.init is not None: self.children.append(("init", self.init))

    # attr_names = ('name', 'quals', 'storage', 'funcspec', )
    attr_names = ('quals', 'storage', 'funcspec',)


class Default(Node):
    def __init__(self,statement,coord=None):
        self.statement=statement
        self.assign_id()
        self.coord=coord
        self.children=[]
    
    def make_children(self):
        if self.statement is not None: self.children.append(("statement", self.statement))
    
    attr_names = ( )


class DoWhile(Node):
    def __init__(self,expr,stmt=None,coord=None):
        self.expr=expr
        self.stmt=stmt
        self.assign_id()
        self.coord=coord
        self.children=[]
    
    def make_children(self):
        if self.expr is not None: self.children.append(("expr", self.expr))
        if self.stmt is not None: self.children.append(("stmt", self.stmt))
    
    attr_names = ()

class Ellipsis(Node):
    def __init__(self, coord=None):
        self.coord = coord
        self.assign_id()
        self.children=[]

    def make_children(self):
        pass

    attr_names = ()

class Enum(Node):
    def __init__(self, name, values, coord=None):
        self.name = name
        self.values = values
        self.assign_id()
        self.coord = coord
        self.children=[]
    
    def make_children(self):
        if self.values is not None: self.children.append(("values", self.values))

    attr_names = ('name', )

class Enumerator(Node):
    def __init__(self, name, value, coord=None):
        self.name = name
        self.value = value
        self.assign_id()
        self.coord = coord
        self.children=[]
    
    def make_children(self):
        if self.value is not None: self.children.append(("value", self.value))

    attr_names = ('name', )

class EnumeratorList(Node):
    def __init__(self, enumerators, coord=None):
        self.enumerators = enumerators
        self.assign_id()
        self.coord = coord
        self.children = []

    def make_children(self):
        for i, child in enumerate(self.enumerators or []):
            self.children.append(("enumerators[%d]" % i, child))

    attr_names = ()


class ExprList(Statements):
    def __init__(self, exprs ,exptype, coord=None):
        self.exprs=exprs
        self.exptype=exptype
        self.coord=coord
        self.place=None
        super(ExprList,self).__init__()

    def make_children(self):
        if self.exprs is not None: 
            for i,child in enumerate(self.exprs):
                self.children.append(("exprs[%d]"%i, child))

    attr_names = ()


class FileAST(Node):
    def __init__(self,ext,coord=None):
        self.ext=ext
        self.assign_id()
        self.coord= coord
        self.children=[]
    
    def make_children(self):
        if self.ext:
            for i, child in enumerate(self.ext):
                self.children.append(("ext[%d]" % i, child))

    attr_names = ()


class For(Statements):
    def __init__(self,init,cond,update,stmt=None,coord=None):
        self.init=init
        self.cond=cond
        self.update=update
        self.stmt=stmt
        self.coord= coord
        super(For,self).__init__()

    def make_children(self):
        if self.init:
            for i,child in enumerate(self.init):
                if child:
                    self.children.append(("init[%d]"%i,child))
        if self.cond is not None: self.children.append(("cond", self.cond))
        if self.update is not None: self.children.append(("update", self.update))
        if self.stmt is not None: self.children.append(("stmt", self.stmt))
    
    attr_names = ()

class FuncCall(Statements):
    def __init__(self, name, args, exptype= None, coord=None):
        self.args=args
        self.name=name
        self.exptype = exptype
        self.coord = coord
        self.place = None
        super(FuncCall,self).__init__()

    def make_children(self):
        if self.name is  not None: self.children.append(("name", self.name))
        if self.args is  not None: self.children.append(("args", self.args))

    attr_names = ()

class FuncDecl(Node):
    def __init__(self, args,type, coord=None):
        self.args=args
        self.type=type
        self.assign_id()
        self.coord = coord
        self.children=[]
    
    def make_children(self):
        if self.type is not None: self.children.append(("type", self.type))
        if self.args is not None: self.children.append(("args", self.args))

    attr_names = ()


class FuncDef(Node):
    def __init__(self, spec , decl, body, coord=None):
        self.spec=spec
        self.decl = decl
        self.body = body
        self.assign_id()
        self.coord = coord
        self.children=[]
    
    def make_children(self):
        if self.decl is not None: self.children.append(("decl", self.decl))
        if self.body is not None: self.children.append(("body", self.body))

    attr_names = ()


class Goto(Statements):
    def __init__(self, name=None, coord=None):
        self.name=name
        self.coord=coord
        super(Goto,self).__init__()

    def make_children(self):
        if self.name is not None: self.children.append(("name", self.name))

    attr_names = ('name',)

class ID(Statements):
    def __init__(self,name,exptype=None,coord=None):
        self.name = name
        self.exptype = exptype
        self.place = name
        self.coord = coord
        super(ID,self).__init__()
    
    def make_children(self):
        pass
    
    attr_names = ('name', )

class IdentifierType(Node):
    def __init__(self,name,coord=None):
        self.name=name
        self.assign_id()
        self.coord=coord
        self.children=[]
    
    def make_children(self):
        pass
    
    attr_names = ('name', )


class If(Statements):
    def __init__(self,expr,ifstmt=None,elsestmt=None,coord=None):
        self.expr=expr
        self.ifstmt=ifstmt
        self.elsestmt=elsestmt
        self.coord=coord
        super(If,self).__init__()

    def make_children(self):
        if self.expr is not None: self.children.append(("expr", self.expr))
        if self.ifstmt is not None: self.children.append(("ifstmt", self.ifstmt))
        if self.elsestmt is not None: self.children.append(("elsestmt", self.elsestmt))
    
    attr_names = ()


class InitList(Node):
    def __init__(self, exprs , coord=None):
        self.exprs=exprs
        self.assign_id()
        self.coord=coord
        self.children = []

    def make_children(self):
        if self.exprs is not None: 
            for i,child in enumerate(self.exprs):
                self.children.append(("exprs[%d]"%i, child))

    attr_names = ()


class Input(Statements):
    def __init__(self, expr, coord=None):
        self.expr=expr
        self.coord=coord
        super(Input,self).__init__()

    def make_children(self):
        pass

    attr_names = ()

class Labelled(Statements):
    def __init__(self, name,statement, coord=None):
        self.statement=statement
        self.name=name
        self.coord = coord
        super(Labelled,self).__init__()

    def make_children(self):
        if self.name is not None: self.children.append(("name", self.name))
        if self.statement is not None: self.children.append(("statement", self.statement))

    attr_names = ('name',)

class Output(Statements):
    def __init__(self, expr, coord=None):
        self.expr=expr
        self.coord=coord
        super(Output,self).__init__()
        
    def make_children(self):
        pass

    attr_names = ()

class ParamDecl(Node):
    def __init__(self, spec, decl , coord=None):
        self.spec=spec
        self.assign_id()
        self.decl=decl
        self.coord=coord
        self.children = []

    def make_children(self):
        if self.decl is not None: self.children.append(("decl", self.decl))

    attr_names = ()

class ParamList(Node):
    def __init__(self, params , coord=None):
        self.params=params
        self.assign_id()
        self.coord=coord
        self.children = []

    def make_children(self):
        if self.params is not None: 
            for i,child in enumerate(self.params):
                self.children.append(("params[%d]"%i, child))

    attr_names = ()


class PtrDecl(Node):
    def __init__(self, quals, type, coord=None):
        self.quals=quals
        self.type=type
        self.assign_id()
        self.coord=coord
        self.children = []
    
    def make_children(self):
        if self.type is not None: self.children.append(("type", self.type))

    attr_names = ('quals', )


class Return(Statements):
    def __init__(self, expr=None, coord=None):
        self.expr=expr
        self.coord=coord
        super(Return,self).__init__()
        
    def make_children(self):
        if self.expr is not None: self.children.append(("expr", self.expr))

    attr_names = ()

class StructOrUnion(Node):
    def __init__(self, s_or_u, name, decls, size, coord=None):
        self.s_or_u= s_or_u
        self.name = name
        self.decls = decls
        self.coord = coord
        self.size = size
        self.assign_id()
        self.children = []
    
    def make_children(self):
        for i, child in enumerate(self.decls or []):
            self.children.append(("decls[%d]" % i, child))
    
    attr_names = ('type', )

class StructRef(Statements):
    def __init__(self, name, type, field, exptype=None, coord=None):
        self.name = name
        self.type = type
        self.field = field
        self.coord = coord
        self.exptype = exptype
        self.place = None
        super(StructRef,self).__init__()
    
    def make_children(self):
        if self.name is not None: self.children.append(("name", self.name))
        if self.field is not None: self.children.append(("field", self.field))

    attr_names = ('type', )


class Switch(Node):
    def __init__(self,expr,stmt=None,coord=None):
        self.expr=expr
        self.stmt=stmt
        self.assign_id()
        self.coord=coord
        self.children=[]
    
    def make_children(self):
        if self.expr is not None: self.children.append(("expr", self.expr))
        if self.stmt is not None: self.children.append(("stmt", self.stmt))
    
    attr_names = ()


class TernaryOp(Statements):
    def __init__(self, cond ,iftrue,iffalse,exptype=None,coord=None):
        self.cond=cond
        self.iftrue=iftrue
        self.iffalse=iffalse
        self.exptype=exptype
        if type(exptype) is str:
            self.optype='('+exptype+')'
        else:
            self.optype='('+exptype.__class__.__name__+')'
        super(TernaryOp,self).__init__()
    
    def make_children(self):
        if self.cond is not None: self.children.append(("cond", self.cond))
        if self.iftrue is not None: self.children.append(("iftrue", self.iftrue))
        if self.iffalse is not None: self.children.append(("iffalse", self.iffalse))

    attr_names = ('optype',)


class TypeDeclaration(Node):
    def __init__(self,name,quals,type,coord=None):
        self.name=name
        self.type = type
        self.quals = quals
        self.assign_id()
        self.coord = coord
        self.children = []
    
    def make_children(self):
        if self.type:
            self.children.append(("type",self.type))

    attr_names = ('name', 'quals', )

class TypeName(Node):
    def __init__(self,name,quals,type,exptype=None,coord=None):
        self.name=name
        self.quals = quals
        self.type = type
        self.exptype = exptype
        self.assign_id()
        self.coord = coord
        self.children = []
    
    def make_children(self):
        if self.type:
            self.children.append(("type",self.type))

    attr_names = ('name', 'quals', )

class UnaryOp(Statements):
    def __init__(self,op,expr=None,exptype=None,coord=None):
        self.op=op
        self.expr=expr
        self.exptype = exptype
        self.place = None
        self.coord=coord
        super(UnaryOp,self).__init__()

    def make_children(self):
        if self.expr is not None: self.children.append(("expr", self.expr))
    
    attr_names = ('op',)


class While(Statements):
    def __init__(self,expr,stmt=None,coord=None):
        self.expr=expr
        self.stmt=stmt
        self.coord=coord
        super(While,self).__init__()

    def make_children(self):
        if self.expr is not None: self.children.append(("expr", self.expr))
        if self.stmt is not None: self.children.append(("stmt", self.stmt))
    
    attr_names = ()
