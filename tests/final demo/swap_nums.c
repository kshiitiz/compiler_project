int swap(int *a, int *b){
	int temp= *a;
	*a = *b;
	*b = temp;
}
int main(){
	int a,b;
	cout <<< "Enter first number" <<<endl;
	cin >>> a;
	cout <<< "Enter second number" <<<endl;
	cin >>> b;
	cout<<< "Before Swap numbers: "<<< a <<< ','<<<b<<<endl;
	swap(&a, &b);
	cout<<< "Swapped numbers: "<<< a <<< ','<<<b<<<endl;
}