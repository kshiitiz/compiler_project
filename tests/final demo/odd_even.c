int odd(int number){
	if (number==0){ 
		return 0;
	}
	else{
		return even(number-1);
	}
}
 
int even(int number){
	if(number==0){ 
		return 1;
	}
	else{
		return odd(number-1);
	}
}

int main ()
{
	int number;
	cout <<< "Enter positive number"<<< endl;
	cin >>> number;
	
	if(odd(number)==1){
		cout <<< number<<<" is odd."<<<endl;
	}
	else {
		cout <<< number<<<" is even."<<<endl;
	}
	return 0;
}