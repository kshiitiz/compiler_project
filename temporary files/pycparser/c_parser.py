#------------------------------------------------------------------------------
# pycparser: c_parser.py
#
# CParser class: Parser and AST builder for the C language
#
# Eli Bendersky [http://eli.thegreenplace.net]
# License: BSD
#------------------------------------------------------------------------------
import re
import ply.yacc as yacc
import c_ast
from c_lexer import CLexer
from plyparser import PLYParser, Coord, ParseError
from ast_transforms import fix_switch_cases
import sys
inFile = sys.argv[1]

id_count = 0
def getid():
    global id_count 
    id_count += 1
    return id_count


class Node:
    def __init__(self,name,children=None):
        self.name = name
        if children:
            self.children = children
        else:
            self.children = []
        self.id = getid()
        print self.id," [label=",self.name,"]"
    
    def graph_dfs(self):
        if self.children:
            # print "Bacche: ",self.id
            # print self.children
            # print self.id,":",len(self.children)
            # for child in self.children:
            #     print child.id
            for child in self.children:
            
                print self.id,"--",child.id
                child.graph_dfs()


class CParser(PLYParser):
    def __init__(
            self,
            lex_optimize=True,
            lexer=CLexer,
            lextab='pycparser.lextab',
            yacc_optimize=True,
            yacctab='pycparser.yacctab',
            yacc_debug=False,
            taboutputdir=''):
        """ Create a new CParser.

            Some arguments for controlling the debug/optimization
            level of the parser are provided. The defaults are
            tuned for release/performance mode.
            The simple rules for using them are:
            *) When tweaking CParser/CLexer, set these to False
            *) When releasing a stable parser, set to True

            lex_optimize:
                Set to False when you're modifying the lexer.
                Otherwise, changes in the lexer won't be used, if
                some lextab.py file exists.
                When releasing with a stable lexer, set to True
                to save the re-generation of the lexer table on
                each run.

            lexer:
                Set this parameter to define the lexer to use if
                you're not using the default CLexer.

            lextab:
                Points to the lex table that's used for optimized
                mode. Only if you're modifying the lexer and want
                some tests to avoid re-generating the table, make
                this point to a local lex table file (that's been
                earlier generated with lex_optimize=True)

            yacc_optimize:
                Set to False when you're modifying the parser.
                Otherwise, changes in the parser won't be used, if
                some parsetab.py file exists.
                When releasing with a stable parser, set to True
                to save the re-generation of the parser table on
                each run.

            yacctab:
                Points to the yacc table that's used for optimized
                mode. Only if you're modifying the parser, make
                this point to a local yacc table file

            yacc_debug:
                Generate a parser.out file that explains how yacc
                built the parsing table from the grammar.

            taboutputdir:
                Set this parameter to control the location of generated
                lextab and yacctab files.
        """
        self.clex = lexer(
            error_func=self._lex_error_func,
            on_lbrace_func=self._lex_on_lbrace_func,
            on_rbrace_func=self._lex_on_rbrace_func,
            type_lookup_func=self._lex_type_lookup_func)

        self.clex.build(
            optimize=lex_optimize,
            lextab=lextab,
            outputdir=taboutputdir)
        self.tokens = self.clex.tokens

        # rules_with_opt = [
        #     'abstract_declarator',
        #     'assignment_expression',
        #     'declaration_list',
        #     'declaration_specifiers',
        #     'designation',
        #     'expression',
        #     'identifier_list',
        #     'init_declarator_list',
        #     'initializer_list',
        #     'parameter_type_list',
        #     'specifier_qualifier_list',
        #     'block_item_list',
        #     'type_qualifier_list',
        #     'struct_declarator_list'
        # ]

        # for rule in rules_with_opt:
        #     self._create_opt_rule(rule)


        self.cparser = yacc.yacc(
            module=self,
            start='translation_unit_or_empty',
            debug=yacc_debug,
            optimize=yacc_optimize,
            tabmodule=yacctab,
            outputdir=taboutputdir)

        # Stack of scopes for keeping track of symbols. _scope_stack[-1] is
        # the current (topmost) scope. Each scope is a dictionary that
        # specifies whether a name is a type. If _scope_stack[n][name] is
        # True, 'name' is currently a type in the scope. If it's False,
        # 'name' is used in the scope but not as a type (for instance, if we
        # saw: int name;
        # If 'name' is not a key in _scope_stack[n] then 'name' was not defined
        # in this scope at all.
        self._scope_stack = [dict()]

        # Keeps track of the last token given to yacc (the lookahead token)
        self._last_yielded_token = None

    def parse(self, text, filename='', debuglevel=0):
        """ Parses C code and returns an AST.

            text:
                A string containing the C source code

            filename:
                Name of the file being parsed (for meaningful
                error messages)

            debuglevel:
                Debug level to yacc
        """
        self.clex.filename = filename
        self.clex.reset_lineno()
        self._scope_stack = [dict()]
        self._last_yielded_token = None
        return self.cparser.parse(
                input=text,
                lexer=self.clex,
                debug=debuglevel)

    ######################--   PRIVATE   --######################

    def _push_scope(self):
        self._scope_stack.append(dict())

    def _pop_scope(self):
        assert len(self._scope_stack) > 1
        self._scope_stack.pop()

    def _add_typedef_name(self, name, coord):
        """ Add a new typedef name (ie a TYPEID) to the current scope
        """
        if not self._scope_stack[-1].get(name, True):
            self._parse_error(
                "Typedef %r previously declared as non-typedef "
                "in this scope" % name, coord)
        self._scope_stack[-1][name] = True

    def _add_identifier(self, name, coord):
        """ Add a new object, function, or enum member name (ie an ID) to the
            current scope
        """
        if self._scope_stack[-1].get(name, False):
            self._parse_error(
                "Non-typedef %r previously declared as typedef "
                "in this scope" % name, coord)
        self._scope_stack[-1][name] = False

    def _is_type_in_scope(self, name):
        """ Is *name* a typedef-name in the current scope?
        """
        for scope in reversed(self._scope_stack):
            # If name is an identifier in this scope it shadows typedefs in
            # higher scopes.
            in_scope = scope.get(name)
            if in_scope is not None: return in_scope
        return False

    def _lex_error_func(self, msg, line, column):
        self._parse_error(msg, self._coord(line, column))

    def _lex_on_lbrace_func(self):
        self._push_scope()

    def _lex_on_rbrace_func(self):
        self._pop_scope()

    def _lex_type_lookup_func(self, name):
        """ Looks up types that were previously defined with
            typedef.
            Passed to the lexer for recognizing identifiers that
            are types.
        """
        is_type = self._is_type_in_scope(name)
        return is_type

    def _get_yacc_lookahead_token(self):
        """ We need access to yacc's lookahead token in certain cases.
            This is the last token yacc requested from the lexer, so we
            ask the lexer.
        """
        return self.clex.last_token

    # To understand what's going on here, read sections A.8.5 and
    # A.8.6 of K&R2 very carefully.
    #
    # A C type consists of a basic type declaration, with a list
    # of modifiers. For example:
    #
    # int *c[5];
    #
    # The basic declaration here is 'int c', and the pointer and
    # the array are the modifiers.
    #
    # Basic declarations are represented by TypeDecl (from module c_ast) and the
    # modifiers are FuncDecl, PtrDecl and ArrayDecl.
    #
    # The standard states that whenever a new modifier is parsed, it should be
    # added to the end of the list of modifiers. For example:
    #
    # K&R2 A.8.6.2: Array Declarators
    #
    # In a declaration T D where D has the form
    #   D1 [constant-expression-opt]
    # and the type of the identifier in the declaration T D1 is
    # "type-modifier T", the type of the
    # identifier of D is "type-modifier array of T"
    #
    # This is what this method does. The declarator it receives
    # can be a list of declarators ending with TypeDecl. It
    # tacks the modifier to the end of this list, just before
    # the TypeDecl.
    #
    # Additionally, the modifier may be a list itself. This is
    # useful for pointers, that can come as a chain from the rule
    # p_pointer. In this case, the whole modifier list is spliced
    # into the new location.
    def _type_modify_decl(self, decl, modifier):
        """ Tacks a type modifier on a declarator, and returns
            the modified declarator.

            Note: the declarator and modifier may be modified
        """
        #~ print '****'
        #~ decl.show(offset=3)
        #~ modifier.show(offset=3)
        #~ print '****'

        modifier_head = modifier
        modifier_tail = modifier

        # The modifier may be a nested list. Reach its tail.
        #
        while modifier_tail.type:
            modifier_tail = modifier_tail.type

        # If the decl is a basic type, just tack the modifier onto
        # it
        #
        if isinstance(decl, c_ast.TypeDecl):
            modifier_tail.type = decl
            return modifier
        else:
            # Otherwise, the decl is a list of modifiers. Reach
            # its tail and splice the modifier onto the tail,
            # pointing to the underlying basic type.
            #
            decl_tail = decl

            while not isinstance(decl_tail.type, c_ast.TypeDecl):
                decl_tail = decl_tail.type

            modifier_tail.type = decl_tail.type
            decl_tail.type = modifier_head
            return decl

    # Due to the order in which declarators are constructed,
    # they have to be fixed in order to look like a normal AST.
    #
    # When a declaration arrives from syntax construction, it has
    # these problems:
    # * The innermost TypeDecl has no type (because the basic
    #   type is only known at the uppermost declaration level)
    # * The declaration has no variable name, since that is saved
    #   in the innermost TypeDecl
    # * The typename of the declaration is a list of type
    #   specifiers, and not a node. Here, basic identifier types
    #   should be separated from more complex types like enums
    #   and structs.
    #
    # This method fixes these problems.
    #
    def _fix_decl_name_type(self, decl, typename):
        """ Fixes a declaration. Modifies decl.
        """
        # Reach the underlying basic type
        #
        type = decl
        while not isinstance(type, c_ast.TypeDecl):
            type = type.type

        decl.name = type.declname
        type.quals = decl.quals

        # The typename is a list of types. If any type in this
        # list isn't an IdentifierType, it must be the only
        # type in the list (it's illegal to declare "int enum ..")
        # If all the types are basic, they're collected in the
        # IdentifierType holder.
        #
        for tn in typename:
            if not isinstance(tn, c_ast.IdentifierType):
                if len(typename) > 1:
                    self._parse_error(
                        "Invalid multiple types specified", tn.coord)
                else:
                    type.type = tn
                    return decl

        if not typename:
            # Functions default to returning int
            #
            if not isinstance(decl.type, c_ast.FuncDecl):
                self._parse_error(
                        "Missing type in declaration", decl.coord)
            type.type = c_ast.IdentifierType(
                    ['int'],
                    coord=decl.coord)
        else:
            # At this point, we know that typename is a list of IdentifierType
            # nodes. Concatenate all the names into a single list.
            #
            type.type = c_ast.IdentifierType(
                [name for id in typename for name in id.names],
                coord=typename[0].coord)
        return decl

    def _add_declaration_specifier(self, declspec, newspec, kind):
        """ Declaration specifiers are represented by a dictionary
            with the entries:
            * qual: a list of type qualifiers
            * storage: a list of storage type qualifiers
            * type: a list of type specifiers
            * function: a list of function specifiers

            This method is given a declaration specifier, and a
            new specifier of a given kind.
            Returns the declaration specifier, with the new
            specifier incorporated.
        """
        spec = declspec or dict(qual=[], storage=[], type=[], function=[])
        spec[kind].insert(0, newspec)
        return spec

    def _build_declarations(self, spec, decls, typedef_namespace=False):
        """ Builds a list of declarations all sharing the given specifiers.
            If typedef_namespace is true, each declared name is added
            to the "typedef namespace", which also includes objects,
            functions, and enum constants.
        """
        is_typedef = 'typedef' in spec['storage']
        declarations = []

        # Bit-fields are allowed to be unnamed.
        #
        if decls[0].get('bitsize') is not None:
            pass

        # When redeclaring typedef names as identifiers in inner scopes, a
        # problem can occur where the identifier gets grouped into
        # spec['type'], leaving decl as None.  This can only occur for the
        # first declarator.
        #
        elif decls[0]['decl'] is None:
            if len(spec['type']) < 2 or len(spec['type'][-1].names) != 1 or \
                    not self._is_type_in_scope(spec['type'][-1].names[0]):
                coord = '?'
                for t in spec['type']:
                    if hasattr(t, 'coord'):
                        coord = t.coord
                        break
                self._parse_error('Invalid declaration', coord)

            # Make this look as if it came from "direct_declarator:ID"
            decls[0]['decl'] = c_ast.TypeDecl(
                declname=spec['type'][-1].names[0],
                type=None,
                quals=None,
                coord=spec['type'][-1].coord)
            # Remove the "new" type's name from the end of spec['type']
            del spec['type'][-1]

        # A similar problem can occur where the declaration ends up looking
        # like an abstract declarator.  Give it a name if this is the case.
        #
        elif not isinstance(decls[0]['decl'],
                (c_ast.Struct, c_ast.Union, c_ast.IdentifierType)):
            decls_0_tail = decls[0]['decl']
            while not isinstance(decls_0_tail, c_ast.TypeDecl):
                decls_0_tail = decls_0_tail.type
            if decls_0_tail.declname is None:
                decls_0_tail.declname = spec['type'][-1].names[0]
                del spec['type'][-1]

        for decl in decls:
            assert decl['decl'] is not None
            if is_typedef:
                declaration = c_ast.Typedef(
                    name=None,
                    quals=spec['qual'],
                    storage=spec['storage'],
                    type=decl['decl'],
                    coord=decl['decl'].coord)
            else:
                declaration = c_ast.Decl(
                    name=None,
                    quals=spec['qual'],
                    storage=spec['storage'],
                    funcspec=spec['function'],
                    type=decl['decl'],
                    init=decl.get('init'),
                    bitsize=decl.get('bitsize'),
                    coord=decl['decl'].coord)

            if isinstance(declaration.type,
                    (c_ast.Struct, c_ast.Union, c_ast.IdentifierType)):
                fixed_decl = declaration
            else:
                fixed_decl = self._fix_decl_name_type(declaration, spec['type'])

            # Add the type name defined by typedef to a
            # symbol table (for usage in the lexer)
            #
            if typedef_namespace:
                if is_typedef:
                    self._add_typedef_name(fixed_decl.name, fixed_decl.coord)
                else:
                    self._add_identifier(fixed_decl.name, fixed_decl.coord)

            declarations.append(fixed_decl)

        return declarations

    def _build_function_definition(self, spec, decl, param_decls, body):
        """ Builds a function definition.
        """
        assert 'typedef' not in spec['storage']

        declaration = self._build_declarations(
            spec=spec,
            decls=[dict(decl=decl, init=None)],
            typedef_namespace=True)[0]

        return c_ast.FuncDef(
            decl=declaration,
            param_decls=param_decls,
            body=body,
            coord=decl.coord)

    def _select_struct_union_class(self, token):
        """ Given a token (either STRUCT or UNION), selects the
            appropriate AST class.
        """
        if token == 'struct':
            return c_ast.Struct
        else:
            return c_ast.Union

    ##
    ## Precedence and associativity of operators
    ##
    precedence = (
        ('left', 'LOR'),
        ('left', 'LAND'),
        ('left', 'OR'),
        ('left', 'XOR'),
        ('left', 'AND'),
        ('left', 'EQ', 'NE'),
        ('left', 'GT', 'GE', 'LT', 'LE'),
        ('left', 'RSHIFT', 'LSHIFT'),
        ('left', 'PLUS', 'MINUS'),
        ('left', 'TIMES', 'DIVIDE', 'MOD')
    )

    ##
    ## Grammar productions
    ## Implementation of the BNF defined in K&R2 A.13
    ##

    # Wrapper around a translation unit, to allow for empty input.
    # Not strictly part of the C99 Grammar, but useful in practice.
    #
    def p_translation_unit_or_empty(self, p):
        """ translation_unit_or_empty   : translation_unit
                                        | empty
        """

        p[0] = Node("translation_unit_or_empty",[p[1]])
        p[0].graph_dfs()

    def p_translation_unit_1(self, p):
        """ translation_unit    : external_declaration
        """
        # Note: external_declaration is already a list
        #
        p[0] = Node("translation_unit",[p[1]])

    def p_translation_unit_2(self, p):
        """ translation_unit    : translation_unit external_declaration
        """

        p[0]=Node("translation_unit",[p[1],p[2]])

    # Declarations always come as lists (because they can be
    # several in one line), so we wrap the function definition
    # into a list as well, to make the return value of
    # external_declaration homogenous.
    #
    def p_external_declaration_1(self, p):
        """ external_declaration    : function_definition
        """
        p[0] = Node("external_declaration",[p[1]])

    def p_external_declaration_2(self, p):
        """ external_declaration    : declaration
        """
        p[0] = Node("external_declaration",[p[1]])

    def p_external_declaration_3(self, p):
        """ external_declaration    : pp_directive
                                    | pppragma_directive
        """
        p[0] = Node("external_declaration",[p[1]])


    def p_external_declaration_4(self, p):
        """ external_declaration    : SEMI
        """
        a=Node('SEMI')
        p[0] = Node("external_declaration",[a])


    def p_pp_directive(self, p):
        """ pp_directive  : PPHASH
        """
        # self._parse_error('Directives not supported yet',
        #                   self._coord(p.lineno(1)))
        a=Node("PPHASH")
        p[0]=Node("pp_directive",[a])

    def p_pppragma_directive(self, p):
        """ pppragma_directive      : PPPRAGMA
                                    | PPPRAGMA PPPRAGMASTR
        """
        if len(p) == 3:
            a=Node("PPPRAGMA")
            b=Node("PPPRAGMASTR")
            p[0]=Node("pppragma_directive",[a,b])
        else:
            a=Node("PPPRAGMA")
            p[0]=Node("pppragma_directive",[a])

    # In function definitions, the declarator can be followed by
    # a declaration list, for old "K&R style" function definitios.
    #
    def p_function_definition_1(self, p):
        """ function_definition : declarator declaration_list_opt compound_statement
        """
        p[0]=Node("function_definition",[p[1],p[2],p[3]])

    def p_function_definition_2(self, p):
        """ function_definition : declaration_specifiers declarator declaration_list_opt compound_statement
        """

        p[0]=Node("function_definition",[p[1],p[2],p[3],p[4]])

    def p_statement(self, p):
        """ statement   : labeled_statement
                        | expression_statement
                        | compound_statement
                        | selection_statement
                        | iteration_statement
                        | jump_statement
                        | pppragma_directive
        """
        p[0]=Node("statement",[p[1]])

    # In C, declarations can come several in a line:
    #   int x, *px, romulo = 5;
    #
    # However, for the AST, we will split them to separate Decl
    # nodes.
    #
    # This rule splits its declarations and always returns a list
    # of Decl nodes, even if it's one element long.
    #
    def p_decl_body(self, p):
        """ decl_body : declaration_specifiers init_declarator_list_opt
        """
        p[0]=Node("decl_body",[p[1],p[2]])

    # The declaration has been split to a decl_body sub-rule and
    # SEMI, because having them in a single rule created a problem
    # for defining typedefs.
    #
    # If a typedef line was directly followed by a line using the
    # type defined with the typedef, the type would not be
    # recognized. This is because to reduce the declaration rule,
    # the parser's lookahead asked for the token after SEMI, which
    # was the type from the next line, and the lexer had no chance
    # to see the updated type symbol table.
    #
    # Splitting solves this problem, because after seeing SEMI,
    # the parser reduces decl_body, which actually adds the new
    # type into the table to be seen by the lexer before the next
    # line is reached.
    def p_declaration(self, p):
        """ declaration : decl_body SEMI
        """
        a=Node("SEMI")
        p[0]=Node("declaration",[p[1],a])

    # Since each declaration is a list of declarations, this
    # rule will combine all the declarations and return a single
    # list
    #
    def p_abstract_declarator_opt(self, p):
        """ abstract_declarator_opt  	:  empty 
        						 		| abstract_declarator """
        p[0]=Node("abstract_declarator_opt",[p[1]])

    def p_assignment_expression_opt(self, p):
        """ assignment_expression_opt  	:  empty 
        						 		| assignment_expression """
        p[0]=Node("assignment_expression_opt",[p[1]])

    def p_declaration_list_opt(self, p):
        """ declaration_list_opt  	:  empty 
        						 	| declaration_list """
        p[0]=Node("declaration_list_opt",[p[1]])

    def p_declaration_specifiers_opt(self, p):
        """ declaration_specifiers_opt  :  empty 
        						 		| declaration_specifiers """
        p[0]=Node("declaration_specifiers_opt",[p[1]])

    def p_designation_opt(self, p):
        """ designation_opt  	:  empty 
        						| designation """
        p[0]=Node("designation_opt",[p[1]])

    def p_expression_opt(self, p):
        """ expression_opt  :  empty 
        					| expression """
        p[0]=Node("expression_opt",[p[1]])                               

    def p_identifier_list_opt(self, p):
        ''' identifier_list_opt :  identifier_list 
        						| empty '''
        p[0]=Node("identifier_list_opt",[p[1]])                               

    def p_init_declarator_list_opt(self, p):
        """ init_declarator_list_opt  	:  empty 
        						 		| init_declarator_list """
        p[0]=Node("init_declarator_list_opt",[p[1]])                               

    def p_initializer_list_opt(self, p):
        """ initializer_list_opt  	:  empty 
        						 	| initializer_list """
        p[0]=Node("initializer_list_opt",[p[1]]) 

    def p_parameter_type_list_opt(self, p):
        """ parameter_type_list_opt  	:  empty 
        						 		| parameter_type_list """
        p[0]=Node("parameter_type_list_opt",[p[1]])                               

    def p_specifier_qualifier_list_opt(self, p):
        """ specifier_qualifier_list_opt  	:  empty 
        						 			| specifier_qualifier_list """
        p[0]=Node("specifier_qualifier_list_opt",[p[1]])                               

    def p_block_item_list_opt(self, p):
        """ block_item_list_opt  :  empty 
        						 | block_item_list """
        p[0]=Node("block_item_list_opt",[p[1]])                               

    def p_type_qualifier_list_opt(self, p):
        """ type_qualifier_list_opt  	:  empty 
        						 		| type_qualifier_list """
        p[0]=Node("type_qualifier_list_opt",[p[1]])                               

    def p_struct_declarator_list_opt(self, p):
        """ struct_declarator_list_opt  :  empty 
        						 		| struct_declarator_list """
        p[0]=Node("struct_declarator_list_opt",[p[1]])


    def p_declaration_list(self, p):
        """ declaration_list    : declaration
                                | declaration_list declaration
        """
        if len(p) == 2:
            p[0] = Node("declaration_list",[p[1]]) 
        else:
            p[0] = Node("declaration_list",[p[1],p[2]])

    def p_declaration_specifiers_1(self, p):
        """ declaration_specifiers  : type_qualifier declaration_specifiers_opt
        """
        p[0]=Node("declaration_specifiers",[p[1],p[2]])

    def p_declaration_specifiers_2(self, p):
        """ declaration_specifiers  : type_specifier declaration_specifiers_opt
        """
        p[0]=Node("declaration_specifiers",[p[1],p[2]])

    def p_declaration_specifiers_3(self, p):
        """ declaration_specifiers  : storage_class_specifier declaration_specifiers_opt
        """
        p[0]=Node("declaration_specifiers",[p[1],p[2]])

    def p_declaration_specifiers_4(self, p):
        """ declaration_specifiers  : function_specifier declaration_specifiers_opt
        """
        p[0]=Node("declaration_specifiers",[p[1],p[2]])

    def p_storage_class_specifier(self, p):
        """ storage_class_specifier : AUTO
                                    | REGISTER
                                    | STATIC
                                    | EXTERN
                                    | TYPEDEF
        """
        #To-Do
        a=Node(p[1])
        p[0]=Node("storage_class_specifier",[a])

    def p_function_specifier(self, p):
        """ function_specifier  : INLINE
        """
        a=Node("INLINE")
        p[0] = Node("function_specifier",[a])


    def p_type_specifier_1(self, p):
        """ type_specifier  : VOID
                            | _BOOL
                            | CHAR
                            | SHORT
                            | INT
                            | LONG
                            | FLOAT
                            | DOUBLE
                            | _COMPLEX
                            | SIGNED
                            | UNSIGNED
                            | __INT128
        """
        #To-Do
        a=Node(p[1])
        p[0]=Node("type_specifier",[a]);

    def p_type_specifier_2(self, p):
        """ type_specifier  : typedef_name
                            | enum_specifier
                            | struct_or_union_specifier
        """
        p[0] = Node("type_specifier",[p[1]])

    def p_type_qualifier(self, p):
        """ type_qualifier  : CONST
                            | RESTRICT
                            | VOLATILE
        """

        #To-Do
        a=Node(p[1])
        p[0]=Node("type_qualifier",[a])

    def p_init_declarator_list_1(self, p):
        """ init_declarator_list    : init_declarator
                                    | init_declarator_list COMMA init_declarator
        """
        if len(p)==4:
            a=Node("COMMA")
            p[0]=Node("init_declarator_list",[p[1],a,p[3]])
        else:
            p[0]=Node("init_declarator_list",[p[1]]) 

    # If the code is declaring a variable that was declared a typedef in an
    # outer scope, yacc will think the name is part of declaration_specifiers,
    # not init_declarator, and will then get confused by EQUALS.  Pass None
    # up in place of declarator, and handle this at a higher level.
    #
    def p_init_declarator_list_2(self, p):
        """ init_declarator_list    : EQUALS initializer
        """
        a=Node("EQUALS")
        p[0]=Node("init_declarator_list",[a,p[2]])

    # Similarly, if the code contains duplicate typedefs of, for example,
    # array types, the array portion will appear as an abstract declarator.
    #
    def p_init_declarator_list_3(self, p):
        """ init_declarator_list    : abstract_declarator
        """
        p[0]=Node("init_declarator_list",[p[1]])

    # Returns a {decl=<declarator> : init=<initializer>} dictionary
    # If there's no initializer, uses None
    #
    def p_init_declarator(self, p):
        """ init_declarator : declarator
                            | declarator EQUALS initializer
        """
        if len(p)==2:
            p[0]=Node("init_declarator",[p[1]])
        else:
            a=Node("EQUALS");
            p[0]=Node("init_declarator",[p[1],a,p[3]])

    def p_specifier_qualifier_list_1(self, p):
        """ specifier_qualifier_list    : type_qualifier specifier_qualifier_list_opt
        """
        p[0]=Node("specifier_qualifier_list",[p[1],p[2]])

    def p_specifier_qualifier_list_2(self, p):
        """ specifier_qualifier_list    : type_specifier specifier_qualifier_list_opt
        """
        p[0]=Node("specifier_qualifier_list",[p[1],p[2]])


    # TYPEID is allowed here (and in other struct/enum related tag names), because
    # struct/enum tags reside in their own namespace and can be named the same as types
    #
    def p_struct_or_union_specifier_1(self, p):
        """ struct_or_union_specifier   : struct_or_union ID
                                        | struct_or_union TYPEID
        """

        #To-Do
        a=Node("ID")
        p[0]=Node("struct_or_union_specifier",[p[1],a])


    def p_struct_or_union_specifier_2(self, p):
        """ struct_or_union_specifier : struct_or_union brace_open struct_declaration_list brace_close
        """
        # klass = self._select_struct_union_class(p[1])
        # p[0] = klass(
        #     name=None,
        #     decls=p[3],
        #     coord=self._coord(p.lineno(2)))
        p[0]=Node("struct_or_union_specifier",[p[1],p[2],p[3],p[4]])

    def p_struct_or_union_specifier_3(self, p):
        """ struct_or_union_specifier   : struct_or_union ID brace_open struct_declaration_list brace_close
                                        | struct_or_union TYPEID brace_open struct_declaration_list brace_close
        """
        #To-Do
        a=Node("ID")
        p[0]=Node("struct_or_union_specifier",[p[1],a,p[3],p[4],p[5]])

    def p_struct_or_union(self, p):
        """ struct_or_union : STRUCT
                            | UNION
        """
        a=Node("STRUCT")
        p[0]=Node("struct_or_union",[a])

    # Combine all declarations into a single list
    #
    def p_struct_declaration_list(self, p):
        """ struct_declaration_list     : struct_declaration
                                        | struct_declaration_list struct_declaration
        """
        if len(p) == 2:
            p[0] = Node("struct_declaration_list",[p[1]])
        else:
            p[0] = Node("struct_declaration_list",[p[1],p[2]])

    def p_struct_declaration_1(self, p):

        """ struct_declaration : specifier_qualifier_list struct_declarator_list_opt SEMI
        """

        a=Node("SEMI")
        p[0]=Node("struct_declaration",[p[1],p[2],a])

    def p_struct_declaration_2(self, p):
        """ struct_declaration : specifier_qualifier_list abstract_declarator SEMI
        """

        a=Node("SEMI")
        p[0]=Node("struct_declaration",[p[1],p[2],a])

    def p_struct_declaration_3(self, p):
        """ struct_declaration : SEMI
        """
        a=Node("SEMI")
        p[0]=Node("struct_declaration",[a])

    def p_struct_declarator_list(self, p):
        """ struct_declarator_list  : struct_declarator
                                    | struct_declarator_list COMMA struct_declarator
        """
        if len(p)==4:
            a=Node("COMMA")
            p[0]=Node("struct_declarator_list",[p[1],a,p[3]])
        else:
            p[0]=Node("struct_declarator_list",[p[1]])

    # struct_declarator passes up a dict with the keys: decl (for
    # the underlying declarator) and bitsize (for the bitsize)
    #
    def p_struct_declarator_1(self, p):
        """ struct_declarator : declarator
        """
        p[0]=Node("struct_declarator",[p[1]])

    def p_struct_declarator_2(self, p):
        """ struct_declarator   : declarator COLON constant_expression
                                | COLON constant_expression
        """
        if len(p) > 3:
            a=Node("COLON")
            p[0]=Node("struct_declarator",[p[1],a,p[3]])
        else:
            a=Node("COLON")
            p[0]=Node("struct_declarator",[a,p[2]])

    def p_enum_specifier_1(self, p):
        """ enum_specifier  : ENUM ID
                            | ENUM TYPEID
        """
        a=Node("ENUM")
        b=Node("ID")
        p[0]=Node("enum_specifier",[a,b])

    def p_enum_specifier_2(self, p):
        """ enum_specifier  : ENUM brace_open enumerator_list brace_close
        """
        a=Node("ENUM")
        p[0]=Node("enum_specifier",[a,p[2],p[3],p[4]])

    def p_enum_specifier_3(self, p):
        """ enum_specifier  : ENUM ID brace_open enumerator_list brace_close
                            | ENUM TYPEID brace_open enumerator_list brace_close
        """
        #To-do
        a=Node("ENUM")
        b=Node("ID")
        p[0]=Node("enum_specifier",[a,b,p[3],p[4],p[4]])

    def p_enumerator_list(self, p):
        """ enumerator_list : enumerator
                            | enumerator_list COMMA
                            | enumerator_list COMMA enumerator
        """
        if len(p) == 2:
            p[0] = Node("enumerator_list",[p[1]])
        elif len(p) == 3:
            a=Node("COMMA")
            p[0]=Node("enumerator_list",[p[1],a])
        else:
            a=Node("COMMA")
            p[0]=Node("enumerator_list",[p[1],a,p[3]])

    def p_enumerator(self, p):
        """ enumerator  : ID
                        | ID EQUALS constant_expression
        """
        if len(p) == 2:
            a=Node("ID")
            p[0]=Node("enumerator",[a])


        else:
            a=Node("ID")
            b=Node("EQUALS")
            p[0]=Node("enumerator",[a,b,p[3]])

    def p_declarator_1(self, p):
        """ declarator  : direct_declarator
        """
        p[0]=Node("declarator",[p[1]])

    def p_declarator_2(self, p):
        """ declarator  : pointer direct_declarator
        """
        p[0] = Node("declarator",[p[1],p[2]])

    # Since it's impossible for a type to be specified after a pointer, assume
    # it's intended to be the name for this declaration.  _add_identifier will
    # raise an error if this TYPEID can't be redeclared.
    #
    def p_declarator_3(self, p):
        """ declarator  : pointer TYPEID
        """
        a=Node("TYPEID")
        p[0]=Node("declarator",[p[1],a])

    def p_direct_declarator_1(self, p):
        """ direct_declarator   : ID
        """
        a=Node("ID")
        p[0] = Node("direct_declarator",[a])

    def p_direct_declarator_2(self, p):
        """ direct_declarator   : LPAREN declarator RPAREN
        """
        a=Node("LPAREN")
        b=Node("RPAREN")
        p[0]=Node("direct_declarator",[a,p[2],b])

    def p_direct_declarator_3(self, p):
        """ direct_declarator   : direct_declarator LBRACKET type_qualifier_list_opt assignment_expression_opt RBRACKET
        """
        a=Node("LBRACKET")
        b=Node("RBRACKET")
        p[0]=Node("direct_declarator",[p[1],a,p[3],p[4],b])


    def p_direct_declarator_4(self, p):
        """ direct_declarator   : direct_declarator LBRACKET STATIC type_qualifier_list_opt assignment_expression RBRACKET
        """
        a=Node("LBRACKET")
        b=Node("STATIC")
        c=Node("RBRACKET")
        p[0]=Node("direct_declarator",[p[1],a,b,p[4],p[5],c])

    def p_direct_declarator_5(self, p):
        """ direct_declarator   : direct_declarator LBRACKET type_qualifier_list STATIC assignment_expression RBRACKET
        """
        a=Node("LBRACKET")
        b=Node("STATIC")
        c=Node("RBRACKET")
        p[0]=Node("direct_declarator",[p[1],a,p[3],b,p[5],c])

    # Special for VLAs
    #
    def p_direct_declarator_6(self, p):
        """ direct_declarator   : direct_declarator LBRACKET type_qualifier_list_opt TIMES RBRACKET
        """
        a=Node("LBRACKET")
        b=Node("TIMES")
        c=Node("RBRACKET")
        p[0]=Node("direct_declarator",[p[1],a,p[3],b,c])

    def p_direct_declarator_7(self, p):
        """ direct_declarator   : direct_declarator LPAREN parameter_type_list RPAREN
                                | direct_declarator LPAREN identifier_list_opt RPAREN
        """
        a=Node("LPAREN")
        b=Node("RPAREN")
        p[0]=Node("direct_declarator",[p[1],a,p[3],b])



    def p_pointer(self, p):
        """ pointer : TIMES type_qualifier_list_opt
                    | TIMES type_qualifier_list_opt pointer
        """
        a=Node("TIMES")
        if len(p) == 3:
            p[0]=Node("pointer",[a,p[2]])
        else:
            p[0] = Node("pointer",[a,p[2],p[3]])

    def p_type_qualifier_list(self, p):
        """ type_qualifier_list : type_qualifier
                                | type_qualifier_list type_qualifier
        """
        if len(p)==2:
            p[0]=Node("type_qualifier_list",[p[1]])
        else:
            p[0]=Node("type_qualifier_list",[p[1],p[2]])

    def p_parameter_type_list(self, p):
        """ parameter_type_list : parameter_list
                                | parameter_list COMMA ELLIPSIS
        """
        if len(p) > 2:
            a=Node("COMMA")
            b=Node("ELLIPSIS")
            p[0]=Node("parameter_type_list",[p[1],a,b])
        else:
            # print "here"
            # print p[1].name
            p[0]=Node("parameter_type_list",[p[1]])

    def p_parameter_list(self, p):
        """ parameter_list  : parameter_declaration
                            | parameter_list COMMA parameter_declaration
        """
        if len(p) == 2: # single parameter
            p[0] = Node("parameter_list",[p[1]])
        else:
            a=Node("COMMA")
            p[0]=Node("parameter_type_list",[p[1],a,p[3]])

    def p_parameter_declaration_1(self, p):
        """ parameter_declaration   : declaration_specifiers declarator
        """
        p[0]=Node("parameter_declaration",[p[1],p[2]])

    def p_parameter_declaration_2(self, p):
        """ parameter_declaration   : declaration_specifiers abstract_declarator_opt
        """
        p[0]=Node("parameter_declaration",[p[1],p[2]])

    def p_identifier_list(self, p):
        """ identifier_list : identifier
                            | identifier_list COMMA identifier
        """
        if len(p) == 2: # single parameter
            p[0] = Node("identifier_list",[p[1]])
        else:
            a=Node("COMMA")
            p[0]= Node("identifier_list",[p[1],a,p[3]])

    def p_initializer_1(self, p):
        """ initializer : assignment_expression
        """
        p[0] = Node("initializer",[p[1]])

    def p_initializer_2(self, p):
        """ initializer : brace_open initializer_list_opt brace_close
                        | brace_open initializer_list COMMA brace_close
        """
        if len(p)==4:
            p[0] = Node("initializer",[p[1],p[2],p[3]])
        else:
            a=Node("COMMA")
            p[0] = Node("initializer",[p[1],p[2],a,p[4]])

    def p_initializer_list(self, p):
        """ initializer_list    : designation_opt initializer
                                | initializer_list COMMA designation_opt initializer
        """
        if len(p) == 3: # single initializer
            p[0]=Node("initializer_list",[p[1],p[2]])
        else:
            a=Node("COMMA")
            p[0]=Node("initializer_list",[p[1],a,p[3],p[4]])

    def p_designation(self, p):
        """ designation : designator_list EQUALS
        """
        a=Node("EQUALS")
        p=Node("designation",[p[1],a])



    # Designators are represented as a list of nodes, in the order in which
    # they're written in the code.
    #
    def p_designator_list(self, p):
        """ designator_list : designator
                            | designator_list designator
        """
        if len(p)==2:
            p=Node("designator_list",[p[1]])
        else:
            p=Node("designator_list",[p[1],p[2]])

    def p_designator(self, p):
        """ designator  : LBRACKET constant_expression RBRACKET
                        | PERIOD identifier
        """
        if len(p)==4:
            a=Node("LBRACKET")
            b=Node("RBRACKET")
            p[0]=Node("designator",[a,p[2],b])
        else:
            a=Node("PERIOD")
            p[0]=Node("designator",[a,p[2]])

    def p_type_name(self, p):
        """ type_name   : specifier_qualifier_list abstract_declarator_opt
        """
        p[0]=Node("type_name",[p[1],p[2]])

    def p_abstract_declarator_1(self, p):
        """ abstract_declarator     : pointer
        """
        p[0]=Node("abstract_declarator",[p[1]])

    def p_abstract_declarator_2(self, p):
        """ abstract_declarator     : pointer direct_abstract_declarator
        """
        p[0]=Node("abstract_declarator",[p[1],p[2]])

    def p_abstract_declarator_3(self, p):
        """ abstract_declarator     : direct_abstract_declarator
        """
        p[0]=Node("abstract_declarator",[p[1]])

    # Creating and using direct_abstract_declarator_opt here
    # instead of listing both direct_abstract_declarator and the
    # lack of it in the beginning of _1 and _2 caused two
    # shift/reduce errors.
    #
    def p_direct_abstract_declarator_1(self, p):
        """ direct_abstract_declarator  : LPAREN abstract_declarator RPAREN """
        a=Node("LPAREN")
        b=Node("RPAREN")
        p[0]=Node("direct_abstract_declarator",[a,p[2],b])

    def p_direct_abstract_declarator_2(self, p):
        """ direct_abstract_declarator  : direct_abstract_declarator LBRACKET assignment_expression_opt RBRACKET
        """
        a=Node("LBRACKET")
        b=Node("RBRACKET")
        p[0]=Node("direct_abstract_declarator",[p[1],a,p[3],b])

    def p_direct_abstract_declarator_3(self, p):
        """ direct_abstract_declarator  : LBRACKET assignment_expression_opt RBRACKET
        """
        a=Node("LBRACKET")
        b=Node("RBRACKET")
        p[0]=Node("direct_abstract_declarator",[a,p[2],b])

    def p_direct_abstract_declarator_4(self, p):
        """ direct_abstract_declarator  : direct_abstract_declarator LBRACKET TIMES RBRACKET
        """
        a=Node("LBRACKET")
        b=Node("RBRACKET")
        c=Node("TIMES")
        p[0]=Node("direct_abstract_declarator",[p[1],a,c,b])

    def p_direct_abstract_declarator_5(self, p):
        """ direct_abstract_declarator  : LBRACKET TIMES RBRACKET
        """
        a=Node("LBRACKET")
        b=Node("RBRACKET")
        c=Node("TIMES")
        p[0]=Node("direct_abstract_declarator",[a,c,b])

    def p_direct_abstract_declarator_6(self, p):
        """ direct_abstract_declarator  : direct_abstract_declarator LPAREN parameter_type_list_opt RPAREN
        """
        a=Node("LPAREN")
        b=Node("RPAREN")
        p[0]=Node("direct_abstract_declarator",[p[1],a,p[3],b])

    def p_direct_abstract_declarator_7(self, p):
        """ direct_abstract_declarator  : LPAREN parameter_type_list_opt RPAREN
        """
        a=Node("LPAREN")
        b=Node("RPAREN")
        p[0]=Node("direct_abstract_declarator",[a,p[2],b])

    # declaration is a list, statement isn't. To make it consistent, block_item
    # will always be a list
    #
    def p_block_item(self, p):
        """ block_item  : declaration
                        | statement
        """
        p[0]=Node("block_item",[p[1]])
    # Since we made block_item a list, this just combines lists
    #
    def p_block_item_list(self, p):
        """ block_item_list : block_item
                            | block_item_list block_item
        """
        # Empty block items (plain ';') produce [None], so ignore them
        if len(p) == 2:
            p[0]=Node("block_item_list",[p[1]])
        else:
            p[0]=Node("block_item_list",[p[1],p[2]])

    def p_compound_statement_1(self, p):
        """ compound_statement : brace_open block_item_list_opt brace_close """
        p[0] = Node("compound_statement",[p[1],p[2],p[3]])

    def p_labeled_statement_1(self, p):
        """ labeled_statement : ID COLON statement """
        a=Node("ID")
        b=Node("COLON")
        p[0]=Node("labeled_statement",[a,b,p[3]])

    def p_labeled_statement_2(self, p):
        """ labeled_statement : CASE constant_expression COLON statement """
        a=Node("CASE")
        b=Node("COLON")
        p[0]=Node("labeled_statement",[a,p[2],b,p[4]])

    def p_labeled_statement_3(self, p):
        """ labeled_statement : DEFAULT COLON statement """
        a=Node("DEFAULT")
        b=Node("COLON")
        p[0]=Node("labeled_statement",[a,b,p[3]])

    def p_selection_statement_1(self, p):
        """ selection_statement : IF LPAREN expression RPAREN statement """
        a=Node("IF")
        b=Node("LPAREN")
        c=Node("RPAREN")
        p[0]=Node("selection_statement",[a,b,p[3],c,p[5]])
    def p_selection_statement_2(self, p):
        """ selection_statement : IF LPAREN expression RPAREN statement ELSE statement """
        a=Node("IF")
        b=Node("LPAREN")
        c=Node("RPAREN")
        d=Node("ELSE")
        p[0]=Node("selection_statement",[a,b,p[3],c,p[5],d,p[7]])

    def p_selection_statement_3(self, p):
        """ selection_statement : SWITCH LPAREN expression RPAREN statement """
        a=Node("SWITCH")
        b=Node("LPAREN")
        c=Node("RPAREN")
        p[0]=Node("selection_statement",[a,b,p[3],c,p[5]])

    def p_iteration_statement_1(self, p):
        """ iteration_statement : WHILE LPAREN expression RPAREN statement """
        a=Node("WHILE")
        b=Node("LPAREN")
        c=Node("RPAREN")
        p[0]=Node("iteration_statement",[a,b,p[3],c,p[5]])

    def p_iteration_statement_2(self, p):
        """ iteration_statement : DO statement WHILE LPAREN expression RPAREN SEMI """
        a=Node("DO")
        b=Node("WHILE")
        c=Node("LPAREN")
        d=Node("RPAREN")
        e=Node("SEMI")
        p[0]=Node("iteration_statement",[a,p[2],b,c,p[5],d,e])

    def p_iteration_statement_3(self, p):
        """ iteration_statement : FOR LPAREN expression_opt SEMI expression_opt SEMI expression_opt RPAREN statement """
        a=Node("FOR")
        b=Node("LPAREN")
        c=Node("SEMI")
        d=Node("SEMI")
        e=Node("RPAREN")
        p[0]=Node("iteration_statement",[a,b,p[3],c,p[5],d,p[7],e,p[9]])


    def p_iteration_statement_4(self, p):
        """ iteration_statement : FOR LPAREN declaration expression_opt SEMI expression_opt RPAREN statement """
        a=Node("FOR")
        b=Node("LPAREN")
        c=Node("SEMI")
        d=Node("RPAREN")
        p[0]=Node("iteration_statement",[a,b,p[3],p[4],c,p[6],d,p[8]])

    def p_jump_statement_1(self, p):
        """ jump_statement  : GOTO ID SEMI """
        a=Node("GOTO")
        b=Node("ID")
        c=Node("SEMI")
        p[0]=Node("jump_statement",[a,b,c])

    def p_jump_statement_2(self, p):
        """ jump_statement  : BREAK SEMI """
        a=Node("BREAK")
        b=Node("SEMI")
        p[0]=Node("jump_statement",[a,b])
        

    def p_jump_statement_3(self, p):
        """ jump_statement  : CONTINUE SEMI """
        a=Node("CONTINUE")
        b=Node("SEMI")
        p[0]=Node("jump_statement",[a,b])

    def p_jump_statement_4(self, p):
        """ jump_statement  : RETURN expression SEMI
                            | RETURN SEMI
        """
        a=Node("RETURN")
        b=Node("SEMI")
        if len(p)==4:
            p[0]=Node("jump_statement",[a,p[2],b])
        else:
            p[0]=Node("jump_statement",[a,b])

    def p_expression_statement(self, p):
        """ expression_statement : expression_opt SEMI """
        a=Node("SEMI")
        p[0]=Node("expression_statement",[p[1],a])

    def p_expression(self, p):
        """ expression  : assignment_expression
                        | expression COMMA assignment_expression
        """
        if len(p) == 2:
            p[0] = Node("expression",[p[1]])
        else:
            a=Node("COMMA")
            p[0]=Node("expression",[p[1],a,p[3]])


    def p_typedef_name(self, p):
        """ typedef_name : TYPEID """
        a=Node("TYPEID")
        p[0]=Node("typedef_name",[a])

    def p_assignment_expression(self, p):
        """ assignment_expression   : conditional_expression
                                    | unary_expression assignment_operator assignment_expression
        """
        if len(p) == 2:
            p[0] = Node("assignment_expression",[p[1]])
        else:
            p[0] = Node("assignment_expression",[p[1],p[2],p[3]])

    # K&R2 defines these as many separate rules, to encode
    # precedence and associativity. Why work hard ? I'll just use
    # the built in precedence/associativity specification feature
    # of PLY. (see precedence declaration above)
    #
    def p_assignment_operator(self, p):
        """ assignment_operator : EQUALS
                                | XOREQUAL
                                | TIMESEQUAL
                                | DIVEQUAL
                                | MODEQUAL
                                | PLUSEQUAL
                                | MINUSEQUAL
                                | LSHIFTEQUAL
                                | RSHIFTEQUAL
                                | ANDEQUAL
                                | OREQUAL
        """
        a=Node("EQUALS")
        p[0] = Node("assignment_operator",[a])

    def p_constant_expression(self, p):
        """ constant_expression : conditional_expression """
        p[0] = Node("constant_expression",[p[1]])

    def p_conditional_expression(self, p):
        """ conditional_expression  : binary_expression
                                    | binary_expression CONDOP expression COLON conditional_expression
        """
        if len(p) == 2:
            p[0] = Node("conditional_expression",[p[1]])
        else:
            a=Node("CONDOP")
            b= Node("COLON")

            p[0] = Node("conditional_expression",[p[1],a,p[3],b,p[5]])

    def p_binary_expression(self, p):
        """ binary_expression   : cast_expression
                                | binary_expression TIMES binary_expression
                                | binary_expression DIVIDE binary_expression
                                | binary_expression MOD binary_expression
                                | binary_expression PLUS binary_expression
                                | binary_expression MINUS binary_expression
                                | binary_expression RSHIFT binary_expression
                                | binary_expression LSHIFT binary_expression
                                | binary_expression LT binary_expression
                                | binary_expression LE binary_expression
                                | binary_expression GE binary_expression
                                | binary_expression GT binary_expression
                                | binary_expression EQ binary_expression
                                | binary_expression NE binary_expression
                                | binary_expression AND binary_expression
                                | binary_expression OR binary_expression
                                | binary_expression XOR binary_expression
                                | binary_expression LAND binary_expression
                                | binary_expression LOR binary_expression
        """
        if len(p) == 2:
            p[0] = Node("binary_expression",[p[1]])
        else:
            a=Node("MATHS")
            p[0] = Node("binary_expression",[p[1],a,p[3]])

    def p_cast_expression_1(self, p):
        """ cast_expression : unary_expression """
        p[0] = Node("cast_expression",[p[1]])

    def p_cast_expression_2(self, p):
        """ cast_expression : LPAREN type_name RPAREN cast_expression """
        a=Node("LPAREN")
        b=Node("RPAREN")
        p[0]=Node("cast_expression",[a,p[2],b,p[4]])
    def p_unary_expression_1(self, p):
        """ unary_expression    : postfix_expression """
        p[0] = Node("unary_expression",[p[1]])

    def p_unary_expression_2(self, p):
        """ unary_expression    : PLUSPLUS unary_expression
        """
        a=Node("PLUSPLUS")
        p[0]=Node("unary_expression",[a,p[2]])


    def p_unary_expression_4(self, p):
        """ unary_expression    : MINUSMINUS unary_expression
        """        
        a=Node("MINUSMINUS")
        p[0]=Node("unary_expression",[a,p[2]])

    def p_unary_expression_5(self, p):
        """ unary_expression    : unary_operator cast_expression
        """
        p[0]=Node("unary_expression",[p[1],p[2]])

    def p_unary_expression_6(self, p):
        """ unary_expression    : SIZEOF unary_expression
                                | SIZEOF LPAREN type_name RPAREN
        """
        if len(p)==3:
            a=Node("SIZEOF")
            p[0]=Node("unary_expression",[a,p[2]])
        else:
            a=Node("SIZEOF")
            b=Node("LPAREN")
            c=Node("RPAREN")
            p[0]=Node("unary_expression",[a,b,p[3],c])

    def p_unary_operator(self, p):
        """ unary_operator  : AND
                            | TIMES
                            | PLUS
                            | MINUS
                            | NOT
                            | LNOT
        """
        a=Node("AND")
        p[0] = Node("unary_operator",[a])

    def p_postfix_expression_1(self, p):
        """ postfix_expression  : primary_expression """
        p[0] = Node("postfix_expression",[p[1]])

    def p_postfix_expression_2(self, p):
        """ postfix_expression  : postfix_expression LBRACKET expression RBRACKET """
        a=Node("LBRACKET")
        b=Node("RBRACKET")
        p[0]=Node("postfix_expression",[p[1],a,p[3],b])

    def p_postfix_expression_3(self, p):
        """ postfix_expression  : postfix_expression LPAREN argument_expression_list RPAREN
                                | postfix_expression LPAREN RPAREN
        """
        a=Node("LPAREN")
        b=Node("RPAREN")

        if len(p)==4:
            p[0]=Node("postfix_expression",[p[1],a,p[3],b])
        else:
            p[0]=Node("postfix_expression",[p[1],a,b])

    def p_postfix_expression_4(self, p):
        """ postfix_expression  : postfix_expression PERIOD ID
                                | postfix_expression PERIOD TYPEID
                                | postfix_expression ARROW ID
                                | postfix_expression ARROW TYPEID
        """
        a=Node("PERIOD")
        b=Node("ID")
        p[0]=Node("postfix_expression",[p[1],a,b])

    def p_postfix_expression_5(self, p):
        """ postfix_expression  : postfix_expression PLUSPLUS
                                | postfix_expression MINUSMINUS
        """
        a=Node("PLUSPLUS")
        p[0] = Node("postfix_expression",[p[1],a])

    def p_postfix_expression_6(self, p):
        """ postfix_expression  : LPAREN type_name RPAREN brace_open initializer_list brace_close
                                | LPAREN type_name RPAREN brace_open initializer_list COMMA brace_close
        """
        if len(p)==7:
            a=Node("LPAREN")
            b=Node("RPAREN")
            p[0]=Node("postfix_expression",[a,p[1],b,p[4],p[5],p[6]])
        else:
            a=Node("LPAREN")
            b=Node("RPAREN")
            c=Node("COMMA")
            p[0]=Node("postfix_expression",[a,p[1],b,p[4],p[5],c,p[7]])

    def p_primary_expression_1(self, p):
        """ primary_expression  : identifier """
        p[0] = Node("primary_expression",[p[1]])

    def p_primary_expression_2(self, p):
        """ primary_expression  : constant """
        p[0] = Node("primary_expression",[p[1]])

    def p_primary_expression_3(self, p):
        """ primary_expression  : unified_string_literal
                                | unified_wstring_literal
        """
        p[0] = Node("primary_expression",[p[1]])

    def p_primary_expression_4(self, p):
        """ primary_expression  : LPAREN expression RPAREN """
        a=Node("LPAREN")
        b=Node("RPAREN")
        p[0] = Node("primary_expression",[a,p[2],b])

    def p_primary_expression_5(self, p):
        """ primary_expression  : OFFSETOF LPAREN type_name COMMA offsetof_member_designator RPAREN
        """
        a=Node("OFFSETOF")
        b=Node("LPAREN")
        c=Node("COMMA")
        d=Node("RPAREN")
        p[0]=Node("primary_expression",[a,b,p[3],c,p[5],d])

    def p_offsetof_member_designator(self, p):
        """ offsetof_member_designator : identifier
                                         | offsetof_member_designator PERIOD identifier
                                         | offsetof_member_designator LBRACKET expression RBRACKET
        """
        if len(p) == 2:
            p[0] = Node("offsetof_member_designator",[p[1]])
        elif len(p) == 4:
            a=Node("PERIOD")
            p[0]=Node("offsetof_member_designator",[p[1],a,p[3]])
        elif len(p) == 5:
            a=Node("RBRACKET")
            b=Node("LBRACKET")
            p[0]=Node("offsetof_member_designator",[p[1],b,p[3],a])
        else:
            raise NotImplementedError("Unexpected parsing state. len(p): %u" % len(p))

    def p_argument_expression_list(self, p):
        """ argument_expression_list    : assignment_expression
                                        | argument_expression_list COMMA assignment_expression
        """
        if len(p) == 2: # single expr
            p[0] = Node("argument_expression_list",p[1])
        else:
            a=Node("COMMA")
            p[0]=Node("argument_expression_list",[p[1],a,p[3]])

    def p_identifier(self, p):
        """ identifier  : ID """
        a=Node("ID")
        p[0]=Node("identifier",[a])

    def p_constant_1(self, p):
        """ constant    : INT_CONST_DEC
                        | INT_CONST_OCT
                        | INT_CONST_HEX
                        | INT_CONST_BIN
        """
        a=Node("INT_CONST_HEX")
        p[0]=Node("constant",[a])

    def p_constant_2(self, p):
        """ constant    : FLOAT_CONST
                        | HEX_FLOAT_CONST
        """
        a=Node("FLOAT_CONST")
        p[0]=Node("constant",[a])     

    def p_constant_3(self, p):
        """ constant    : CHAR_CONST
                        | WCHAR_CONST
        """
        a=Node("CHAR_CONST")
        p[0]=Node("constant",[a])

    # The "unified" string and wstring literal rules are for supporting
    # concatenation of adjacent string literals.
    # I.e. "hello " "world" is seen by the C compiler as a single string literal
    # with the value "hello world"
    #
    def p_unified_string_literal(self, p):
        """ unified_string_literal  : STRING_LITERAL
                                    | unified_string_literal STRING_LITERAL
        """
        a=Node("STRING_LITERAL")

        if len(p) == 2: # single literal
            p[0] = Node("unified_string_literal",a)
        else:
            p[0] = Node("unified_string_literal",[p[1],a])

    def p_unified_wstring_literal(self, p):
        """ unified_wstring_literal : WSTRING_LITERAL
                                    | unified_wstring_literal WSTRING_LITERAL
        """
        a=Node("WSTRING_LITERAL")

        if len(p) == 2: # single literal
            p[0] = Node("unified_wstring_literal",a)
        else:
            p[0] = Node("unified_wstring_literal",[p[1],a])

    def p_brace_open(self, p):
        """ brace_open  :   LBRACE
        """
        a=Node("LBRACE")
        p[0]=Node("brace_open",[a])

    def p_brace_close(self, p):
        """ brace_close :   RBRACE
        """
        a=Node("RBRACE")
        p[0]=Node("brace_close",[a])

    def p_empty(self, p):
        'empty : '
        p[0] = Node("empty")

    def p_error(self, p):
        # If error recovery is added here in the future, make sure
        # _get_yacc_lookahead_token still works!
        #
        if p:
            self._parse_error(
                'before: %s' % p.value,
                self._coord(lineno=p.lineno,
                            column=self.clex.find_tok_column(p)))
        else:
            self._parse_error('At end of input', self.clex.filename)


#------------------------------------------------------------------------------
if __name__ == "__main__":

    parser=CParser()
    print "graph{"
    print "ordering=out"
    with open(inFile,'r') as i:
        text = i.read()
        parser.parse(text)
    print"}"

    #t1 = time.time()
    #parser = CParser(lex_optimize=True, yacc_debug=True, yacc_optimize=False)
    #sys.write(time.time() - t1)

    #buf = '''
        #int (*k)(int);
    #'''

    ## set debuglevel to 2 for debugging
    #t = parser.parse(buf, 'x.c', debuglevel=0)
    #t.show(showcoord=True)

