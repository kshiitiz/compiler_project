flushOrLoad_temp:
    move $s5, $sp
    add $s7, $s5, $s6
    jr  $ra
exit:
    li      $v0, 10
    syscall

print_integer:
    li      $v0, 1 
    syscall
    jr      $ra

print_char:
    li      $v0, 11 
    syscall
    jr      $ra

print_float:
    li      $v0, 2 
    syscall
    jr      $ra

print_double:
    li      $v0, 3 
    syscall
    jr      $ra

print_string:
    li      $v0, 4
    syscall
    jr      $ra

print_undefined:
    li      $v0, 4
    la      $a0, __undefined__
    syscall
    jr      $ra

print_newline:
    li      $v0, 4
    la      $a0, __newline__
    syscall
    jr      $ra

print_tab:
    li      $v0, 4
    la      $a0, __tab__
    syscall
    jr      $ra

read_integer:
    li      $v0, 5
    syscall
    jr      $ra

read_float:
    li      $v0, 6
    syscall
    jr      $ra

read_double:
    li      $v0, 7
    syscall
    jr      $ra

read_string:
    li      $v0, 8
    syscall
    jr      $ra