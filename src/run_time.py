import execcode
from sys import argv
from tree_parser import make_TAC_ST
ST_list =dict()


def is_ptr(temp,func):
    if ST_list[func].table[temp]['type']=='PtrTemp':
        return True
    else:
        return False

def codeGenerator(inputF,outputF):
    global ST_list
    TAC, ST = make_TAC_ST(inputF)
    RTC = execcode.RunCode(TAC, ST)
    ST_list['GLOBAL']=ST
    param_counter =0;
    for st in ST.children:
        ST_list[st.name]=st

    for func in TAC.code:
        RTC.makeFunc(func)

        #Setup Function Beginning
        if (func == 'main'):
            #Set Main's Frame pointer
            RTC.addCode(['la','$fp','0($sp)',''])

            RTC.freeAll()
            RTC.addCode(['li','$v0',ST_list[func].size,''])
            RTC.addCode(['sub','$sp','$sp','$v0'])

        elif func !='GLOBAL':
            #Set Stack Pointer
            RTC.addCode("#Function Setup")
            RTC.addCode(['sub','$sp','$sp','72'])
            #Set Return Address
            RTC.addCode(['sw','$ra','0($sp)',''])
            #Save previous frame pointer
            RTC.addCode(['sw','$fp','4($sp)',''])
            #Set new frame pointer
            RTC.addCode(['la','$fp','72($sp)',''])

            #store prev value of stack pointer in 8($sp)
            RTC.addCode(['sw','$fp','8($sp)',''])
             
            #Storing parent's registers
            for num in range(10):
                RTC.addCode(['sw','$t'+str(num),str(12+4*num)+'($sp)',''])
            for num in range(5):
                RTC.addCode(['sw','$s'+str(num),str(52+4*num)+'($sp)',''])
            RTC.addCode("#Finished Function Setup")

            #free registers from parent
            RTC.freeAll()

            RTC.addCode("#Space for locals and temporaries")

            RTC.addCode(['li','$v0',ST_list[func].size,''])
            RTC.addCode(['sub','$sp','$sp','$v0'])
            RTC.addCode("#Parameters")

            parameter_list = ST_list['GLOBAL'].table[func]['type']['type'].args.params 
            for x in range(len(parameter_list)):
                var_name = ST_list[func].addressDescriptor.keys()[x]
                if x<4:
                    var_offset = ST_list[func].table[var_name]['offset']
                    RTC.addCode(['sw','$a'+str(x),str(var_offset)+'($sp)',''])
                # setting the value stored in memory flag to true
                ST_list[func].addressDescriptor[var_name]['store']=True

        #Setup Function Body
        lineno=-1
        for line in TAC.code[func]:
            lineno+=1
            if line[3] == '=':
                reg1 = RTC.getReg(line[0])
                reg2 = RTC.getReg(line[1])
                if is_ptr(line[0],func) :
                    RTC.addCode(['sw',reg2,'('+reg1+')',''])
                else:
                    RTC.addCode(['move',reg1,reg2,''])


                RTC.flushRegister(line[0])
                RTC.clearRegister(line[1])

            elif line[3] == '=i':
                reg1 = RTC.getReg(line[0])
                if line[2]=='char':
                    RTC.addCode(['li',reg1,'\''+line[1]+'\'',''])
                else:
                    RTC.addCode(['li',reg1,line[1],''])

                RTC.flushRegister(line[0])

            elif line[3] == '=Arr':
                offset=ST_list[func].addressDescriptor[line[1]]['memory']
                reg1 = RTC.getReg(line[0])
                reg2 = RTC.getReg(line[2])
                if ST_list[func].addressDescriptor[line[1]]['arg']==False:
                    RTC.addCode(['addi',reg1,reg2,offset])
                    RTC.addCode(['la',reg2,'($sp)',''])
                    RTC.addCode(['add',reg1,reg1,reg2])

                else:
                    RTC.addCode(['la',reg1,'($sp)',''])
                    RTC.addCode(['addi',reg1,reg1,offset])
                    RTC.addCode(['lw',reg1,'('+reg1+')',''])
                    RTC.addCode(['add',reg1,reg1,reg2])
                RTC.flushRegister(line[0])
                RTC.clearRegister(line[2])

            elif line[3] == '=Struct':
                reg1 = RTC.getReg(line[0])
                offset=ST_list[func].addressDescriptor[line[1]]['memory']
                RTC.addCode(['la',reg1,'($sp)',''])
                RTC.addCode(['addi',reg1,reg1,offset])
                RTC.flushRegister(line[0])


            elif line[3] == '=REF':
                reg1 = RTC.getReg(line[0])
                reg2 = RTC.getReg(line[1])
                RTC.addCode(['lw',reg1,'('+reg2+')',''])

                RTC.flushRegister(line[0])
                RTC.clearRegister(line[1])

            elif line[3] == 'dot':
                reg1 = RTC.getReg(line[0])
                offset= ST_list[func].addressDescriptor[line[1]]['memory']
                struct_name = ST_list[func].table[line[1]]['type']['type'].type.name
                field_offset= ST_list['GLOBAL'].userTypeTable[struct_name]['decls'][line[2]]['declaration'].offset
                if ST_list[func].addressDescriptor[line[1]]['arg']==False:
                    RTC.addCode(['la',reg1,'($sp)',''])
                    RTC.addCode(['addi',reg1,reg1,offset])
                    RTC.addCode(['addi',reg1,reg1,field_offset])
                else:
                    RTC.addCode(['la',reg1,'($sp)',''])
                    RTC.addCode(['addi',reg1,reg1,offset])
                    RTC.addCode(['lw',reg1,'('+reg1+')',''])
                    RTC.addCode(['addi',reg1,reg1,field_offset])
                RTC.flushRegister(line[0])

            elif line[3] == '+':
                reg1 = RTC.getReg(line[0])
                reg2 = RTC.getReg(line[1])
                reg3 = RTC.getReg(line[2])
                RTC.addCode(['add', reg1, reg2, reg3])

                RTC.flushRegister(line[0])
                RTC.clearRegister(line[1])
                RTC.clearRegister(line[2])

            elif line[3] == 'u+':
                reg1 = RTC.getReg(line[0])
                reg2 = RTC.getReg(line[1])
                RTC.addCode(['move', reg1, reg2, ''])

            elif line[3] == '-':
                reg1 = RTC.getReg(line[0])
                reg2 = RTC.getReg(line[1])
                reg3 = RTC.getReg(line[2])
                RTC.addCode(['sub', reg1, reg2, reg3])

                RTC.flushRegister(line[0])
                RTC.clearRegister(line[1])
                RTC.clearRegister(line[2])

            elif line[3] == 'u-':
                reg1 = RTC.getReg(line[0])
                reg2 = RTC.getReg(line[1])
                RTC.addCode(['sub', reg1, '$0', reg2])


            elif line[3] == 'u!' or line[3] == 'u~':
                reg1 = RTC.getReg(line[0])
                reg2 = RTC.getReg(line[1])
                RTC.addCode(['seq', reg1, '$0', reg2])

            elif line[3] == '*':
                reg1 = RTC.getReg(line[0])
                reg2 = RTC.getReg(line[1])
                reg3 = RTC.getReg(line[2])
                RTC.addCode(['mult', reg2, reg3,''])
                RTC.addCode(['mflo', reg1,'',''])

                RTC.flushRegister(line[0])
                RTC.clearRegister(line[1])
                RTC.clearRegister(line[2])                    

            elif line[3] == '/':
                reg1 = RTC.getReg(line[0])
                reg2 = RTC.getReg(line[1])
                reg3 = RTC.getReg(line[2])
                RTC.addCode(['div', reg2, reg3,''])
                RTC.addCode(['mflo', reg1,'',''])

                RTC.flushRegister(line[0])
                RTC.clearRegister(line[1])
                RTC.clearRegister(line[2])   

            elif line[3] == '%':
                reg1 = RTC.getReg(line[0])
                reg2 = RTC.getReg(line[1])
                reg3 = RTC.getReg(line[2])
                RTC.addCode(['div', reg2, reg3,''])
                RTC.addCode(['mfhi', reg1,'',''])

                RTC.flushRegister(line[0])
                RTC.clearRegister(line[1])
                RTC.clearRegister(line[2])   

            elif line[3] == '<':
                reg1 = RTC.getReg(line[0])
                reg2 = RTC.getReg(line[1])
                reg3 = RTC.getReg(line[2])
                RTC.addCode(['slt', reg1, reg2, reg3])

                RTC.flushRegister(line[0])
                RTC.clearRegister(line[1])
                RTC.clearRegister(line[2])

            elif line[3] == '>':
                reg1 = RTC.getReg(line[0])
                reg2 = RTC.getReg(line[1])
                reg3 = RTC.getReg(line[2])
                RTC.addCode(['sgt', reg1, reg2, reg3])

                RTC.flushRegister(line[0])
                RTC.clearRegister(line[1])
                RTC.clearRegister(line[2])   

            elif line[3] == '<=':
                reg1 = RTC.getReg(line[0])
                reg2 = RTC.getReg(line[1])
                reg3 = RTC.getReg(line[2])
                RTC.addCode(['sle', reg1, reg2, reg3])

                RTC.flushRegister(line[0])
                RTC.clearRegister(line[1])
                RTC.clearRegister(line[2])   

            elif line[3] == '>=':
                reg1 = RTC.getReg(line[0])
                reg2 = RTC.getReg(line[1])
                reg3 = RTC.getReg(line[2])
                RTC.addCode(['sge', reg1, reg2, reg3])

                RTC.flushRegister(line[0])
                RTC.clearRegister(line[1])
                RTC.clearRegister(line[2])   

            elif line[3] == '==':
                reg1 = RTC.getReg(line[0])
                reg2 = RTC.getReg(line[1])
                reg3 = RTC.getReg(line[2])
                RTC.addCode(['seq', reg1, reg2, reg3])

                RTC.flushRegister(line[0])
                RTC.clearRegister(line[1])
                RTC.clearRegister(line[2])   

            elif line[3] == '!=':
                reg1 = RTC.getReg(line[0])
                reg2 = RTC.getReg(line[1])
                reg3 = RTC.getReg(line[2])
                RTC.addCode(['sne', reg1, reg2, reg3])

                RTC.flushRegister(line[0])  
                RTC.clearRegister(line[1])
                RTC.clearRegister(line[2])   

            elif line[3] == 'p++':
                reg1 = RTC.getReg(line[0])
                reg2 = RTC.getReg(line[1])
                RTC.addCode(['move',reg1,reg2,''])
                RTC.addCode(['addi',reg2,reg2,1])

                RTC.flushRegister(line[0]) 
                RTC.flushRegister(line[1]) 


            elif line[3] == 'p--':
                reg1 = RTC.getReg(line[0])
                reg2 = RTC.getReg(line[1])
                RTC.addCode(['move',reg1,reg2,''])
                RTC.addCode(['addiu',reg2,reg2,-1])

                RTC.flushRegister(line[0])
                RTC.flushRegister(line[1]) 

            elif line[3] == '++':
                reg1 = RTC.getReg(line[0])
                reg2 = RTC.getReg(line[1])
                RTC.addCode(['addi',reg1,reg2,1])
                RTC.addCode(['move',reg2,reg1,''])

                RTC.flushRegister(line[0]) 
                RTC.flushRegister(line[1]) 

            elif line[3] == '--':
                reg1 = RTC.getReg(line[0])
                reg2 = RTC.getReg(line[1])
                RTC.addCode(['addiu',reg1,reg2,-1])
                RTC.addCode(['move',reg2,reg1,''])

                RTC.flushRegister(line[0])
                RTC.flushRegister(line[1]) 

            elif line[3] == '&Ptr':
                reg1=RTC.getReg(line[0])
                offset=ST_list[func].addressDescriptor[line[1]]['memory']
                RTC.addCode(['move',reg1,'$sp',''])
                RTC.addCode(['addi',reg1,reg1,offset])
                RTC.flushRegister(line[0])

            elif line[3] == '*Deref':
                reg1=RTC.getReg(line[0])
                reg2=RTC.getReg(line[1])
                RTC.addCode(['move',reg1,reg2,''])

            elif line[3] == 'JUMPBACK':
                RTC.addCode(['b', func + 'end', '', ''])

            elif line[3]=='PRINT' and line[0]=='':
                RTC.addCode(['jal', 'print_newline', '', ''])

            elif line[3]=="SCAN":
                RTC.addCode(['jal','read_integer','',''])
                reg1 = RTC.getReg(line[0])
                if is_ptr(line[0],func) :
                    RTC.addCode(['sw','$v0','('+reg1+')',''])
                else:
                    RTC.addCode(['move',reg1,'$v0',''])

                RTC.flushRegister(line[0])

            elif line[3] == 'RETURN' and line[0]=='':
                RTC.addCode(['b', func + 'end', '', ''])

            elif line[3] == 'RETURN':
                reg1 = RTC.getReg(line[0])

                RTC.addCode(['move', '$v0', reg1, ''])
                RTC.addCode(['b', func + 'end', '', ''])
                RTC.clearRegister(line[0])

            elif line[3] == 'PARAM':
                reg = RTC.getReg(line[0])
                if param_counter<4:
                    RTC.addCode(['move', '$a'+str(param_counter), reg,''])
                else:
                    nextline= lineno+1

                    while(TAC.code[func][nextline][3] != 'CALL'):
                        nextline+=1
                    called_func = TAC.code[func][nextline][2]
                    called_func_size = ST_list[called_func].size
                    var_name = ST_list[called_func].addressDescriptor.keys()[param_counter]
                    var_offset = ST_list[called_func].table[var_name]['offset']
                    var_rel_offset = -(72+called_func_size-var_offset)
                    RTC.addCode(['sw',reg,str(var_rel_offset)+'($sp)',''])
                RTC.clearRegister(line[0])
                param_counter = param_counter +1 

            elif line[3] == 'COND_GOTO_Z':
                reg = RTC.getReg(line[0])
                RTC.addCode(['beq', reg, '$0' ,line[2]])
                RTC.clearRegister(line[0])

            elif line[3] == 'GOTO':
                RTC.addCode(['b',line[2],'',''])

            elif line[3]=='CALL':
                param_counter= 0;
                RTC.addCode(['jal',line[2],'',''])

            elif line[3]=='RET_VAL':
                reg1 = RTC.getReg(line[0])
                RTC.addCode(['move',reg1,'$v0',''])

            elif line[0]=='LABEL':
                RTC.addCode(line)

            elif line[3] == 'PRINT' and line[1] == 'string':
                RTC.addCode(['la','$a0',line[0]+'_'+func,''])
                RTC.addCode(['jal', 'print_string', '', ''])

            elif line[3] == 'PRINT':
                reg = RTC.getReg(line[0])
                RTC.addCode(['move', '$a0', reg, ''])

                if line[1] == 'int':
                    RTC.addCode(['jal', 'print_integer', '', ''])
                elif line[1] == 'char':
                    RTC.addCode(['jal', 'print_char', '', ''])
                elif line[1] == 'float':
                    RTC.addCode(['jal', 'print_float', '', ''])
                RTC.clearRegister(line[0])
            else:
                print line[3]

        #Setup Function End
        if func not in  ['main','GLOBAL']:
            #Function Finish Label
            RTC.addCode(['LABEL',func+'end','',''])

            #Remove Temporaries and Local Data
            RTC.addCode(['add','$sp','$sp',ST_list[func].size])

            #Restore Control Links
            RTC.addCode(['lw','$ra','0($sp)',''])
            RTC.addCode(['lw','$fp','4($sp)',''])

            #Restore parent's register values
            for num in range(10):
                RTC.addCode(['lw','$t'+str(num),str(12+4*num)+'($sp)',''])
            for num in range(5):
                RTC.addCode(['lw','$s'+str(num),str(52+4*num)+'($sp)',''])

            RTC.addCode(['add','$sp','$sp','72'])
            RTC.addCode(['jr','$ra','',''])

        if func == 'main':
            #Function Finish Label
            RTC.addCode(['LABEL',func+'end','',''])
            RTC.addCode(['jal','exit','',''])
    RTC.dumpCode(outputF)


if __name__ == '__main__':
    # Parse the program to get the threeAddressCode
    inputFile = argv[1]
    codeGenerator(inputFile, outputF='ass')

