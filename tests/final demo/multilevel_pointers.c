// To demonstrate multilevel pointers and Depth checking

int main(){
	int a[5][5][5];
	cout <<< "Enter dimensions of 3D Array." <<<endl;
	int p,q,r;
	cin >>> p;
	cin >>> q;
	cin >>> r;
	int i,j,k;
	for (i=0;i<p;i++){
		for (j=0;j<q;j++){
			for(k=0;k<r;k++){
				cout <<< "Enter element a["<<<i<<<"]["<<<j<<<"]["<<<k<<<"]"<<<endl;
				cin>>>a[i][j][k];
			}
		}
	}

	int p0;
	int * p1;
	int ** p2;
	int *** p3;

	// gives an error due to 'depth' mismatch*/
/*	p1 = a ; 
	p2 = a[1][1];*/
	p3 = a ; // Works
	p2 = a[0];
	p1 = a[1][0];
	p0 = a[1][1][0];

	cout <<< "p0: "<<< p0<<<endl;
	cout <<< "p1 dereferenced: " <<< *p1<<<endl;
	cout <<< "p2 dereferenced: " <<< **p2<<<endl;
	cout <<< "p3 dereferenced: " <<< ***p3<<<endl;

	//extra depth added in q


	//gives an error as we are dereferencing non-pointer type
/*	cout <<< "p1 dereferenced: "<<< **p1<<<endl;*/

	int *m= p1; // allowed
	cout <<< "m dereferenced: " <<< *m<<<endl;


	//not allowed - give error
/*	int **n =p1;
	int ***n =p1;
*/
}