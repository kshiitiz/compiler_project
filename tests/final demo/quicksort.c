// int quick(int first,int last,int *x){
int quick(int first, int last, int x[100]){
    int pivot,j,temp,i;
     if(first<last){
         pivot=first;
         i=first;
         j=last;

         while(i<j){
             while(x[i]<=x[pivot]&&i<last){
                i++;
             }
             while(x[j]>x[pivot]){
              j--;
             }
             if(i<j){
                 temp=x[i];
                  x[i]=x[j];
                  x[j]=temp;
             }
         }

         temp=x[pivot];
         x[pivot]=x[j];
         x[j]=temp;
         quick(first,j-1,x);
         quick(j+1,last,x);

    }
}

int main(){
  int i,x[100],size;
  cout <<< "Enter the number of elements"<<<endl;
  cin >>> size;
  cout <<< "Enter the Array elements"<<<endl;
  for(i=0; i<size ; i++){
    cin >>> x[i];
  }
  quick(0,size-1,x);
  cout <<< endl;
  cout <<< "Sorted Array:"<<<endl;
  for(i=0;i<size;i++){
    cout<<<x[i]<<<' ';
  }
  cout <<< endl;
}
