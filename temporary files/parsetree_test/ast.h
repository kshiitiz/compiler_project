/*
 * Declarations for a calculator fb3-1
 */
/* interface to the lexer */
extern int yylineno; /* from lexer */
void yyerror(char *s, ...);
/* nodes in the abstract syntax tree */
struct ast {
 int nodetype;
 struct ast *l;
 struct ast *r;
 int nodeid;
};
struct numval {
 int nodetype; /* type K for constant */
 double number;
 int nodeid;
};
/* build an AST */
struct ast *newast(int nodetype, struct ast *l, struct ast *r);
struct ast *newnum(double d);
int getid();
/* evaluate an AST */
void eval(struct ast *);
/* delete and free an AST */
void treefree(struct ast *);