int main(){

	int marks;
	char grade;
	cout <<< "Enter marks." <<<endl;
	cin >>> marks;
	grade = (marks>80)? ((marks>90)?'A':'B') : (marks>70)?'C':'D' ;
	cout <<< "Grade:"<<<grade<<<endl;

	//Also demonstrating implicit type conversion
	int temp;
	temp = grade +1;
	char next = grade+1;
	cout <<< "Implicit type conversions" <<<endl;
	cout <<< temp <<<' '<<< next <<<endl;

}