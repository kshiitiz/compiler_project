int ackermann(int x, int y);
int main()
{
	int x,y;
	cout <<< "Enter the first number"<<<endl;
	cin >>> x;
	cout <<< "Enter the second number"<<<endl;
	cin >>> y;
	cout <<< "Ackermann Function with inputs "<<< x <<< " and "<<< y <<< " is "<<< ackermann(x,y)<<<endl;
}
int ackermann(int x, int y)
{
	if(x==0)
		return y+1;
	if(y==0)
		return ackermann(x-1,1);
	return ackermann(x-1,ackermann(x,y-1));
}