from collections import OrderedDict
tempcount = 0
class SymTable():

    def __init__(self,parent,depth,name=None,offset=0,scope_count=1):
        self.name=name
        self.children=[]
        self.table=OrderedDict()
        self.parent=parent
        self.userTypeTable=OrderedDict()
        self.depth=depth
        self.offset=offset
        self.size=0
        self.addressDescriptor=OrderedDict()
        self.scope_count = scope_count

    def add_child(self,child):
        assert isinstance(child,SymTable)
        self.children.append(child)

    def print_symbol_table(self,cf,error_func):
        lead = '   '*(self.depth-1)
        print lead+"Scope :%s\tSize:%s\tOffset:%s"%(self.name,self.size,self.offset)
        line = "Scope : %s\n"%self.name
        cf.write(line)
        print lead+"{:<10}{:<10}{:<15}{:<10}{:<10}{:<10}{:<10}".format('Variable','Function','Type','Storage','Qualifers','Size','Offset')
        line ='Variable,Function,Type,Storage,Qualifiers,Size,Offset\n'
        cf.write(line)

        for key,value in self.table.iteritems():
            if not value['defined']:
                error_func("Function %s declared but not defined"%key)
            specifiers = value['type']
            func=[]
            storage=[]
            qual=[]
            if specifiers == 'Temp' or specifiers == 'PtrTemp':
            	type = specifiers
            else:
	            func = specifiers['function']
	            if hasattr(specifiers['type'],"name"):
	                type = specifiers['type'].type.name
	            else:
	                type = specifiers['type'].__class__.__name__
	            storage = specifiers['storage']
	            qual = specifiers['qual']
            size = value['size'] or 0
            offset = value['offset']
            print lead+"{:<10}{:<10}{:<15}{:<10}{:<10}{:<10}{:<10}".format(key,func,type,storage,qual,size,offset)
            line="%s,%s,%s,%s,%s,%s,%s\n"%(key,func,type,storage,qual,size,offset)
            cf.write(line)
        print
        cf.write("\n")
        for child in self.children:
            child.print_symbol_table(cf,error_func)

    def makeTemp(self,size,offset_stack,type):
        global tempcount
        name = 't_'+str(tempcount)
        tempcount =tempcount+1
        self.addAddressDescriptor(name,offset_stack[-1])
        self.table[name]=dict(type=type,
                            size=size,
                            offset=offset_stack[-1],
                            defined=True)
        offset_stack[-1]=offset_stack[-1]+size
        return name

    def addAddressDescriptor(self,name,memory=None):
        self.addressDescriptor[name]  = { 'memory': memory , 
                                        'register': None, 
                                        'store': False , 
                                        'dirty': False,
                                        'scope': self.name,
                                        'variable': None,
                                        'arg': False}

    def updateAddressDescriptor(self,name,attribute,value):
        self.addressDescriptor[name][attribute]  = value