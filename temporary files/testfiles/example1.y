
	
	// BISON file example1 .y
%{
# include <stdio.h>
# include <stdlib.h>
# include <string>
# include <map>
using namespace std ;
map < string , double > vars ; // map from variable name to value
extern int yylex ();
extern void yyerror ( char *);
void Div0Error ( void );
void UnknownVarError ( string s );

int ls=0,l=0,ex=0,eq=0,va=0,sc=0,pl=0,num=0,in1=0,in2=0,mi=0,as=0,fs=0,pr=0;
%}

%union {
int int_val ;
double double_val ;
string * str_val ;
}

%token < int_val > PLUS MINUS ASTERISK FSLASH EQUALS PRINT LPAREN RPAREN SEMICOLON
%token < str_val > VARIABLE
%token < double_val > NUMBER

%type < double_val > expression ;
%type < double_val > inner1 ;
%type < double_val > inner2 ;

%left ASTERISK
%right MINUS
%right PLUS


%start parsetree


%%

parsetree : lines { printf ("parsetree--lines%d\n",ls); } ;
lines : lines line {ls++;printf ("lines%d--lines%d\n lines%d--line%d\n",ls,ls-1,ls,l); } | line{ ++ls;printf ("lines%d--line%d\n",ls,l);};
line : PRINT expression SEMICOLON { l++;pr++;sc++; printf ("line%d--PRINT%d\n line%d--expression%d\n line%d--SEMICOLON%d\n",l,pr,l,ex,l,sc );}
| VARIABLE EQUALS expression SEMICOLON { ++l;++va;++eq; ++sc;printf ("line%d--VARIABLE%d\n line%d--EQUALS%d\n line%d--expression%d\nline%d--SEMICOLON%d\n",l,va,l,eq,l,ex,l,sc );};
expression : expression PLUS inner1 { ++ex;++pl;printf("expression%d--expression%d\n expression%d--PLUS%d\n expression%d--inner1%d\n",ex,ex-1,ex,pl,ex,in1) ;}
| expression MINUS inner1 { ++ex;++mi;printf("expression%d--expression%d\n expression%d--MINUS%d\n expression%d--inner1%d\n",ex,ex-1,ex,mi,ex,in1) ;}
| inner1 { ++ex;printf("expression%d--inner1%d\n",ex,in1);};
inner1 : inner1 ASTERISK inner2 { ++in1;++as;printf("inner1%d--inner1%d\n inner1%d--ASTERISK%d\n inner1%d--inner2%d\n",in1,in1-1,in1,as,in1,in2) ;}
| inner1 FSLASH inner2 
{if( $3 == 0) Div0Error (); else {++in1;++fs;printf("inner1%d--inner1%d\n inner1%d--FSLASH%d\n inner1%d--inner2%d\n",in1,in1-1,in1,fs,in1,in2) ;}}
| inner2 { ++in1;printf("inner1%d--inner2%d\n",in1,in2);};
inner2 : VARIABLE
{ ++in2;++va;printf("inner2%d--VARIABLE%d\n",in2,va);};
| NUMBER { ++in2,++num;printf("inner2%d--NUMBER%d\n",in2,num);};
| LPAREN expression RPAREN { $$ = $2 ;};

%%

void Div0Error ( void ) { printf (" Error : division by zero \n"); exit (0);}
void UnknownVarError ( string s ) { printf (" Error : %s does not exist !\n", s . c_str ()); exit (0);}