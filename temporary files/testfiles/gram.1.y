
%%
program: stmt_list

stmt: ';'
    | expr ';'
    | VARIABLE '=' expr ';'
    | IF '(' expr ')' stmt
    | '{' stmt_list '}'
    ;

stmt_list: stmt
         | stmt_list stmt
         ;
expr: INTEGER
    | VARIABLE
    | '-' expr
    | expr '+' expr
    | expr '-' expr
    | expr '*' expr
    | expr '/' expr
    | expr '<' expr
    | expr '>' expr
    | expr GE expr
    | expr LE expr
    | expr NE expr
    | expr EQ expr
    | '(' expr ')'
    ;
