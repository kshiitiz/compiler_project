%{
#include <stdlib.h>
#include <string.h>
void yyerror(char *);
int lineno = 1;
%}
letter      [a-zA-Z]
digit       [0-9]
c
ident       {letter}({letter}|{digit})*

%%

0               { yylval.iValue = atoi(yytext);
                  return INTEGER;
                }
[1-9]{digit}*   { yylval.iValue = atoi(yytext);
	          return INTEGER;
	        }
[-()<>=+*/;{}.] { return *yytext;
		}
">=" 		return GE;
"<=" 		return LE;
"==" 		return EQ;
"!=" 		return NE;
"while" 	return WHILE;
"if" 		return IF;
"else" 		return ELSE;
"print" 	return PRINT;
[ \t]+ 	        ; /* ignore whitespace */
"\n" 	        lineno++ ; /* track line number */
{ident}         { yylval.code = strdup(yytext);
                  return VARIABLE;
                }
. 		yyerror("Unknown character");

%%
int yywrap(void) {
 return 1;
}
